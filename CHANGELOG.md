# Changelog overview of all mlcvzoo components

Dependency Matrix mlcvzoo-base:

## Classification Modules

| mlcvzoo-tf-classification | mlcvzoo-base |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
|---------------------------|--------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 2.0.0                     | ^2.0         | Initial release of the package                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| 3.0.0                     | ^3.0         | Use new features from AnnotationClassMapper that have been added with mlcvzoo_base v3.0.0                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | |
| 4.0.0                     | ^3.0         | Added functionality - Saving of training checkpoints to a directory: <br/> - Added ModelCheckpointConfig that configures a callback which is executed during training to save checkpoints <br/> - Added option for saving weights or full model state in checkpoints by introducing "save_weights_only" parameter in net_config, so also restoring a saved state to a net is possible <br/> - Removed Functional class from the Net classes' inheritance due to issues during model saving and finally because of deprecation of tf.python.keras module <br/> - Switched all imports of tf.python.keras module to tf.keras module <br/> - Changed saving/loading procedure in store and restore functions of the Net classes <br/> - Added tests for saving and loading checkpoints and full model |
| 4.0.1                     | ^3.0         | Prepare package for PyPi                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| 5.0.0                     | ^4.0         | Adapt to mlcvzoo-base 4.0.0: Remove the net classes and move the functionality to the respective models                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| 5.0.1                     | ^4.0         | Ensure ConfigBuilder version 7 compatibility                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| 6.0.0                     | ^5.0         | Implement API changes introduced by mlcvzoo-base version 5.0.0, Remove detector-config and use a single ModelConfiguration, Remove duplicate attributes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| 6.0.1                     | ^5.0         | Python 3.10 compatibility                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |

## Object Detection Modules

| mlcvzoo-darknet | mlcvzoo-base |                                                                                                                                                         |
|-----------------|--------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|
| 2.0.0           | ^2.0         | Initial release of the package                                                                                                                          |
| 3.0.0           | ^3.0         | Use new features from AnnotationClassMapper that have been added with mlcvzoo_base v3.0.0                                                               |
| 3.0.1           | ^3.0         | Prepare package for PyPi                                                                                                                                |
| 3.1.0           | ^4.0         | Adapt to mlcvzoo-base 4.0.0                                                                                                                             |
| 3.1.1           | ^4.0         | Ensure ConfigBuilder version 7 compatibility                                                                                                            |
| 3.1.1           | ^4.0         | Implement API changes introduced by mlcvzoo-base version 5.0.0, Remove detector-config and use a single ModelConfiguration, Remove duplicate attributes |
| 4.0.0           | ^5.0         | Implement API changes introduced by mlcvzoo-base version 5.0.0, Remove detector-config and use a single ModelConfiguration, Remove duplicate attributes |

<br></br>

| mlcvzoo-mmdetection | mlcvzoo-base |                                                                                                                                                                                                                                                                                                                                                                        |
|---------------------|--------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 2.0.0               | ^2.0         | Initial release of the package                                                                                                                                                                                                                                                                                                                                         |
| 2.0.1               | ^2.0         | Changed python executable for distributed training: <br/> - It can happen that the system python and python for running code are not the same. When starting distributed training, the system python was called. <br/> - Now the python executable that runs the code is also executed when starting distributed (multi gpu) training.                                 |
| 3.0.0               | ^3.0         | Use new features from AnnotationClassMapper that have been added with mlcvzoo_base v3.0.0                                                                                                                                                                                                                                                                              |
| 3.0.1               | ^3.0         | Prepare package for PyPi                                                                                                                                                                                                                                                                                                                                               |
| 4.0.0               | ^4.0         | Adapt to mlcvzoo-base 4.0.0 Refactor and enhance mlcvzoo_mmdetection: <br/> - The MMDetectionModel is now the base of all models of open-mmlab <br/> - Add MMObjectDetectionModel as dedicated model for object detection <br/> - Remove the CSVDataset and replace it with the MLCVZooMMDetDataset <br/> - Add latest commandline parameter for training mmdet models |                                                                                                                                                                                                                                                                                                    |
| 4.0.1               | ^4.0         | Ensure ConfigBuilder version 7 compatibility                                                                                                                                                                                                                                                                                                                           |
| 4.0.2               | ^4.0         | Fix bug in restore method: Ensure that the checkpoint is used which is passed to the method                                                                                                                                                                                                                                                                            |
| 5.0.0               | ^5.0         | Implement API changes introduced by mlcvzoo-base version 5.0.0, Remove detector-config and use a single ModelConfiguration, Remove duplicate attributes                                                                                                                                                                                                                |
| 5.0.1               | ^5.0         | Python 3.10 compatibility                                                                                                                                                                                                                                                                                                                                              |
|5.1.1 | ^5.0 | Add the MMSegmentationModel model |


<br></br>

| mlcvzoo-yolox | mlcvzoo-base |                                                                                                                                                               |
|---------------|--------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 2.0.0         | ^2.0         | Initial release of the package                                                                                                                                |
| 3.0.0         | ^3.0         | Use new features from AnnotationClassMapper that have been added with mlcvzoo_base v3.0.0                                                                     |
| 4.0.0         | ^3.0         | Refactor and update: <br/> - Update to yolox version 0.3.0 <br/> - Refactor initialization of yolox experiments in the MLCVZoo <br/> - Fix num_classes method |
| 4.0.1         | ^3.0         | Fix num_classes bug: Ensure that the model in yolox gets initialized with the correct number of classes                                                       |
| 4.0.2         | ^3.0         | Prepare package for PyPi                                                                                                                                      |
| 5.0.0         | ^4.0         | Adapt to mlcvzoo-base 4.0.0                                                                                                                                   |
| 5.1.0         | ^4.0         | Ensure ConfigBuilder version 7 compatibility                                                                                                                  |
| 5.2.0         | ^4.0         | Upgrade TensorRT version from 8.2.3.0 to 8.4.2.4                                                                                                              |
| 5.3.0         | ^4.0         | Include YOLOX multi-gpu training capabilities                                                                                                                 |
| 6.0.0         | ^5.0         | Implement API changes introduced by mlcvzoo-base version 5.0.0, Remove detector-config and use a single ModelConfiguration, Remove duplicate attributes       |
| 6.0.1         | ^5.0         | Python 3.10 compatibility                                                                                                                                     |

## OCR Modules

| mlcvzoo-mmocr | mlcvzoo-base | mlcvzoo-mmdetection |                                                                                                                                                              |
|---------------|--------------|---------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 2.0.0         | ^2.0         | ^2.0                | Initial release of the package                                                                                                                               |
| 2.0.1         | ^2.0         | ^2.0                | Changed python executable for distributed training                                                                                                           |
| 3.0.0         | ^3.0         | ^3.0                | Use new features from AnnotationClassMapper that have been added with mlcvzoo_base v3.0.0                                                                    |
| 3.1.0         | ^3.0         | ^3.0                | Add feature: Implement API feature "predict on many data-items" respectively "batch-inference" for the MMOCRTextDetectionModel and MMOCRTextRecognitionModel |
| 3.1.1         | ^3.2.1       | ^3.0.1              | Prepare package for PyPi                                                                                                                                     |
| 4.0.0         | ^4.0         | ^4.0                | - Adapt to mlcvzoo-base 4.0.0 and mlcvzoo-mmdetection 4.0.0 <br/> - Refactor and enhance mlcvzoo_mmocr                                                       |
| 4.0.1         | ^4.0         | ^4.0                | Fix processing of result in predict many function                                                                                                            |
| 4.0.2         | ^4.0         | ^4.0                | Ensure ConfigBuilder version 7 compatibility                                                                                                                 |
| 4.1.0         | ^4.0         | ^4.0                | Make the "from_yaml" of the MMOCRModel, MMOCRTextDetectionModel and MMOCRTextRecognitionModel constructor Optional                                           |
| 5.0.0         | ^5.0         | ^5.0                | Implement API changes introduced by mlcvzoo-base version 5.0.0 and mlcvzoo-mmdetetection version 5.0.0                                                       |
| 5.0.1 | ^5.0 | ^5.0 | Python 3.10 compatibility       |

## Other Modules

| mlcvzoo-util | mlcvzoo-base |                                                                                                                                                                                                                                                                                                     |
|--------------|--------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0.0.1        | ^4.2         | Initial release of the package                                                                                                                                                                                                                                                                      |
| 0.1.1        | ^4.2         | Remove dependency on backports.strenum                                                                                                                                                                                                                                                              |
| 0.2.0        | ^4.2         | Enhance MetricFactory: <br/> - Add optional parameter 'logging_configs' to MetricFactory.log_results <br/> - Add tensorboard false-positive and false-negative image logging to the ODMetricFactory <br/> - Fix a bug in log_false_positive_info_to_tb where bounding boxes where drawn incorrectly |
| 0.3.0        | ^5.0         | Adapt to mlcvzoo_base v5 API changes                                                                                                                                                                                                                                                                |




<br></br>

| mlcvzoo-tracker | mlcvzoo-base |                                                                                                                                                                                                                                                                                                     |
|-----------------|--------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0.1.0           | ^5.1         | Initial release of the package                                                                                                                                                                                                                                                                      |                                                                                                                                                                                                                                                             |


<br></br>
<br></br>
----------------------
# MLCVZoo Versions (Old):

NOTE: In version 2.0.0 on, the mlcvzoo was split into separate modules.
      Therefore, each project has its own CHANGELOG, which can be found
      in mlcvzoo_*/CHANGELOG.md

2.0.0 (2022-04-05)
------------------
Split the mlcvzoo into separate modules:
- No change in functionality
- Every model wrapper implementation of mlcvzoo.models becomes its own python project (module)
  - mlcvzoo.models.classification.tensorflow_models -> mlcvzoo_tf_classification
  - mlcvzoo.models.object_detection.darknet -> mlcvzoo_darknet
  - mlcvzoo.models.object_detection.mmdetection -> mlcvzoo_mmdetection
  - mlcvzoo.models.object_detection.yolox -> mlcvzoo_yolox
  - mlcvzoo.models.ocr.mmocr -> mlcvzoo_mmocr
- Every model project (mlcvzoo_*) depends on mlcvzoo_base
- mlcvzoo_mmocr depends on mlcvzoo_mmdetection
- Every model project (mlcvzoo_*) starts with version 2.0.0

1.6.1 (2022-03-29)
------------------
Replace f-strings with lazy loading in logging functions

1.6.0 (2022-03-29)
------------------
Add possibility to modify the text origin when drawing bounding boxes on an image

1.5.0 (2022-03-24)
------------------
Fix bug in the evaluation of bounding boxes of different sizes:
- It can happen that the size of the ground truth
  bounding box and the size of a true positive bounding
  are not the same. This leads to wrong metrics.
- Therefore, take the size of the matching ground truth bounding box
  as size for true positive bounding boxes
- Set reasonable default values for the metrics

1.4.0 (2022-03-23)
------------------
Refactor Coco Annotation Builder to accept annotations in COCO format from sources different from CVAT:
- Implement query to distinguish between the two types
- Reduce code complexity

1.3.0 (2022-03-23)
------------------
Add a function for saving mmdetection model checkpoint without optimizer states
- it keeps meta information and weights
- by skipping optimizer states the checkpoint size is halved

1.2.0 (2022-03-22)
------------------
Add interface for deployment of YOLOX models as TensorRT model:
- Add function for conversion from Torch to TensorRT: convert_to_tensorrt()
- Add option to enforce the loading of TensorRT model when instantiating a YOLOXModel
- Extend the configuration with new trt_config option to configure the TensorRT conversion and inference

1.1.0 (2022-03-09)
------------------
Add a new function to object detection metrics evaluation class which:
- Evaluates if FN bounding boxes of a class were detected as FP bounding boxes of another class
- Assign matching FN and FP bounding boxes in metric image info dict to a new attribute false_negative_matched_false_positive
- Return the updated metric image info dict
- Return a confusion matrix of the FN bounding boxes which matched FP bounding boxes

1.0.1 (2022-03-07)
------------------
- Definition of not required configuration attributes as Optional
- Addition of ValueError queries for methods that require optional attributes
  - Addition of unit-tests for coverage of changed code within ./models/object_detection/darknet/model.py

1.0.0 (2022-03-04)
------------------
Refactor and remove the usage of the EnvironmentConfig class:
- Rename it to a more appropriate name: EnvironmentConfig => ReplacementConfig
- Decouple the usage of the EnvironmentConfig from the configuration classes
- Make use of the EnvironmentConfig to define the relevant placeholders that are
  handed over to the string_replacement_map parameter of the ConfigBuilder
- Change model api:
  - Adapt the 'create_configuration(...)' method
  - Replace the config_root_dir or model_config_root_dir parameter by a string_replacement_map parameter
  - Adapt any other code that is affected by this change

0.17.0 (2022-02-22)
------------------
Refactor shared Objection Detection model parameters:
- Remove the unused config attribute iou-threshold. It is not
  related to the model configuration but only relevant for
  evaluation purposes. Therefore, leave it to the evaluation
  script to define this parameter.
- The nms-threshold can not be applied to every model inference_config.
  The mmdetection models for example have different settings where
  the nms-threshold is stated in their own configuration files. The
  mlcvzoo is not aware of that.
- Ensure correct usage of interface methods

0.16.0 (2022-02-21)
------------------
- Code quality improvements
- Fix violation of the 'Liskov substitution principle' in the model API
  - Don't overwrite method signatures in subclasses
  - Use generics instead
  - Split the api.model package into models and interfaces

0.15.0 (2022-02-10)
------------------
- Don't touch logging configuration inside library code
- Don't use print() in unit tests
- Move some main methods into tools where they belong so
  that the custom MLCVZoo logging package can move there too

0.14.0 (2022-02-09)
------------------
- Add method for filtering the datastructures of the BaseAnnotation class
- Enhance methods for checking the equality of the main data structures
  BaseAnnotation, Classification, BoundingBox and Segmentation

0.13.2 (2022-02-09)
------------------
Apply score filtering for mmocr models:
- Use the already existing configuration attribute and filter
  predictions of the MMOCRTextDetectionModel and MMOCRTextRecognitionModel
  that have a score beneath the configured threshold

0.13.1 (2022-02-08)
------------------
Ensure that project "extras" (poetry) are optional:
- Ensure that the extras defined in the pyproject.toml can indeed be used optionally
- This mainly includes the catching of exceptions of the type ModuleNotFoundError, when
  an optional dependency of an extra is not installed

0.13.0 (2022-02-08)
------------------
Add evaluation routine to yolox training:
- Replace dummy implementation of the class
  mlcvzoo.models.object_detection.yolox.configuration.MetricsEvaluation,
  so that it can be used to compute metrics during the training of a yolox model
- Refactor code in the yolox package where necessary
- Enhance data for related unit tests to increase test coverage


0.12.0 (2022-02-02)
------------------
Strictly divide metric computing functionality for object detection evaluation:
- Move everything that is related to computing the
  metrics to the MetricsEvaluation class
- provide methods to fill the internal data of the
  MetricsEvaluation class that is needed to compute
  the metrics
- The ObjectDetectorEvaluator initializes a MetricsEvaluation
  object and calls the according methods
- add method for calculating the AP over all classes
- fix error and stabilize MLFLowRunner

0.11.0 (2022-01-26)
------------------
- Adapt code to the changes that are introduced with the config-builder version v3.0.0
  - Remove the usage of the string replacement map that is defined per BaseConfigClass instance
  - refactor the generation of a string replacement map based on a given EnvironmentConfig instance
- Side fixes:
  - Fasten up the dependency resolving of poetry by setting fixed version ranges where possible
  - set pycocotools as optional dependency for the poetry extras mmocr and mmdetection

0.10.0 (2021-12-15)
------------------
- Add the model_config_root_dir for the creation of model configurations
  - Similar to the config_root_dir is can be used as placeholder in configuration files
- modify api:
  - make model.create_configuration static
  - this allows one to create a configuration prior to instantiating a model
- minor bug fixes and refactorings for yolox
- add third party method "order_points" from https://github.com/PyImageSearch/imutils
  perspective.order_points since there is a bug in their implementation

0.9.2 (2021-12-14)
------------------
- Remove test coverage for tools package
- minor refactorings

0.9.1 (2021-12-14)
------------------
- Ensure correct config files licensing:
  - Remove all external config files to avoid license issues
  - Shrink the remaining configs to contain only the necessary content
  - Move config files to a dedicated third party package if necessary
- Ensure correct behavior of distributed training for mmocr and mmdetection
- Minor bug fixes:
  - Revise config template tests
  - Error message improvements

0.9.0 (2021-12-10)
------------------
- rename VGGCustomBlock to CustomBlock
  - It follows design concepts of the VGG neural network but shouldn't be confused with VGG
  - minor bug fixes in

0.8.1 (2021-12-10)
------------------
- Use config-root-dir in mmocr and mmdetection Datasets correctly

0.8.0 (2021-12-09)
------------------
- add methods for generating information how a model was trained
  - info about git
  - info about installed pip packages
  - generate a YAML file out of the used model configuration

0.7.3 (2021-12-09)
------------------
- Refactor the import of the training module via importlib for mmocr and mmdet
  - Make it more robust
  - use only the specific directory in the PYTHONPATH when
  - calling the importlib function
  - this ensures that only the "train" module can be imported
  - that is actually meant there
  - remove unneeded function in the utils

0.7.2 (2021-12-07)
------------------
- update the handling of the os-string-replacement-map due to latest changes of the config-builder in version 2.3.0
- use a dedicated test image for object detection inference

0.7.2 (2021-12-07)
------------------
- refactor object detection evaluator
  - allow configuration to be optional so that it is easier to instantiate a default config
- add yolox to model registry

0.7.1 (2021-11-23)
------------------
- intergrate the yolox model from: https://github.com/Megvii-BaseDetection/YOLOX

0.7.0 (2021-12-01)
------------------
- refactor the training of the mmocr and mmdetection wrapper model
  - before the PYTHONPATH had to contain the parent folder where the
    projects have been checked out to, in order to be able to call their training function
  - now we use the package importlib to address their training function directly
- ensure that all models are able to either build their configuration from a given yaml file
  OR to set their configuration to a given configuration
- use the configuration import mechanism of mmdetection/mmocr where possible for unittest configurations
- revise logging verbosity

0.6.2 (2021-11-29)
------------------
- removed customized third party function order_points and exchanged it at its usages with the original imutils.perspective.order_points
- see also issue #65

0.6.1 (2021-11-26)
------------------
- take the complete impletentation of third party methods to avoid license issues
- remove unused api packages

0.6.0 (2021-11-24)
------------------
- Add Silicon Economy (OLF) License in project root and in every file header
- Move copy code to third party package
- Add arc42 documentation template

0.5.0 (2021-11-19)
------------------
- refactor api:
  - make api design leaner
  - remove TrainableNetBasedModel, NetBasedModel, TrainableModel and therefore inherit directly from NetBased and Trainable in consumer classes
  - make the ClassificationModel as super class for any classification related model, like ObjectDetectionModel and SegmentationModel
  - apply AnnotationWriter concept to the writing of csv annotation files
  - apply AnnotationWriter concept to the writing of darknet annotation files
  - refactor model-registry concept

0.4.4 (2021-11-17)
------------------
- rewrite copyright function of darknet

0.4.3 (2021-11-17)
------------------
- Overhaul container image to make it multi-arch capable

0.4.2 (2021-11-11)
------------------
- correct calculation of the false positive and related metrics
- there was an error counting FP for classes that had no ground truth data

0.4.1 (2021-11-03)
------------------
- correct version ranges

0.4.0 (2021-10-29)
------------------
- remove singleton pattern from AnnotationClassMapper
- apply builder pattern for AnnotationWriter

0.3.4 (2021-10-29)
------------------
- add basic docstrings

0.3.3 (2021-10-28)
------------------
- ability for relative paths for config files added to darknet

0.3.2 (2021-10-27)
------------------
- add feature for generating a cvat xml file out of a list of annotations

0.3.1 (2021-10-18)
------------------
- fix minor build issues

0.3.0 (2021-10-16)
------------------
- change build system to poetry

0.3.2 (2021-10-20)
------------------
- add basic doc-strings

0.2.0 (2021-10-01)
------------------
- adapt training for mmdetection and mmocr to use the code of the corresponding repositories

0.1.0 (2021-02-12)
------------------
- initial status

#!/bin/sh

# This script is intended to be executed from the root of the project

FAILED=0
for p in "mlcvzoo_"*; do
  scripts/complementary_license_check.sh "$p/third-party-licenses/third-party-licenses.csv" "$p/third-party-licenses/third-party-licenses-complementary.csv" || FAILED=1
done
exit $FAILED

#!/usr/bin/bash

# NOTE: This script is supposed to be executed from the
#       project root directory

# NOTE: Fill these variables with meaningful content, made
#       for your application

export DB_NAME=mlflowdb
export DB_USER=postgres
export DB_PW=12345678
export DB_PORT=5432

export MLFLOW_PORT=5001

export ARTIFACT_DATA_DIR="test_output/evaluation/test_mlflow/artifacts/"

export BACKEND_STORE_URI="postgresql://"${DB_USER}":"${DB_PW}"@localhost:"${DB_PORT}"/"${DB_NAME}


mlflow server \
  -p $MLFLOW_PORT \
  --backend-store-uri $BACKEND_STORE_URI  \
  --default-artifact-root $ARTIFACT_DATA_DIR

#!/usr/bin/bash

# NOTE: This script is supposed to be executed from the
#       project root directory

# NOTE: Fill these variables with meaningful content, made
#       for your application

# Variables for settings up postgressql database
export DB_NAME=mlflowdb
export DB_USER=postgres
export DB_PW=12345678
export DB_PORT=5432
export DB_DATA_DIR="test_output/evaluation/test_mlflow/mlflow_db"

mkdir -p $DB_DATA_DIR

docker-compose up --force-recreate --build

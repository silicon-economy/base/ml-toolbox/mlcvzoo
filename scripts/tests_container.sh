#!/bin/sh

if [ $# -lt 2 ]; then
  echo "Usage: $0 SUBPROJECT_SUFFIX CI_CONTAINER_IMAGE_TAG [EXTRA_DOCKER_ARGS]"
  return 1
fi

# Automatically select the correct path, assumes script is placed in scripts/ relative to repository
run_path=$(realpath "${0%/*}/..")

# Store and shift required args, pass the rest of them to docker later
subproject=$1; shift
tag=$1; shift

docker run -it \
  -v "$run_path"/mlcvzoo_"$subproject":/build-env/MLCVZoo/mlcvzoo_"$subproject" \
  -v "$run_path"/config/templates:/build-env/MLCVZoo/config/templates \
  -v "$run_path"/modeling_data:/build-env/MLCVZoo/modeling_data \
  -v "$run_path"/test_data:/build-env/MLCVZoo/test_data \
  -e MLCVZOO_DIR=/build-env/MLCVZoo \
  -e PROJECT_ROOT_DIR=/build-env/MLCVZoo \
  -e CONFIG_DIR=PROJECT_ROOT_DIR/config \
  -e BASELINE_MODEL_DIR=PROJECT_ROOT_DIR/modeling_data \
  -e MMDETECTION_DIR=/build-env/external/mmdet_repo \
  -e MMOCR_DIR=/build-env/external/mmocr_repo \
  -e DARKNET_DIR=/build-env/external/darknet \
  -e YOLOX_DIR=/build-env/external/yolox/ \
  -e MEDIA_DIR=PROJECT_ROOT_DIR \
  -e ANNOTATION_DIR=PROJECT_ROOT_DIR \
  "$@" \
  mlcvzoo_"$subproject":"$tag" sh -c "envsubst < /build-env/MLCVZoo/config/templates/replacement_config_template.yaml > /build-env/MLCVZoo/config/replacement_config.yaml \
    && poetry run pytest mlcvzoo_$subproject/tests"

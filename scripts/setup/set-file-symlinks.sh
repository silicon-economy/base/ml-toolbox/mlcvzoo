#!/usr/bin/env bash

export PYTHONPATH=$(pwd)/src

# ===========================================================
# Set symlink to the directory where the ImageNet weights for
# the mmdetection framework are stored

MMDETECTION_IMAGE_WEIGHTS_DIR_SOURCE=$1

MMDETECTION_IMAGE_WEIGHTS_DIR=~/.cache/torch/hub/

mkdir -p $MMDETECTION_IMAGE_WEIGHTS_DIR

MMDETECTION_IMAGE_WEIGHTS_DIR_TARGET=$MMDETECTION_IMAGE_WEIGHTS_DIR/checkpoints

ln -s $MMDETECTION_IMAGE_WEIGHTS_DIR_SOURCE $MMDETECTION_IMAGE_WEIGHTS_DIR_TARGET
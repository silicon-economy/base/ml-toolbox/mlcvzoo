#!/bin/bash

if [ $# -ne 2 ] ; then
  echo "Usage: $0 'MMOCR_VERSION' 'MMOCR_DIR'"
  echo "   MMOCR_VERSION: which version to install"
  echo "   MMOCR_DIR: the directory where the repository should be cloned to"
  echo "NOTE: the repository will be stored in the folder 'mmocr_repo' in the directory 'MMOCR_DIR'"
  echo "      This is necessary in order to be able to call the original training script of mmocr"
  echo 'Example: ./scripts/setup/get-mmocr.sh v0.3.0 /repositories/'
  exit 1
fi

export MMOCR_VERSION=$1
export MMOCR_DIR="$2/mmocr_repo"

git clone https://github.com/open-mmlab/MMOCR.git --branch $MMOCR_VERSION $MMOCR_DIR
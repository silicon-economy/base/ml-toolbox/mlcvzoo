#!/bin/bash

export MMDETECTION_VERSION=$1
export MMDETECTION_DIR=$2

git clone https://github.com/open-mmlab/mmdetection.git --branch $MMDETECTION_VERSION $MMDETECTION_DIR


#!/bin/bash

if [ $# -ne 2 ] ; then
  echo "Usage: $0 'MMDETECTION_VERSION' 'MMDETECTION_DIR'"
  echo "   MMDETECTION_VERSION: which version to install"
  echo "   MMDETECTION_DIR: the directory where the repository should be cloned to"
  echo "NOTE: the repository will be stored in the folder 'mmdet_repo' in the directory 'MMDETECTION_DIR'"
  echo "      This is necessary in order to be able to call the original training script of mmdetection"
  echo 'Example: ./scripts/setup/get-mmocr.sh v2.14.0 /repositories/'
  exit 1
fi

export MMDETECTION_VERSION=$1
export MMDETECTION_DIR="$2/mmdet_repo"

git clone https://github.com/open-mmlab/mmdetection.git --branch $MMDETECTION_VERSION $MMDETECTION_DIR
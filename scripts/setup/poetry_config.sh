#!/bin/bash

# Use new access token if provided
if [ -n "$1" ] && [ -n "$2" ]; then
  poetry config http-basic.cfgbuilder "$1" "$2"
  poetry config http-basic.related "$1" "$2"
  poetry config http-basic.mlcvzoo "$1" "$2"
fi

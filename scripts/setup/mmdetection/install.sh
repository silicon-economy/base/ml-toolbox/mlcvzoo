#!/usr/bin/env bash

CURRENT_DIR=$(pwd)

CUDA_HOME=/usr/local/cuda-$1

MMDET_VERSION=$2

MMDETECTION_DIR=$3

MLCVZOO_DIR=$4

MMDETECTION_NAME="mmdetection-fork"

MMDETECTION_PATH=$MMDETECTION_DIR"/"$MMDETECTION_NAME


MMDET_PIP_STRING="git+ssh://git@gitlab.cc-asp.fraunhofer.de/motten/"$MMDETECTION_NAME".git@"$MMDET_VERSION
MMDET_GIT_STRING="git@gitlab.cc-asp.fraunhofer.de:motten/"$MMDETECTION_NAME".git"

pip3 install -r $MLCVZOO_DIR/requirements/mmdetection.txt

pip3 install --upgrade $MMDET_PIP_STRING

mkdir -p $MMDETECTION_DIR

if test ! -d "$MMDETECTION_PATH"; then
  echo "clone branch " $MMDET_VERSION " from mmdetection to " $MMDETECTION_DIR "..."
  git clone $MMDET_GIT_STRING --branch $MMDET_VERSION $MMDETECTION_PATH
else
  echo "mmdetection already cloned to" $MMDETECTION_DIR"/mmdetection"
fi

cd $MMDETECTION_PATH

git checkout $MMDET_VERSION

python3 setup.py install

cd $CURRENT_DIR
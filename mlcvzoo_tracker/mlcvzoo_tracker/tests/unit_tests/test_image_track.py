# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import os
from copy import copy
from typing import Any, Dict, List, Optional, Tuple, cast
from unittest import TestCase, main, skip

import numpy as np
from config_builder import ConfigBuilder
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information

from mlcvzoo_tracker.configuration import TrackerConfig
from mlcvzoo_tracker.image_track import ImageTrack, TrackerState

logger = logging.getLogger(__name__)


class TestImageTrack(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_tracker"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

        # set up image track
        self.tracker_config = cast(
            TrackerConfig,
            ConfigBuilder(
                class_type=TrackerConfig,
                yaml_config_path=os.path.join(
                    self.project_root,
                    "mlcvzoo_tracker/mlcvzoo_tracker/tests/test_data/test_tracker/"
                    "hungarian_tracker_config.yaml",
                ),
                string_replacement_map=self.string_replacement_map,
            ).configuration,
        )

        self.track_id: int = 0

        self.initial_bounding_box: BoundingBox = BoundingBox(
            box=Box(xmin=0, ymin=0, xmax=2, ymax=2),
            class_identifier=ClassIdentifier(
                class_id=0,
                class_name="cup",
            ),
            score=0.8,
            difficult=False,
            occluded=False,
            content="",
        )

        self.restore()

    def restore(self) -> None:
        self.image_track: ImageTrack = ImageTrack(
            configuration=self.tracker_config,
            track_id=self.track_id,
            initial_frame_id=0,
            initial_bbox=self.initial_bounding_box,
            initial_color_hist=None,
            meta_info=None,  # currently not used
        )

    @staticmethod
    def assert_track_lifecycle(
        t: ImageTrack,
        is_active: bool,
        is_alive: bool,
        is_valid: bool,
        current_state: TrackerState,
    ) -> None:
        assert (
            t.is_active() == is_active
        ), f"is_active should be {t.is_active()} found {is_active}"
        assert (
            t.is_alive() == is_alive
        ), f"is_alive should be {t.is_alive()} found {is_alive}"
        assert (
            t.is_valid() == is_valid
        ), f"is_valid should be {t.is_valid()} found {is_valid}"
        assert (
            t.current_state == current_state
        ), f"current_state should be {t.current_state} found {current_state}"

    @staticmethod
    def run_dead_reckoning(
        image_track: ImageTrack,
        state_dict: Dict[str, Tuple[Any, Any, Optional[Any]]],
        occlusion_bounding_boxes: Optional[List[BoundingBox]] = None,
    ) -> None:
        TestImageTrack.assert_track_lifecycle(
            image_track,
            state_dict["is_active"][0],
            state_dict["is_alive"][0],
            state_dict["is_valid"][0],
            state_dict["current_state"][0],
        )
        image_track.predict(occlusion_bounding_boxes=occlusion_bounding_boxes)
        TestImageTrack.assert_track_lifecycle(
            image_track,
            state_dict["is_active"][1],
            state_dict["is_alive"][1],
            state_dict["is_valid"][1],
            state_dict["current_state"][1],
        )

    @staticmethod
    def run_with_update(
        image_track: ImageTrack,
        bb: BoundingBox,
        state_dict: Dict[str, Tuple[Any, Any, Any]],
        occlusion_bounding_boxes: Optional[List[BoundingBox]] = None,
    ) -> None:
        TestImageTrack.run_dead_reckoning(
            image_track, state_dict, occlusion_bounding_boxes
        )
        image_track.update(bb)
        TestImageTrack.assert_track_lifecycle(
            image_track,
            state_dict["is_active"][2],
            state_dict["is_alive"][2],
            state_dict["is_valid"][2],
            state_dict["current_state"][2],
        )

    def test_image_track_state(self) -> None:
        """
        Possible changes of state:

        from INITIATED to ...
            > ACTIVE (1st)
            > not to OCCLUDED (6th)
            > DEAD (9th)

        from ACTIVE to ...
            > OCCLUDED (2nd)
            > DEAD (4th)

        from OCCLUDED to ...
            > ACTIVE (3rd)
            > DEAD (7th)

        from DEAD NOT to ...
            > ACTIVE (5th)
            > OCCLUDED (8th)


        (1st)           (2nd)       (3rd)           (4th)       (5th)
        INITIATED   >   ACTIVE  >   OCCLUDED    >   ACTIVE  >   DEAD    > not to ACTIVE

        (6th)
        INITIATED   >   not to OCCLUDED

        (7th)           (8th)
        OCCLUDED    >   DEAD    >   not to OCCLUDED

        (9th)
        INITIATED   >   DEAD

        (10th) in one run_with_update(...)
        ACTIVE  >   OCCLUDED    >   ACTIVE

        """

        # 1st: INITIATED   >   ACTIVE
        logger.info(
            "############################################################\n"
            "# TEST state change from INITIATED to ACTIVE\n"
            "############################################################"
        )

        # initial state and state after next_frame, add_detection, finish_frame
        state_min_detections: Dict[str, Tuple[Any, Any, Any]] = {
            "is_active": (False,) * 3,
            "is_alive": (True,) * 3,
            "is_valid": (False,) * 3,
            "current_state": (TrackerState.INITIATED,) * 3,
        }

        # go up to (min_detections_active - 1) sensor updates/detections
        # initialization already counts as one sensor updates/detection so the loop goes till (min_detections_active - 2)
        for i in range(self.tracker_config.min_detections_active - 2):
            TestImageTrack.run_with_update(
                self.image_track,
                self.initial_bounding_box,
                state_min_detections,
            )

        # state change when meeting min_detections_active
        state_min_detections_active: Dict[str, Tuple[Any, Any, Any]] = {
            "is_active": (False, False, True),
            "is_alive": (True,) * 3,
            "is_valid": (False, False, True),
            "current_state": (
                TrackerState.INITIATED,
                TrackerState.INITIATED,
                TrackerState.ACTIVE,
            ),
        }

        # next sensor update/detection should validate track
        TestImageTrack.run_with_update(
            self.image_track,
            self.initial_bounding_box,
            state_min_detections_active,
        )

        # 2nd: ACTIVE  >   OCCLUDED
        logger.info(
            "############################################################\n"
            "# TEST state change from ACTIVE to OCCLUDED\n"
            "############################################################"
        )

        # introduce occlusion
        state_active_to_occlusion: Dict[str, Tuple[Any, Any]] = {
            "is_active": (True,) * 2,
            "is_alive": (True,) * 2,
            "is_valid": (True,) * 2,
            "current_state": (TrackerState.ACTIVE, TrackerState.OCCLUDED),
        }

        TestImageTrack.run_dead_reckoning(
            self.image_track,
            state_active_to_occlusion,
            occlusion_bounding_boxes=[self.initial_bounding_box],
        )

        # 3rd: OCCLUDED    >   ACTIVE
        logger.info(
            "############################################################\n"
            "# TEST state change from OCCLUDED to ACTIVE\n"
            "############################################################"
        )

        state_occlusion_to_active: Dict[str, Tuple[Any, Any, Any]] = {
            "is_active": (True,) * 3,
            "is_alive": (True,) * 3,
            "is_valid": (True,) * 3,
            "current_state": (
                TrackerState.OCCLUDED,
                TrackerState.OCCLUDED,
                TrackerState.ACTIVE,
            ),
        }

        TestImageTrack.run_with_update(
            self.image_track,
            self.initial_bounding_box,
            state_occlusion_to_active,
        )

        # 4th: ACTIVE  >   DEAD
        logger.info(
            "############################################################\n"
            "# TEST state change from ACTIVE to DEAD\n"
            "############################################################"
        )

        state_active_1: Dict[str, Tuple[Any, Any, Any]] = {
            "is_active": (True,) * 3,
            "is_alive": (True,) * 3,
            "is_valid": (True,) * 3,
            "current_state": (TrackerState.ACTIVE,) * 3,
        }

        # make one update
        TestImageTrack.run_with_update(
            self.image_track,
            self.initial_bounding_box,
            state_active_1,
        )

        state_active_2: Dict[str, Tuple[Any, Any]] = {
            "is_active": (True,) * 2,
            "is_alive": (True,) * 2,
            "is_valid": (True,) * 2,
            "current_state": (TrackerState.ACTIVE,) * 2,
        }

        for i in range(self.tracker_config.max_age + 1):
            TestImageTrack.run_dead_reckoning(
                self.image_track,
                state_active_2,
            )

        state_active_to_dead: Dict[str, Tuple[Any, Any]] = {
            "is_active": (True, False),
            "is_alive": (True, False),
            "is_valid": (True,) * 2,
            "current_state": (TrackerState.ACTIVE, TrackerState.DEAD),
        }

        TestImageTrack.run_dead_reckoning(
            self.image_track,
            state_active_to_dead,
        )

        # 5th: DEAD    > not to ACTIVE
        logger.info(
            "############################################################\n"
            "# TEST for impossible state change from DEAD to ACTIVE \n"
            "############################################################"
        )

        state_still_dead_1: Dict[str, Tuple[Any, Any]] = {
            "is_active": (False,) * 2,
            "is_alive": (False,) * 2,
            "is_valid": (True,) * 2,
            "current_state": (TrackerState.DEAD,) * 2,
        }

        state_still_dead_2: Dict[str, Tuple[Any, Any, Any]] = {
            "is_active": (False,) * 3,
            "is_alive": (False,) * 3,
            "is_valid": (True,) * 3,
            "current_state": (TrackerState.DEAD,) * 3,
        }

        TestImageTrack.run_with_update(
            self.image_track,
            self.initial_bounding_box,
            state_still_dead_2,
        )

        TestImageTrack.run_with_update(
            self.image_track,
            self.initial_bounding_box,
            state_still_dead_2,
            occlusion_bounding_boxes=[self.initial_bounding_box],
        )

        TestImageTrack.run_dead_reckoning(
            self.image_track,
            state_still_dead_1,
        )

        TestImageTrack.run_dead_reckoning(
            self.image_track,
            state_still_dead_1,
            occlusion_bounding_boxes=[self.initial_bounding_box],
        )

        # 6th: INITIATED   >   not to OCCLUDED
        logger.info(
            "############################################################\n"
            "# TEST for impossible state change from INITIATED to OCCLUDED\n"
            "############################################################"
        )

        self.restore()

        state_initiated_to_occluded: Dict[str, Tuple[Any, Any]] = {
            "is_active": (False,) * 2,
            "is_alive": (True,) * 2,
            "is_valid": (False,) * 2,
            "current_state": (TrackerState.INITIATED,) * 2,
        }

        TestImageTrack.run_dead_reckoning(
            self.image_track,
            state_initiated_to_occluded,
            occlusion_bounding_boxes=[self.initial_bounding_box],
        )

        # 7th: OCCLUDED  >     DEAD
        logger.info(
            "############################################################\n"
            "# TEST state change from OCCLUDED to DEAD\n"
            "############################################################"
        )

        self.tracker_config.max_age = 1
        self.tracker_config.min_detections_active = (
            0  # skip INITIALIZED state and become ACTIVE immediately
        )

        self.restore()

        state_active_to_occluded: Dict[str, Tuple[Any, Any]] = {
            "is_active": (True,) * 2,
            "is_alive": (True,) * 2,
            "is_valid": (True,) * 2,
            "current_state": (TrackerState.ACTIVE, TrackerState.OCCLUDED),
        }

        TestImageTrack.run_dead_reckoning(
            self.image_track,
            state_active_to_occluded,
            occlusion_bounding_boxes=[self.initial_bounding_box],
        )

        state_occluded: Dict[str, Tuple[Any, Any]] = {
            "is_active": (True,) * 2,
            "is_alive": (True,) * 2,
            "is_valid": (True,) * 2,
            "current_state": (TrackerState.OCCLUDED,) * 2,
        }

        TestImageTrack.run_dead_reckoning(
            self.image_track,
            state_occluded,
        )

        state_occluded_to_dead: Dict[str, Tuple[Any, Any]] = {
            "is_active": (True, False),
            "is_alive": (True, False),
            "is_valid": (True,) * 2,
            "current_state": (TrackerState.OCCLUDED, TrackerState.DEAD),
        }

        TestImageTrack.run_dead_reckoning(
            self.image_track,
            state_occluded_to_dead,
        )

        # 8th: DEAD    >   not to OCCLUDED
        logger.info(
            "############################################################\n"
            "# TEST for impossible state change from DEAD to OCCLUDED \n"
            "############################################################"
        )

        state_dead: Dict[str, Tuple[Any, Any]] = {
            "is_active": (False,) * 2,
            "is_alive": (False,) * 2,
            "is_valid": (True,) * 2,
            "current_state": (TrackerState.DEAD,) * 2,
        }

        TestImageTrack.run_dead_reckoning(
            self.image_track,
            state_dead,
            occlusion_bounding_boxes=[self.initial_bounding_box],
        )

        # 9th: INITIATED   >   DEAD
        logger.info(
            "############################################################\n"
            "# TEST state change from INITIATED to DEAD\n"
            "############################################################"
        )

        self.tracker_config.max_age = 1
        self.tracker_config.min_detections_active = 10

        self.restore()

        state_initiated: Dict[str, Tuple[Any, Any]] = {
            "is_active": (False,) * 2,
            "is_alive": (True,) * 2,
            "is_valid": (False,) * 2,
            "current_state": (TrackerState.INITIATED, TrackerState.INITIATED),
        }

        # this sets the ImageTrack to its max age
        TestImageTrack.run_dead_reckoning(
            self.image_track,
            state_initiated,
        )

        # this will exceed the max age
        TestImageTrack.run_dead_reckoning(
            self.image_track,
            state_initiated,
        )

        state_initiated_to_dead: Dict[str, Tuple[Any, Any]] = {
            "is_active": (False,) * 2,
            "is_alive": (True, False),
            "is_valid": (False,) * 2,
            "current_state": (TrackerState.INITIATED, TrackerState.DEAD),
        }

        # this will detect the exceeded max age
        TestImageTrack.run_dead_reckoning(
            self.image_track,
            state_initiated_to_dead,
        )

        # (10th) in one run_with_update(...)
        # ACTIVE  >   OCCLUDED    >   ACTIVE
        logger.info(
            "############################################################\n"
            "# TEST state change from ACTIVE to OCCLUDED back to ACTIVE"
            "# in one run_with_update()\n"
            "############################################################"
        )

        # become active immediately
        self.tracker_config.min_detections_active = 0
        self.restore()

        state_active_to_occlusion_to_active: Dict[str, Tuple[Any, Any, Any]] = {
            "is_active": (True,) * 3,
            "is_alive": (True,) * 3,
            "is_valid": (True,) * 3,
            "current_state": (
                TrackerState.ACTIVE,
                TrackerState.OCCLUDED,
                TrackerState.ACTIVE,
            ),
        }

        TestImageTrack.run_with_update(
            self.image_track,
            self.initial_bounding_box,
            state_active_to_occlusion_to_active,
            occlusion_bounding_boxes=[self.initial_bounding_box],
        )

    def test_image_track_kalman_filter(self) -> None:
        """
        Test-Cases:

        kalman_delay

        modes
         - prediction
         - update

        scenarios
         - not moving
         - moving
         - occlusion

        kalman_delay    >   prediction  >   occlusion   >   update

        """

        logger.info(
            "############################################################\n"
            "# TEST Kalman Filter: kalman_delay\n"
            "############################################################"
        )

        self.tracker_config.max_age = 4
        self.restore()

        for i in range(self.tracker_config.kalman_filter_config.kalman_delay - 1):
            self.image_track.predict()
            self.image_track.update(self.initial_bounding_box)

            # set position (x=10, y=16) of Kalman Filter state different to the current bounding box position
            self.image_track._kf.x = np.array([[10], [2], [16], [4]])

            cur_box: Box = copy(self.image_track.get_current_bounding_box().box)

            # Kalman Filter got updated in update().
            # Call predict(...) again to apply our recent Kalman Filter state changes.
            # During this loop this should not change the current bounding box.
            self.image_track.predict()

            # assert the latest bounding box is used as current position as long as kalman_delay is not exceeded
            assert cur_box == self.image_track.get_current_bounding_box().box
            assert cur_box == self.initial_bounding_box.box

            kalman_filter_box: Box = copy(
                cur_box
            )  # copy cur_box to retain width and height
            kalman_filter_box.new_center(
                int(self.image_track._kf.x[0]), int(self.image_track._kf.x[2])
            )

            # assert that the Kalman Filter position is not used as current position
            # as long as kalman_delay is not exceeded
            assert cur_box != kalman_filter_box

        logger.info(
            "############################################################\n"
            "# TEST Kalman Filter: kalman_delay exceedance\n"
            "############################################################"
        )

        # now the position of the Kalman Filter should be used
        self.image_track.predict()
        self.image_track.update(self.initial_bounding_box)

        # assert the Kalman Filter position is used instead of the latest bounding box
        box_old: Box = copy(self.image_track.get_current_bounding_box().box)
        assert box_old != self.initial_bounding_box.box

        # set position (x=10, y=16) of Kalman Filter state different to the current initial_bbox position
        self.image_track._kf.x = np.array([[10], [2], [16], [4]])

        # Kalman Filter got updated in update()
        # call predict(...) again to apply our recent Kalman Filter state changes
        # this should change the current bounding box
        self.image_track.predict()

        cur_box: Box = copy(self.image_track.get_current_bounding_box().box)

        assert box_old != cur_box
        assert cur_box != self.initial_bounding_box.box

        kalman_filter_box: Box = copy(
            cur_box
        )  # copy cur_box to retain width and height
        kalman_filter_box.new_center(
            int(self.image_track._kf.x[0]), int(self.image_track._kf.x[2])
        )

        # assert the current bounding box matches the position of the Kalman Filter
        assert cur_box == kalman_filter_box

        logger.info(
            "############################################################\n"
            "# TEST Kalman Filter: tracking\n"
            "############################################################"
        )

        # current KF state:
        #   x    dx    y    dy
        # [[12], [2], [20], [4]]

        self.image_track.predict()

        cur_box: Box = copy(self.image_track.get_current_bounding_box().box)
        box_correct: Box = copy(cur_box)  # copy cur_box to retain width and height
        box_correct.new_center(12 + 2, 20 + 4)

        assert cur_box == box_correct

        # set initial_bbox to next updated position for occlusion
        self.initial_bounding_box.box.new_center(16, 28)
        self.image_track.predict(
            occlusion_bounding_boxes=[
                self.initial_bounding_box,
            ]
        )

        cur_box: Box = copy(self.image_track.get_current_bounding_box().box)

        box_correct: Box = copy(cur_box)  # copy cur_box to retain width and height
        box_correct.new_center(16, 28)  # set the center where it is supposed to be

        assert cur_box == box_correct
        assert self.image_track.current_state == TrackerState.OCCLUDED

        # redetect, but with negative movement
        self.initial_bounding_box.box.translation(-16, -28)
        self.image_track.predict()
        self.image_track.update(self.initial_bounding_box)

        # with the current parameters the Kalman Filter jumps to the position of the last bounding box update
        assert (
            self.initial_bounding_box.box
            == self.image_track.get_current_bounding_box().box
        )

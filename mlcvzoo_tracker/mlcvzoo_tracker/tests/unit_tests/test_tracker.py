# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import os
from copy import copy, deepcopy
from typing import Dict, List, Optional, Tuple, cast
from unittest import TestCase, main

import cv2
import numpy as np
from config_builder import ConfigBuilder
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.types import ImageType
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information
from scipy.optimize import linear_sum_assignment

from mlcvzoo_tracker.configuration import TrackerConfig
from mlcvzoo_tracker.hungarian_image_tracker import HungarianImageTracker
from mlcvzoo_tracker.image_track import ImageTrack

logger = logging.getLogger(__name__)


class TestTracker(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_tracker"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

    @staticmethod
    def check_next_state(
        t: HungarianImageTracker,
        bbs: List[BoundingBox],
        num_tracks: int,
        num_alive_tracks: int,
        num_active_tracks: int,
        num_valid_tracks: int,
        frame: Optional[ImageType] = None,
        occlusion_bounding_boxes: Optional[List[BoundingBox]] = None,
    ) -> None:
        t.next(
            bounding_boxes=bbs,
            frame=frame,
            occlusion_bounding_boxes=occlusion_bounding_boxes,
        )

        assert len(t.get_tracks()) == num_tracks
        assert len(t.get_alive_tracks()) == num_alive_tracks
        assert len(t.get_active_tracks()) == num_active_tracks
        assert len(t.get_valid_tracks()) == num_valid_tracks

    def test_hungarian_image_tracker_track_management(self) -> None:
        # min_detections_active and max_age are set to one so tracking takes effect almost immediately
        tracker_config = cast(
            TrackerConfig,
            ConfigBuilder(
                class_type=TrackerConfig,
                yaml_config_path=os.path.join(
                    self.project_root,
                    "mlcvzoo_tracker/mlcvzoo_tracker/tests/test_data/test_tracker/"
                    "hungarian_tracker_config.yaml",
                ),
                string_replacement_map=self.string_replacement_map,
            ).configuration,
        )
        tracker_config.max_age = 1
        tracker_config.min_detections_active = 1

        tracker: HungarianImageTracker = HungarianImageTracker(
            configuration=tracker_config,
            object_class_identifier=ClassIdentifier(class_id=0, class_name="cup"),
        )

        frame_path: str = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )

        # shape = (500, 375, 3)
        frame = cv2.imread(frame_path)

        # tracker just got initialized
        assert len(tracker.get_tracks()) == 0
        assert len(tracker.get_alive_tracks()) == 0
        assert len(tracker.get_active_tracks()) == 0
        assert len(tracker.get_valid_tracks()) == 0

        bounding_box1 = BoundingBox(
            box=Box(xmin=0, ymin=0, xmax=2, ymax=2),
            class_identifier=ClassIdentifier(
                class_id=0,
                class_name="cup",
            ),
            score=0.8,
            difficult=False,
            occluded=False,
            content="",
        )

        bounding_box2 = BoundingBox(
            box=Box(xmin=2, ymin=2, xmax=4, ymax=4),
            class_identifier=ClassIdentifier(
                class_id=0,
                class_name="cup",
            ),
            score=0.8,
            difficult=False,
            occluded=False,
            content="",
        )

        bounding_box3 = BoundingBox(
            box=Box(xmin=10, ymin=0, xmax=12, ymax=2),
            class_identifier=ClassIdentifier(
                class_id=0,
                class_name="cup",
            ),
            score=0.8,
            difficult=False,
            occluded=False,
            content="",
        )

        bounding_box4 = BoundingBox(
            box=Box(xmin=10, ymin=10, xmax=12, ymax=12),
            class_identifier=ClassIdentifier(
                class_id=0,
                class_name="cup",
            ),
            score=0.8,
            difficult=False,
            occluded=False,
            content="",
        )

        # tracks in tracker just got initialized
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box1, bounding_box2],
            num_tracks=2,
            num_alive_tracks=2,
            num_valid_tracks=0,
            num_active_tracks=0,
            frame=frame,
        )

        # no movement yet
        # tracks in tracker should now be active and valid
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box1, bounding_box2],
            num_tracks=2,
            num_alive_tracks=2,
            num_valid_tracks=2,
            num_active_tracks=2,
            frame=frame,
        )

        # move bounding_box1 from (1, 1) to (2, 2)
        bounding_box1.box.translation(1, 1)
        # nothing should have changed
        # no new track, movement got tracked
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box1, bounding_box2],
            num_tracks=2,
            num_alive_tracks=2,
            num_valid_tracks=2,
            num_active_tracks=2,
        )

        # move bounding_box1 on top of bounding_box2
        x, y = bounding_box2.box.center()
        bounding_box1.box.translation(x, y)
        # nothing should have changed
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box1, bounding_box2],
            num_tracks=2,
            num_alive_tracks=2,
            num_valid_tracks=2,
            num_active_tracks=2,
        )

        # add a third bounding_box
        # a third track should be alive
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box1, bounding_box2, bounding_box3],
            num_tracks=3,
            num_alive_tracks=3,
            num_valid_tracks=2,
            num_active_tracks=2,
        )

        # a third track should be active
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box1, bounding_box2, bounding_box3],
            num_tracks=3,
            num_alive_tracks=3,
            num_valid_tracks=3,
            num_active_tracks=3,
        )

        # remove bounding_box1
        # nothing should have changed after first iteration
        # this iteration reaches max_age
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box2, bounding_box3],
            num_tracks=3,
            num_alive_tracks=3,
            num_valid_tracks=3,
            num_active_tracks=3,
        )

        # nothing should have changed after second iteration
        # this iteration exceeds max_age
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box2, bounding_box3],
            num_tracks=3,
            num_alive_tracks=3,
            num_valid_tracks=3,
            num_active_tracks=3,
        )

        # this iteration should bring change
        # this iteration recognizes the exceedance of max_age
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box2, bounding_box3],
            num_tracks=3,
            num_alive_tracks=2,
            num_valid_tracks=3,
            num_active_tracks=2,
        )

        # add bounding_box1
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box1, bounding_box2, bounding_box3],
            num_tracks=4,
            num_alive_tracks=3,
            num_valid_tracks=3,
            num_active_tracks=2,
        )

        # add bounding_box4
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box1, bounding_box2, bounding_box3, bounding_box4],
            num_tracks=5,
            num_alive_tracks=4,
            num_valid_tracks=4,
            num_active_tracks=3,
        )

        # the update from adding bounding_box1 and bounding_box4 should take full effect now
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box1, bounding_box2, bounding_box3, bounding_box4],
            num_tracks=5,
            num_alive_tracks=4,
            num_valid_tracks=5,
            num_active_tracks=4,
        )

        # move two boxes and remove two
        # deletion should take effect in two iterations
        bounding_box3.box.translation(1, 1)
        bounding_box4.box.translation(-1, -1)
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box3, bounding_box4],
            num_tracks=5,
            num_alive_tracks=4,
            num_valid_tracks=5,
            num_active_tracks=4,
        )
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box3, bounding_box4],
            num_tracks=5,
            num_alive_tracks=4,
            num_valid_tracks=5,
            num_active_tracks=4,
        )

        # deletion should take effect now
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box3, bounding_box4],
            num_tracks=5,
            num_alive_tracks=2,
            num_valid_tracks=5,
            num_active_tracks=2,
        )

        # test occlusion
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box3, bounding_box4],
            num_tracks=5,
            num_alive_tracks=2,
            num_valid_tracks=5,
            num_active_tracks=2,
            occlusion_bounding_boxes=[bounding_box3],
        )
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box4],
            num_tracks=5,
            num_alive_tracks=2,
            num_valid_tracks=5,
            num_active_tracks=2,
            occlusion_bounding_boxes=[bounding_box3],
        )
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box4],
            num_tracks=5,
            num_alive_tracks=2,
            num_valid_tracks=5,
            num_active_tracks=2,
            occlusion_bounding_boxes=[bounding_box3],
        )

        # deletion of bounding_box3 takes affect now
        # add another bounding_box
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box4, bounding_box1],
            num_tracks=5,
            num_alive_tracks=1,
            num_valid_tracks=5,
            num_active_tracks=1,
            occlusion_bounding_boxes=[bounding_box3],
        )
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box4, bounding_box1],
            num_tracks=6,
            num_alive_tracks=2,
            num_valid_tracks=5,
            num_active_tracks=1,
            occlusion_bounding_boxes=[bounding_box3],
        )

        # delete all dead tracks
        tracker.configuration.keep_dead_tracks = False
        TestTracker.check_next_state(
            t=tracker,
            bbs=[bounding_box4, bounding_box1],
            num_tracks=2,
            num_alive_tracks=2,
            num_valid_tracks=2,
            num_active_tracks=2,
            occlusion_bounding_boxes=[bounding_box3],
        )

    @staticmethod
    def _init_bounding_box_with_hist(
        corners: List[float], margins: Tuple[float, float], frame: ImageType
    ) -> Tuple[BoundingBox, List[Optional[ImageType]]]:
        bounding_box: BoundingBox = BoundingBox(
            box=Box(xmin=corners[0], ymin=corners[1], xmax=corners[2], ymax=corners[3]),
            class_identifier=ClassIdentifier(
                class_id=0,
                class_name="cup",
            ),
            score=0.8,
            difficult=False,
            occluded=False,
            content="",
        )

        hist: List[Optional[ImageType]] = bounding_box.box.color_hist(
            margin_x=margins[0],
            margin_y=margins[1],
            frame=frame,
        )

        return bounding_box, hist

    def test_hungarian_image_tracker_cost_matrix(self) -> None:
        """

        1. initialize bounding boxes, then ImageTrack s

        2. alterate bounding boxes - movement, color, size

        3. prepare params

        4. compute cost matrix

        5. validate cost matrix

        """

        # min_detections_active and max_age are set to one so tracking takes effect almost immediately
        tracker_config = cast(
            TrackerConfig,
            ConfigBuilder(
                class_type=TrackerConfig,
                yaml_config_path=os.path.join(
                    self.project_root,
                    "mlcvzoo_tracker/mlcvzoo_tracker/tests/test_data/test_tracker/"
                    "hungarian_tracker_config.yaml",
                ),
                string_replacement_map=self.string_replacement_map,
            ).configuration,
        )

        # shape = (500, 375, 3)
        frame = cv2.imread(
            os.path.join(
                self.project_root,
                "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
            )
        )

        # margins
        margins: Tuple[float, float] = (
            tracker_config.assignment_cost_config.color_cost.margin_x,
            tracker_config.assignment_cost_config.color_cost.margin_y,
        )

        # initialize bounding boxes for ImageTrack s
        # each bb is 20 px wide and 20 px high
        # each bb starts at 20 and ends at 40 on the x-axis

        corners_1: List[float] = [20, 20, 40, 40]
        bounding_box_1, hist_1 = TestTracker._init_bounding_box_with_hist(
            corners=corners_1,
            margins=margins,
            frame=frame,
        )

        corners_2: List[float] = [20, 100, 40, 120]
        bounding_box_2, hist_2 = TestTracker._init_bounding_box_with_hist(
            corners=corners_2,
            margins=margins,
            frame=frame,
        )

        corners_3: List[float] = [20, 200, 40, 220]
        bounding_box_3, hist_3 = TestTracker._init_bounding_box_with_hist(
            corners=corners_3,
            margins=margins,
            frame=frame,
        )

        corners_4: List[float] = [20, 300, 40, 320]
        bounding_box_4, hist_4 = TestTracker._init_bounding_box_with_hist(
            corners=corners_4,
            margins=margins,
            frame=frame,
        )

        corners_5: List[float] = copy(corners_1)
        corners_5[0] = corners_1[0] + 4  # x_min
        corners_5[2] = corners_1[2] + 4  # x_max
        bounding_box_5, _ = TestTracker._init_bounding_box_with_hist(
            corners=corners_5,
            margins=margins,
            frame=frame,
        )
        hist_5 = hist_1

        # initialize bounding boxes for matching

        # no movement
        corners_1_1: List[float] = copy(corners_1)
        bounding_box_1_1, _ = TestTracker._init_bounding_box_with_hist(
            corners=corners_1_1,
            margins=margins,
            frame=frame,
        )
        hist_1_1 = hist_1

        # movement - bb2_2 is pushed 2 pixels further to the right
        corners_2_1: List[float] = copy(corners_2)
        corners_2_1[0] = corners_2[0] + 30  # x_min
        corners_2_1[2] = corners_2[2] + 30  # x_max
        bounding_box_2_1, _ = TestTracker._init_bounding_box_with_hist(
            corners=corners_2_1,
            margins=margins,
            frame=frame,
        )
        hist_2_1 = hist_2

        corners_2_2: List[float] = copy(corners_2)
        corners_2_2[0] = corners_2[0] + 32  # x_min
        corners_2_2[2] = corners_2[2] + 32  # x_max
        bounding_box_2_2, _ = TestTracker._init_bounding_box_with_hist(
            corners=corners_2_2,
            margins=margins,
            frame=frame,
        )
        hist_2_2 = hist_2

        # color - both bounding boxes at roughly the same position, but the second one with different colors
        corners_3_1: List[float] = copy(corners_3)
        corners_3_1[0] = corners_3[0] + 30  # x_min
        corners_3_1[2] = corners_3[2] + 30  # x_max
        bounding_box_3_1, _ = TestTracker._init_bounding_box_with_hist(
            corners=corners_3_1,
            margins=margins,
            frame=frame,
        )
        hist_3_1 = hist_3

        # move bounding_box_3_2 one px back towards bounding_box_3
        # now the color cost has to outweight the distance cost
        frame += 50
        frame[frame > 255] = 255
        bounding_box_3_2 = deepcopy(bounding_box_3_1)
        bounding_box_3_2.box.translation(-1, 0)
        hist_3_2 = bounding_box_3.box.color_hist(  # recompute color histogram for bounding_box_3 with changed frame
            margin_x=margins[0],
            margin_y=margins[1],
            frame=frame,
        )

        # iou - second BB has greater iou
        corners_4_1: List[float] = copy(corners_4)
        corners_4_1[0] = corners_4[0] - 4  # x_min
        corners_4_1[2] = corners_4[2] + 4  # x_max
        bounding_box_4_1, _ = TestTracker._init_bounding_box_with_hist(
            corners=corners_4_1,
            margins=margins,
            frame=frame,
        )
        hist_4_1 = hist_4

        corners_4_2: List[float] = copy(corners_4)
        corners_4_2[0] = corners_4[0] - 2  # x_min
        corners_4_2[2] = corners_4[2] + 2  # x_max
        bounding_box_4_2, _ = TestTracker._init_bounding_box_with_hist(
            corners=corners_4_2,
            margins=margins,
            frame=frame,
        )
        hist_4_2 = hist_4

        # 5th BB for testing 1:1 matching between tracks and bounding boxes
        corners_5_1: List[float] = copy(corners_1)
        corners_5_1[0] = corners_5[0] + 20  # x_min
        corners_5_1[2] = corners_5[2] + 20  # x_max
        bounding_box_5_1, _ = TestTracker._init_bounding_box_with_hist(
            corners=corners_5_1,
            margins=margins,
            frame=frame,
        )
        hist_5_1 = hist_5

        # create params for _compute_cost_matrix(...)

        # initialize ImageTrack s

        track_1: ImageTrack = ImageTrack(
            configuration=tracker_config,
            track_id=0,
            initial_frame_id=0,
            initial_bbox=bounding_box_1,
            initial_color_hist=hist_1,
        )

        track_2: ImageTrack = ImageTrack(
            configuration=tracker_config,
            track_id=1,
            initial_frame_id=0,
            initial_bbox=bounding_box_2,
            initial_color_hist=hist_2,
        )

        track_3: ImageTrack = ImageTrack(
            configuration=tracker_config,
            track_id=2,
            initial_frame_id=0,
            initial_bbox=bounding_box_3,
            initial_color_hist=hist_3,
        )

        track_4: ImageTrack = ImageTrack(
            configuration=tracker_config,
            track_id=3,
            initial_frame_id=0,
            initial_bbox=bounding_box_4,
            initial_color_hist=hist_4,
        )

        track_5: ImageTrack = ImageTrack(
            configuration=tracker_config,
            track_id=4,
            initial_frame_id=0,
            initial_bbox=bounding_box_5,
            initial_color_hist=hist_5,
        )

        iou_weight: float = tracker_config.assignment_cost_config.iou_weight
        distance_cost_weight: float = (
            tracker_config.assignment_cost_config.distance_cost.weight
        )
        color_cost_weight: float = (
            tracker_config.assignment_cost_config.color_cost.weight
        )

        alive_tracks: List[ImageTrack] = [
            track_1,
            track_2,
            track_3,
            track_4,
            track_5,
        ]

        bounding_boxes: List[BoundingBox] = [
            bounding_box_1_1,
            bounding_box_2_1,
            bounding_box_2_2,
            bounding_box_3_1,
            bounding_box_3_2,
            bounding_box_4_1,
            bounding_box_4_2,
            bounding_box_5_1,
        ]

        current_hists: List[Optional[ImageType]] = [
            hist_1_1,
            hist_2_1,
            hist_2_2,
            hist_3_1,
            hist_3_2,
            hist_4_1,
            hist_4_2,
            hist_5_1,
        ]

        # compute cost matrix and validate

        # (len(alive_tracks), len(bounding_boxes))
        cost_matrix: ImageType = HungarianImageTracker._compute_cost_matrix(
            iou_weight=iou_weight,
            distance_cost_weight=distance_cost_weight,
            color_cost_weight=color_cost_weight,
            alive_tracks=alive_tracks,
            bounding_boxes=bounding_boxes,
            current_hists=current_hists,
        )

        min_inds = np.argmin(cost_matrix, axis=1)

        logger.info(f"cost matrix: {cost_matrix}")
        logger.info(f"max inds: {min_inds}")

        # track_1 -> bounding_box_1_1 - min for row 0 at col 0
        assert min_inds[0] == 0

        # track_2 -> bounding_box_2_1 - min for row 1 at col 1
        assert min_inds[1] == 1

        # track_3 -> bounding_box_3_1 - min for row 2 at col 3
        assert min_inds[2] == 3

        # track_4 -> bounding_box_4_2 - min for row 3 at col 6
        assert min_inds[3] == 6

        # track_5 -> bounding_box_1_1 - min for row 4 at col 0
        # track_1 and track_5 match to bounding_box_1_1
        assert min_inds[4] == 0

        # compute matching and validate
        row_ind: NDArray[Shape["Any"], Float]
        col_ind: NDArray[Shape["Any"], Float]
        row_ind, col_ind = linear_sum_assignment(cost_matrix)

        logger.info(f"row_ind: {row_ind}")
        logger.info(f"col_ind: {col_ind}")

        assert row_ind[0] == 0 and col_ind[0] == 0
        assert row_ind[1] == 1 and col_ind[1] == 1
        assert row_ind[2] == 2 and col_ind[2] == 3
        assert row_ind[3] == 3 and col_ind[3] == 6

        # track_5 and track_1 can not match to the same BB so track_5 has to match to a BB further away
        assert row_ind[4] == 4 and col_ind[4] == 7


if __name__ == "__main__":
    main()

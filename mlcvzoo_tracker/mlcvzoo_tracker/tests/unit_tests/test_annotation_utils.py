import logging
import os
from copy import deepcopy
from datetime import datetime
from typing import Dict, List
from unittest import TestCase

from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information

from mlcvzoo_tracker.annotation_utils import (
    read_image_tracks,
    write_image_tracks_to_mot,
)
from mlcvzoo_tracker.image_track import TrackerState, TrackEvent

logger = logging.getLogger(__name__)


class TestAnnotationUtils(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_tracker"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

        """
        Write an annotation file in the MOT201617 / MOT2020 format
        based on the given list of image-tracks. Format for is:

        <frame>, <id>, <bb_left>, <bb_top>, <bb_width>, <bb_height>, <conf>, <class>, <visibility>


        FRAME 0:
            Bounding Box 1:
                1, 1, 0, 0, 10, 10, 1.0, 1, 1.0  // move at constant speed (1, 2)
            Bounding Box 2:
                1, 2, 100, 100, 10, 10, 1.0, 2, 1.0  // no movement
            Bounding Box 3:
                1, 3, 50, 50, 20, 20, 1.0, 2, 1.0  // no movement
            Bounding Box 4:
                1, 4, 0, 0, 20, 20, 1.0, 2, 1.0  // no movement
        FRAME 1:
            Bounding Box 1:
                2, 1, 1, 2, 10, 10, 1.0, 1, 1.0
            Bounding Box 2:
                2, 2, 100, 100, 10, 10, 1.0, 2, 1.0
            Bounding Box 3:
                2, 3, 50, 50, 20, 20, 1.0, 2, 1.0
        FRAME 2:
            Bounding Box 1:
                3, 1, 2, 4, 10, 10, 1.0, 1, 1.0
            Bounding Box 5:
                3, 5, 0, 0, 20, 20, 1.0, 1, 1.0  // no movement
            Bounding Box 2:
                DEAD
        FRAME 3:
            Bounding Box 1:
                DEAD



        frame       f"{frame_id + 1},"
        id          f"{track_event.track_id + 1},"
        bb_left     f"{track_event.bounding_box.box.xmin},"
        bb_top      f"{track_event.bounding_box.box.ymin},"
        bb_width    f"{track_event.bounding_box.box.width},"
        bb_height   f"{track_event.bounding_box.box.height},"
        conf        f"{conf},"
        class       f"{track_event.bounding_box.class_id + 1},"
        visibility  f"{visibility}\n"

        """

        self.mot_output_correct: List[str] = [
            "1, 1, 0, 0, 10, 10, 1.0, 1, 1.0",
            "1, 2, 100, 100, 10, 10, 1.0, 2, 1.0",
            "1, 3, 50, 50, 20, 20, 1.0, 2, 1.0",
            "1, 4, 0, 0, 20, 20, 1.0, 2, 1.0",
            "2, 1, 1, 2, 10, 10, 1.0, 1, 1.0",
            "2, 2, 100, 100, 10, 10, 1.0, 2, 1.0",
            "2, 3, 50, 50, 20, 20, 1.0, 2, 1.0",
            "3, 1, 2, 4, 10, 10, 1.0, 1, 1.0",
            "3, 5, 0, 0, 20, 20, 1.0, 1, 1.0",
        ]

        bounding_box_1: BoundingBox = BoundingBox(
            box=Box(xmin=0, ymin=0, xmax=10, ymax=10),
            class_identifier=ClassIdentifier(
                class_id=0,
                class_name="cup",
            ),
            score=0.8,
            difficult=False,
            occluded=False,
            content="",
        )

        bounding_box_2: BoundingBox = BoundingBox(
            box=Box(xmin=100, ymin=100, xmax=110, ymax=110),
            class_identifier=ClassIdentifier(
                class_id=1,
                class_name="cup",
            ),
            score=0.8,
            difficult=False,
            occluded=False,
            content="",
        )

        bounding_box_3: BoundingBox = BoundingBox(
            box=Box(xmin=50, ymin=50, xmax=70, ymax=70),
            class_identifier=ClassIdentifier(
                class_id=1,
                class_name="cup",
            ),
            score=0.8,
            difficult=False,
            occluded=False,
            content="",
        )

        bounding_box_4: BoundingBox = BoundingBox(
            box=Box(xmin=0, ymin=0, xmax=20, ymax=20),
            class_identifier=ClassIdentifier(
                class_id=1,
                class_name="cup",
            ),
            score=0.8,
            difficult=False,
            occluded=False,
            content="",
        )

        bounding_box_5: BoundingBox = BoundingBox(
            box=Box(xmin=0, ymin=0, xmax=20, ymax=20),
            class_identifier=ClassIdentifier(
                class_id=0,
                class_name="cup",
            ),
            score=0.8,
            difficult=False,
            occluded=False,
            content="",
        )

        track_1: List[TrackEvent] = []
        track_2: List[TrackEvent] = []
        track_3: List[TrackEvent] = []
        track_4: List[TrackEvent] = []
        track_5: List[TrackEvent] = []

        track_1.append(
            TestAnnotationUtils.create_track_event(
                bounding_box=bounding_box_1,
                state=TrackerState.INITIATED,
                frame_id=0,
                track_id=0,
            )
        )
        bounding_box_1 = deepcopy(bounding_box_1)
        bounding_box_1.box.translation(1, 2)
        track_1.append(
            TestAnnotationUtils.create_track_event(
                bounding_box=bounding_box_1,
                state=TrackerState.ACTIVE,
                frame_id=1,
                track_id=0,
            )
        )
        bounding_box_1 = deepcopy(bounding_box_1)
        bounding_box_1.box.translation(1, 2)
        track_1.append(
            TestAnnotationUtils.create_track_event_from_reference(
                reference_track_event=track_1[-1],
                bounding_box=bounding_box_1,
            )
        )
        bounding_box_1 = deepcopy(bounding_box_1)
        bounding_box_1.box.translation(1, 2)
        track_1.append(  # append TrackEvent in state DEAD
            TestAnnotationUtils.create_track_event(
                bounding_box=bounding_box_1,
                state=TrackerState.DEAD,
                frame_id=3,
                track_id=0,
            )
        )

        track_2.append(
            TestAnnotationUtils.create_track_event(
                bounding_box=bounding_box_2,
                state=TrackerState.INITIATED,
                frame_id=0,
                track_id=1,
            )
        )
        track_2.append(
            TestAnnotationUtils.create_track_event(
                bounding_box=bounding_box_2,
                state=TrackerState.ACTIVE,
                frame_id=1,
                track_id=1,
            )
        )
        track_2.append(  # append TrackEvent in state DEAD
            TestAnnotationUtils.create_track_event(
                bounding_box=bounding_box_2,
                state=TrackerState.DEAD,
                frame_id=3,
                track_id=1,
            )
        )

        track_3.append(
            TestAnnotationUtils.create_track_event(
                bounding_box=bounding_box_3,
                state=TrackerState.INITIATED,
                frame_id=0,
                track_id=2,
            )
        )
        track_3.append(
            TestAnnotationUtils.create_track_event(
                bounding_box=bounding_box_3,
                state=TrackerState.ACTIVE,
                frame_id=1,
                track_id=2,
            )
        )

        track_4.append(
            TestAnnotationUtils.create_track_event(
                bounding_box=bounding_box_4,
                state=TrackerState.INITIATED,
                frame_id=0,
                track_id=3,
            )
        )

        track_5.append(
            TestAnnotationUtils.create_track_event(
                bounding_box=bounding_box_5,
                state=TrackerState.INITIATED,
                frame_id=2,
                track_id=4,
            )
        )

        # convert tracks to dict[frame_id] = List[TrackEvent]
        self.track_events_dict: Dict[int, List[TrackEvent]] = {
            0: [
                track_1[0],
                track_2[0],
                track_3[0],
                track_4[0],
            ],
            1: [track_1[1], track_2[1], track_3[1]],
            2: [track_1[2], track_5[0], track_2[2]],
            3: [track_1[3]],
            4: [],  # add one empty record
        }

    @staticmethod
    def create_track_event(
        bounding_box: BoundingBox,
        state: TrackerState,
        frame_id: int,
        track_id: int,
    ) -> TrackEvent:
        track_event = TrackEvent(
            bounding_box=bounding_box,
            timestamp=datetime.now(),  # not relevant for this test
            state=state,
            frame_id=frame_id,
            track_id=track_id,
            speed=1.0,  # not relevant for this test
        )

        return track_event

    @staticmethod
    def create_track_event_from_reference(
        reference_track_event: TrackEvent,
        bounding_box: BoundingBox,
    ) -> TrackEvent:
        new_track_event = TrackEvent(
            bounding_box=bounding_box,
            timestamp=datetime.now(),  # not relevant for this test
            state=reference_track_event.state,
            frame_id=reference_track_event.frame_id + 1,
            track_id=reference_track_event.track_id,
            speed=1.0,  # not relevant for this test
        )

        return new_track_event

    def test_write_image_tracks_to_mot(self) -> None:
        mot_output_file = os.path.join(
            self.project_root,
            "test_output",
            "tracking",
            "test_annotation_utils_mot_in.txt",
        )

        write_image_tracks_to_mot(self.track_events_dict, mot_output_file)

        mot_lines = []

        with open(mot_output_file, "r") as mot_file:
            mot_lines = mot_file.readlines()

        for i in range(len(mot_lines)):
            line_test = mot_lines[i].strip().replace(" ", "")
            line_correct = self.mot_output_correct[i].strip().replace(" ", "")

            assert line_test == line_correct

    def test_read_image_tracks(self) -> None:
        mot_input_file = os.path.join(
            self.project_root,
            "mlcvzoo_tracker",
            "mlcvzoo_tracker",
            "tests",
            "test_data",
            "test_tracker",
            "test_annotation_utils_mot_in.txt",
        )

        tracks_dict: Dict[int, List[TrackEvent]] = read_image_tracks(mot_input_file)

        line_counter = 0

        for i in range(len(tracks_dict)):
            track_event_list = tracks_dict[i]

            for track_event in track_event_list:
                line_correct = (
                    self.mot_output_correct[line_counter].strip().replace(" ", "")
                )
                line_counter += 1

                conf = "1.0"
                visibility = "1.0"
                # IMPORTANT NOTE: Indices in MOT are 1-based, therefore plus 1 for the IDs
                # <frame>, <id>, <bb_left>, <bb_top>, <bb_width>, <bb_height>, <conf>, <class>, <visibility>
                mot_line = (
                    (
                        f"{track_event.frame_id + 1},"
                        f"{track_event.track_id + 1},"
                        f"{track_event.bounding_box.box.xmin},"
                        f"{track_event.bounding_box.box.ymin},"
                        f"{track_event.bounding_box.box.width},"
                        f"{track_event.bounding_box.box.height},"
                        f"{conf},"
                        f"{track_event.bounding_box.class_id + 1},"
                        f"{visibility}\n"
                    )
                    .strip()
                    .replace(" ", "")
                )

                assert mot_line == line_correct

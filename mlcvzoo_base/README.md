# MLCVZoo Base

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_base** provides the base modules
that are defining the MLCVZoo API. Furthermore, it includes modules that allow to handle
and process the data structures of the MLCVZoo, as well as providing modules for
running evaluations / calculation of metrics.

Further information about the MLCVZoo can be found [here](../README.md).

## Install
`
pip install mlcvzoo-base
`

## Technology stack

- Python

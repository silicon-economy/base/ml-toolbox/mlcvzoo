# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import os
from typing import Any, Dict
from unittest import TestCase, main

from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information

logger = logging.getLogger(__name__)


class TestAPISegmentation(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(file_path=__file__, dir_depth=5, code_base="mlcvzoo_base")

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(self.project_root, "config", "replacement_config.yaml"),
        )

        logger.debug(
            "Setup finished: \n"
            " - this_dir: %s\n"
            " - project_root: %s\n"
            " - code_root: %s\n"
            % (
                self.this_dir,
                self.project_root,
                self.code_root,
            )
        )

    @staticmethod
    def __create_dummy_segmentation(with_box: bool = True) -> Segmentation:
        if with_box:
            box = Box(xmin=0, ymin=0, xmax=100, ymax=100)
        else:
            box = None
        return Segmentation(
            polygon=[(0, 0), (100, 0), (100, 100), (0, 100)],
            box=box,
            class_identifier=ClassIdentifier(class_id=0, class_name="test"),
            model_class_identifier=ClassIdentifier(class_id=0, class_name="test"),
            score=1,
            difficult=False,
            occluded=False,
            content="",
        )

    def test_to_dict(self) -> None:
        dummy_segmentation: Segmentation = self.__create_dummy_segmentation()
        expected_dict: Dict = {
            "box": {"xmax": 100, "xmin": 0, "ymax": 100, "ymin": 0},
            "class_identifier": {"class_id": 0, "class_name": "test"},
            "content": "",
            "difficult": False,
            "model_class_identifier": {"class_id": 0, "class_name": "test"},
            "occluded": False,
            "polygon": [(0, 0), (100, 0), (100, 100), (0, 100)],
            "score": 1,
        }

        assert dummy_segmentation.to_dict() == expected_dict

    def test_to_dict_without_box(self) -> None:
        dummy_segmentation: Segmentation = self.__create_dummy_segmentation(with_box=False)
        expected_dict: Dict = {
            "box": None,
            "class_identifier": {"class_id": 0, "class_name": "test"},
            "content": "",
            "difficult": False,
            "model_class_identifier": {"class_id": 0, "class_name": "test"},
            "occluded": False,
            "polygon": [(0, 0), (100, 0), (100, 100), (0, 100)],
            "score": 1,
        }

        assert dummy_segmentation.to_dict() == expected_dict

    def test_to_dict_raw(self) -> None:
        dummy_segmentation: Segmentation = self.__create_dummy_segmentation()
        expected_dict: Dict = {
            "box": Box(xmin=0, ymin=0, xmax=100, ymax=100),
            "class_identifier": dummy_segmentation.class_identifier,
            "content": "",
            "difficult": False,
            "model_class_identifier": dummy_segmentation.model_class_identifier,
            "occluded": False,
            "polygon": [(0, 0), (100, 0), (100, 100), (0, 100)],
            "score": 1,
        }

        assert dummy_segmentation.to_dict(raw_type=True) == expected_dict

    def test_to_dict_reduced(self) -> None:
        dummy_segmentation: Segmentation = self.__create_dummy_segmentation()
        expected_dict: Dict = {
            "box": {"xmax": 100, "xmin": 0, "ymax": 100, "ymin": 0},
            "class_id": 0,
            "class_name": "test",
            "model_class_id": 0,
            "model_class_name": "test",
            "polygon": [(0, 0), (100, 0), (100, 100), (0, 100)],
            "score": 1,
        }

        assert dummy_segmentation.to_dict(reduced=True) == expected_dict

    def test_to_dict_raw_reduced(self) -> None:
        dummy_segmentation: Segmentation = self.__create_dummy_segmentation()
        expected_dict: Dict = {
            "box": Box(xmin=0, ymin=0, xmax=100, ymax=100),
            "class_id": 0,
            "class_name": "test",
            "model_class_id": 0,
            "model_class_name": "test",
            "polygon": [(0, 0), (100, 0), (100, 100), (0, 100)],
            "score": 1,
        }

        assert dummy_segmentation.to_dict(raw_type=True, reduced=True) == expected_dict

    def test_from_dict(self) -> None:
        segmentation_dict: Dict = {
            "box": {"xmax": 100, "xmin": 0, "ymax": 100, "ymin": 0},
            "class_identifier": {"class_id": 0, "class_name": "test"},
            "content": "",
            "difficult": False,
            "model_class_identifier": {"class_id": 0, "class_name": "test"},
            "occluded": False,
            "polygon": [(0, 0), (100, 0), (100, 100), (0, 100)],
            "score": 1,
        }
        segmentation: Segmentation = Segmentation.from_dict(segmentation_dict)

        assert segmentation == self.__create_dummy_segmentation()

    def test_from_dict_reduced(self) -> None:
        segmentation_dict: Dict = {
            "box": {"xmax": 100, "xmin": 0, "ymax": 100, "ymin": 0},
            "class_id": 0,
            "class_name": "test",
            "model_class_id": 0,
            "model_class_name": "test",
            "polygon": [(0, 0), (100, 0), (100, 100), (0, 100)],
            "score": 1,
        }
        segmentation: Segmentation = Segmentation.from_dict(segmentation_dict, reduced=True)

        assert segmentation == self.__create_dummy_segmentation()

    def test_to_json(self) -> None:
        dummy_segmentation: Segmentation = self.__create_dummy_segmentation()
        segmentation_json: Any = dummy_segmentation.to_json()
        expected_json = {
            "box": {"xmax": 100, "xmin": 0, "ymax": 100, "ymin": 0},
            "class_identifier": {"class_id": 0, "class_name": "test"},
            "content": "",
            "difficult": False,
            "model_class_identifier": {"class_id": 0, "class_name": "test"},
            "occluded": False,
            "polygon": [(0, 0), (100, 0), (100, 100), (0, 100)],
            "score": 1,
        }
        print(type(segmentation_json))
        assert segmentation_json == expected_json


if __name__ == "__main__":
    main()

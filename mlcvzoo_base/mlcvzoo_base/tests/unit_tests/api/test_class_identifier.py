# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import os
from typing import Any, Dict
from unittest import TestCase, main

from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information

logger = logging.getLogger(__name__)


class TestAPIClassIdentifier(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(file_path=__file__, dir_depth=5, code_base="mlcvzoo_base")

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(self.project_root, "config", "replacement_config.yaml"),
        )

        logger.debug(
            "Setup finished: \n"
            " - this_dir: %s\n"
            " - project_root: %s\n"
            " - code_root: %s\n"
            % (
                self.this_dir,
                self.project_root,
                self.code_root,
            )
        )

    def test_to_dict(self) -> None:
        dummy_class_identifier: ClassIdentifier = ClassIdentifier(class_id=0, class_name="test")
        expected_dict: Dict = {"class_id": 0, "class_name": "test"}

        assert dummy_class_identifier.to_dict() == expected_dict

    def test_to_json(self) -> None:
        dummy_class_identifier: ClassIdentifier = ClassIdentifier(class_id=0, class_name="test")
        expected_json: Any = {"class_id": 0, "class_name": "test"}

        assert dummy_class_identifier.to_json() == expected_json


if __name__ == "__main__":
    main()

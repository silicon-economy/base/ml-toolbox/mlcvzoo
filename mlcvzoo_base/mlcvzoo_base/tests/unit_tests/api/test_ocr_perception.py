# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import os
from typing import Any, Dict
from unittest import TestCase, main

from mlcvzoo_base.api.data.ocr_perception import OCRPerception
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information

logger = logging.getLogger(__name__)


class TestAPIOCRPerception(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(file_path=__file__, dir_depth=5, code_base="mlcvzoo_base")

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(self.project_root, "config", "replacement_config.yaml"),
        )

        logger.debug(
            "Setup finished: \n"
            " - this_dir: %s\n"
            " - project_root: %s\n"
            " - code_root: %s\n"
            % (
                self.this_dir,
                self.project_root,
                self.code_root,
            )
        )

    @staticmethod
    def __create_dummy_perception() -> OCRPerception:
        return OCRPerception(
            words=["a", "b", "c"], word_scores=[0.1, 0.2, 0.3], content="test", score=0.4
        )

    def test_to_json(self) -> None:
        dummy_perception: OCRPerception = self.__create_dummy_perception()
        perception_json: Any = dummy_perception.to_json()
        expected_json: Dict = {
            "content": "test",
            "score": 0.4,
            "word_scores": [0.1, 0.2, 0.3],
            "words": ["a", "b", "c"],
        }
        assert perception_json == expected_json

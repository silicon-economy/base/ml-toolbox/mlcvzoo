# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import copy
import logging
import os
from typing import Dict
from unittest import TestCase, main

from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.ocr_perception import OCRPerception
from mlcvzoo_base.configuration.structs import ObjectDetectionBBoxFormats
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information

logger = logging.getLogger(__name__)

# TODO: add test that surrounding box is written to csv files


class TestMLCVZooAPI(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(file_path=__file__, dir_depth=5, code_base="mlcvzoo_base")

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(self.project_root, "config", "replacement_config.yaml"),
        )

        logger.debug(
            "Setup finished: \n"
            " - this_dir: %s\n"
            " - project_root: %s\n"
            " - code_root: %s\n"
            % (
                self.this_dir,
                self.project_root,
                self.code_root,
            )
        )

        self.test_ocr_perception_words = OCRPerception(
            words=["Hello", "World"], word_scores=[0.9, 0.8], content="", score=0.99
        )
        self.test_ocr_perception_content = OCRPerception(
            words=[], word_scores=[], content="Hello World", score=0.99
        )

    def test_ocr_perception_to_dict(self) -> None:
        test_ocr_perception_words = copy.deepcopy(self.test_ocr_perception_words)
        test_ocr_perception_words_dict = test_ocr_perception_words.to_dict()

        assert test_ocr_perception_words_dict["words"] == ["Hello", "World"]
        assert test_ocr_perception_words_dict["word_scores"] == [0.9, 0.8]
        assert test_ocr_perception_words_dict["content"] == "Hello World"
        assert test_ocr_perception_words_dict["score"] == 0.99

    def test_ocr_perception_from_dict(self) -> None:
        test_ocr_perception_words_dict = {}
        test_ocr_perception_words_dict["words"] = ["Hello", "World"]
        test_ocr_perception_words_dict["word_scores"] = [0.9, 0.8]
        test_ocr_perception_words_dict["content"] = "Hello World"
        test_ocr_perception_words_dict["score"] = 0.99

        test_ocr_perception_words = OCRPerception.from_dict(
            from_dict=test_ocr_perception_words_dict
        )

        assert test_ocr_perception_words.words == ["Hello", "World"]
        assert test_ocr_perception_words.word_scores == [0.9, 0.8]
        assert test_ocr_perception_words.content == "Hello World"
        assert test_ocr_perception_words.score == 0.99

    def test_class_identifier_from_str(self) -> None:
        class_identifier: ClassIdentifier = ClassIdentifier.from_str(class_identifier_str="0_Car")

        assert class_identifier.class_id == 0
        assert class_identifier.class_name == "Car"

        class_identifier = ClassIdentifier.from_str(class_identifier_str="1_Ignored_Class")

        assert class_identifier.class_id == 1
        assert class_identifier.class_name == "Ignored_Class"

    def test_class_identifier_from_str_no_delimiter_error(self) -> None:
        with self.assertRaises(ValueError):
            ClassIdentifier.from_str(class_identifier_str="Car")

    def test_class_identifier_from_str_no_class_id_info_error(self) -> None:
        with self.assertRaises(ValueError):
            ClassIdentifier.from_str(class_identifier_str="_Car")

    def test_class_identifier_from_str_no_class_name_info_error(self) -> None:
        with self.assertRaises(ValueError):
            ClassIdentifier.from_str(class_identifier_str="0_")


if __name__ == "__main__":
    main()

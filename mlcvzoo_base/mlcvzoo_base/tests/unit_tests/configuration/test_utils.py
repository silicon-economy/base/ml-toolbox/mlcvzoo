# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import os
from argparse import ArgumentTypeError
from typing import Dict, Optional
from unittest import TestCase, main

from mlcvzoo_base.configuration.utils import (
    get_replacement_map_from_replacement_config,
    str2bool,
)
from mlcvzoo_base.utils.file_utils import get_project_path_information

logger = logging.getLogger(__name__)


# TODO: add test that surrounding box is written to csv files


class TestUtils(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(file_path=__file__, dir_depth=5, code_base="mlcvzoo_base")

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(self.project_root, "config", "replacement_config.yaml"),
        )

        logger.debug(
            "Setup finished: \n"
            " - this_dir: %s\n"
            " - project_root: %s\n"
            " - code_root: %s\n"
            % (
                self.this_dir,
                self.project_root,
                self.code_root,
            )
        )

    def test_str_to_bool(self):
        # check that actual boolean values are accepted
        assert str2bool(True) is True
        assert str2bool(False) is False

        # check spelling variants of 'yes' are accepted
        assert str2bool("yes") is True
        assert str2bool("yeS") is True
        assert str2bool("yEs") is True
        assert str2bool("yES") is True
        assert str2bool("Yes") is True
        assert str2bool("YeS") is True
        assert str2bool("YEs") is True
        assert str2bool("YES") is True

        # check spelling variants of 'no' are accepted
        assert str2bool("no") is False
        assert str2bool("nO") is False
        assert str2bool("No") is False
        assert str2bool("NO") is False

        # check for single letter 't' for 'true'
        assert str2bool("t") is True
        assert str2bool("T") is True
        # check for single letter 'f' for 'false
        assert str2bool("f") is False
        assert str2bool("F") is False

        # check for single letter 'y' for 'yes'
        assert str2bool("y") is True
        assert str2bool("Y") is True
        # check for single letter 'n' for 'no'
        assert str2bool("n") is False
        assert str2bool("N") is False

        # check number representation of true
        assert str2bool("1") is True
        assert str2bool("1.0") is True

        # check number representations of false
        assert str2bool("0") is False
        assert str2bool("0.0") is False

        self.assertRaises(ArgumentTypeError, str2bool, "test")

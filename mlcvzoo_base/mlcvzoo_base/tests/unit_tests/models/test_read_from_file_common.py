# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.
import logging
import os
from typing import Dict
from unittest import TestCase, main

from related import to_model

from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.models.model_registry import ModelRegistry
from mlcvzoo_base.models.read_from_file.configuration import ReadFromFileConfig
from mlcvzoo_base.models.read_from_file.model import ReadFromFileModel
from mlcvzoo_base.utils.file_utils import get_project_path_information

logger = logging.getLogger(__name__)


class TestReadFromFileCommon(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(file_path=__file__, dir_depth=5, code_base="mlcvzoo_base")

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(self.project_root, "config", "replacement_config.yaml"),
        )

        logger.debug(
            "Setup finished: \n"
            f" - this_dir: {self.this_dir}\n"
            f" - project_root: {self.project_root}\n"
            f" - code_root: {self.code_root}\n"
        )

    def test_build_config_from_yaml(self) -> None:
        configuration = ReadFromFileModel.create_configuration(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_base",
                "mlcvzoo_base",
                "tests",
                "test_data",
                "test_ReadFromFileObjectDetectionModel",
                "read-from-file_cvat_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        assert configuration is not None

        config_dict = configuration.to_dict()

        configuration_2 = to_model(ReadFromFileConfig, config_dict)

        assert configuration_2 is not None

    def test_build_config(self) -> None:
        configuration = ReadFromFileModel.create_configuration(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_base",
                "mlcvzoo_base",
                "tests",
                "test_data",
                "test_ReadFromFileObjectDetectionModel",
                "read-from-file_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        print(configuration)

    def test_read_from_file_registry(self) -> None:
        model_registry = ModelRegistry()

        # We are testing mlcvzoo-base code here, but this code only
        # works when the mlcvzoo-mmocr package project is installed
        # in the python environment
        assert model_registry.config_registry["read_from_file_config"] == ReadFromFileConfig


if __name__ == "__main__":
    main()

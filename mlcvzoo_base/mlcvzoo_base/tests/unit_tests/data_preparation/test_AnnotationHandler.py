# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import hashlib
import logging
import os
import xml.etree.ElementTree as ET_xml
from typing import Dict, List
from unittest import TestCase, main

import cv2

from mlcvzoo_base.api.data.annotation import BaseAnnotation
from mlcvzoo_base.api.data.annotation_class_mapper import AnnotationClassMapper
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.classification import Classification
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_base.api.exceptions import ClassMappingNotFoundError
from mlcvzoo_base.configuration.class_mapping_config import (
    ClassMappingConfig,
    ClassMappingModelClassesConfig,
)
from mlcvzoo_base.configuration.reduction_mapping_config import (
    ReductionMappingConfig,
    ReductionMappingMappingConfig,
)
from mlcvzoo_base.configuration.structs import CSVFileNameIdentifier
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.data_preparation.annotation_handler import AnnotationHandler
from mlcvzoo_base.data_preparation.annotation_writer.csv_annotation_writer import (
    CSVAnnotationWriter,
)
from mlcvzoo_base.data_preparation.annotation_writer.cvat_annotation_writer import (
    CVATAnnotationWriter,
)
from mlcvzoo_base.data_preparation.structs import CSVOutputStringFormats
from mlcvzoo_base.utils.common_utils import get_current_timestamp
from mlcvzoo_base.utils.draw_utils import (
    draw_bbox_cv2,
    draw_on_image,
    generate_detector_colors,
)
from mlcvzoo_base.utils.file_utils import get_file_list, get_project_path_information

logger = logging.getLogger(__name__)

# TODO: add test for the filtering of a "BAD IMAGE" tag/box
# TODO: add test that surrounding box is written to csv files


def _md5sum(output_dir: str, image_sub_path: str) -> str:
    with open(os.path.join(output_dir, image_sub_path), "rb") as image_file:
        content = image_file.read()
        md5 = hashlib.md5(content).hexdigest()
        return md5


def _xml_equal(xml_path_1: str, xml_path_2: str) -> bool:
    """
    Helper for comparison of two XML trees

    Args:
        xml_path_1: path to one xml file
        xml_path_2: path to the other xml file

    Returns:
        True if xml trees are identical, else False.
    """

    tree_1 = ET_xml.parse(xml_path_1)
    root_1 = tree_1.getroot()

    tree_2 = ET_xml.parse(xml_path_2)
    root_2 = tree_2.getroot()

    return _xml_root_compare(root_1=root_1, root_2=root_2)


def _xml_root_compare(root_1: ET_xml.Element, root_2: ET_xml.Element) -> bool:
    """
    Recursive helper function that compares tags and attributes of a tree and
     calls itself for each child.
    Args:
        root_1: xml.etree.ElementTree.Element object
        root_2: xml.etree.ElementTree.Element object

    Returns:
        True if trees are identical, else False.

    """

    if root_1.tag == root_2.tag:
        # NOTE: The local and gitlab-ci "path" tag are different, but are
        #       not relevant for this test
        if root_1.tag == "path":
            result = True
        elif root_1.tag == root_2.tag and root_1.text == root_2.text:
            result = True

            for index, child_1 in enumerate(root_1):
                if len(root_2) > index:
                    child_2 = root_2[index]
                    result = result and _xml_root_compare(child_1, child_2)
                else:
                    # If both roots do have the same length, they are not equal
                    result = False
                    break
        else:
            result = False
    else:
        result = False

    return result


class TestAnnotationHandler(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(file_path=__file__, dir_depth=5, code_base="mlcvzoo_base")

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(self.project_root, "config", "replacement_config.yaml"),
        )

        logger.debug(
            "Setup finished: \n" " - this_dir: %s\n" " - project_root: %s\n" " - code_root: %s\n",
            self.this_dir,
            self.project_root,
            self.code_root,
        )

    def test_generate_csv(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST generation of csv:\n"
            "#      test_generate_csv(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_pascal-voc_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations: List[BaseAnnotation] = annotation_handler.parse_annotations_from_xml()

        annotation_handler.generate_csv(
            annotations=annotations, output_string_format=CSVOutputStringFormats.BASE
        )

        eval_string = (
            "IMAGE_DIR_0/dummy_task/person.jpg "
            "500 353 64,46,349,329,person 70,35,350,340,person"
        )

        train_string_0 = (
            "IMAGE_DIR_0/dummy_task/cars.jpg "
            "500 406 63,60,298,381,car 63,60,298,381,car 67,58,305,370,car"
        )
        train_string_1 = (
            "IMAGE_DIR_0/dummy_task/truck.jpg "
            "375 500 45,63,271,370,truck 45,63,271,369,truck 40,70,260,367,truck"
        )

        eval_path = os.path.join(self.project_root, "test_output", "test-task_eval.csv")
        train_path = os.path.join(self.project_root, "test_output", "test-task_train.csv")

        # CHECK eval file for correct content
        with open(eval_path, mode="r") as eval_file:
            eval_lines = eval_file.readlines()

        logger.info(
            f"==================================================================\n"
            "\n\neval_lines[0] %s\n"
            "eval_string %s\n\n"
            f"==================================================================\n",
            eval_lines[0].strip(),
            eval_string,
        )
        assert eval_lines[0].strip() == eval_string

        # CHECK train file for correct content
        with open(train_path, mode="r") as train_file:
            train_lines = train_file.readlines()

        logger.info(
            "==================================================================\n"
            "\n\ntrain_lines[0] %s\n"
            "train_string_0 %s\n\n"
            "==================================================================\n",
            train_lines[0].strip(),
            train_string_0,
        )
        assert train_lines[0].strip() == train_string_0

        logger.info(
            "==================================================================\n"
            "\n\ntrain_lines[1] %s\n"
            "train_string_1 %s\n\n"
            "==================================================================\n",
            train_lines[1].strip(),
            train_string_1,
        )
        assert train_lines[1].strip() == train_string_1

    def test_generate_csv_dont_use_occluded_and_difficult(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST generation of csv:\n"
            "#      test_generate_csv(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_coco_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations: List[BaseAnnotation] = annotation_handler.parse_annotations_from_coco()

        annotation_handler.generate_csv(
            annotations=annotations, output_string_format=CSVOutputStringFormats.BASE
        )

        val_string_0 = (
            "IMAGE_DIR_0/person.jpg 500 353 "
            "23,7,337,474,person 159,386,168,391,lp 73,252,221,340,person"
        )
        val_string_1 = (
            "IMAGE_DIR_0/cars.jpg 406 500 "
            "16,313,80,364,car 139,267,287,355,person 309,348,318,353,lp"
        )

        val_path = os.path.join(
            self.project_root,
            "test_output",
            "test-task_difficult-occluded_validation.csv",
        )

        # CHECK eval file for correct content
        with open(val_path, mode="r") as val_file:
            val_lines = val_file.readlines()

        logger.info(
            "==================================================================\n"
            "\n\ntrain_lines[0] %s\n"
            "train_string_0 %s\n\n"
            "==================================================================\n",
            val_lines[0].strip(),
            val_string_0,
        )
        assert val_lines[0].strip() == val_string_0

        logger.info(
            "==================================================================\n"
            "\n\nval_lines[1] %s\n"
            "val_string_1 %s\n\n"
            "==================================================================\n",
            val_lines[1].strip(),
            val_string_1,
        )
        assert val_lines[1].strip() == val_string_1

    def test_generate_csv_wrong_format(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST generation of csv:\n"
            "#      test_generate_csv(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_coco_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations: List[BaseAnnotation] = annotation_handler.parse_annotations_from_coco()

        with self.assertRaises(ValueError):
            annotation_handler.generate_csv(annotations=annotations, output_string_format="WRONG")

    def test_generate_darknet_train_set(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_generate-darknet_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations: List[BaseAnnotation] = annotation_handler.parse_annotations_from_xml()

        output_dir = os.path.join(self.project_root, "test_output", "darknet_train_set")

        annotation_handler.generate_darknet_train_set(
            annotations=annotations,
        )

        assert _md5sum(output_dir, "JPEGImages/cars.jpg") == "e1126d7955bf1691b8182b2170bf530c"
        assert _md5sum(output_dir, "JPEGImages/person.jpg") == "f5de025562070a71da4b791d52b04614"
        assert _md5sum(output_dir, "JPEGImages/truck.jpg") == "f4b1d605ec675098df2e39c19c93e720"
        with open(os.path.join(output_dir, "labels/cars.txt")) as cars_file:
            cars_lines = cars_file.readlines()

        assert cars_lines == [
            "2 0.4445812807881774 0.441 0.5788177339901478 0.642\n",
            "2 0.4445812807881774 0.441 0.5788177339901478 0.642\n",
            "2 0.458128078817734 0.428 0.5862068965517241 0.624\n",
        ]

        with open(os.path.join(output_dir, "labels/person.txt")) as person_file:
            person_lines = person_file.readlines()

        assert person_lines == [
            "0 0.584985835694051 0.375 0.8073654390934845 0.566\n",
            "0 0.594900849858357 0.375 0.7932011331444759 0.61\n",
        ]

        with open(os.path.join(output_dir, "labels/truck.txt")) as truck_file:
            truck_lines = truck_file.readlines()

        assert truck_lines == [
            "1 0.316 0.5773333333333334 0.452 0.8186666666666667\n",
            "1 0.316 0.576 0.452 0.816\n",
            "1 0.30000000000000004 0.5706666666666667 0.4 0.8213333333333334\n",
            "1 0.3 0.5826666666666667 0.44 0.792\n",
        ]

        line_count = 0

        test_file_path = (
            annotation_handler.configuration.write_output.darknet_train_set.get_test_file_path()
        )
        train_file_path = (
            annotation_handler.configuration.write_output.darknet_train_set.get_train_file_path()
        )

        with open(test_file_path, "r") as f:
            for _ in f:
                line_count += 1
        with open(train_file_path, "r") as f:
            for _ in f:
                line_count += 1
        assert line_count == 3

    def test_generate_yolo_csv(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST generation of txt:\n"
            "#      test_generate_txt(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_pascal-voc_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations: List[BaseAnnotation] = annotation_handler.parse_annotations_from_xml()

        annotation_handler.generate_csv(
            annotations=annotations, output_string_format=CSVOutputStringFormats.YOLO
        )

        eval_string = "IMAGE_DIR_0/dummy_task/person.jpg " "64,46,349,329,0 70,35,350,340,0"

        train_string_0 = (
            "IMAGE_DIR_0/dummy_task/cars.jpg " "63,60,298,381,2 63,60,298,381,2 67,58,305,370,2"
        )

        train_string_1 = (
            "IMAGE_DIR_0/dummy_task/truck.jpg " "45,63,271,370,1 45,63,271,369,1 40,70,260,367,1"
        )

        eval_path = os.path.join(self.project_root, "test_output", "test-task_eval.txt")

        train_path = os.path.join(self.project_root, "test_output", "test-task_train.txt")

        # CHECK eval file for correct content
        with open(eval_path, mode="r") as eval_file:
            eval_lines = eval_file.readlines()

        logger.info(
            "==================================================================\n"
            "\n\neval_lines[0] %s\n"
            "eval_string %s\n\n"
            "==================================================================\n",
            eval_lines[0].strip(),
            eval_string,
        )

        # CHECK train file for correct content
        with open(train_path, mode="r") as train_file:
            train_lines = train_file.readlines()

        logger.info(
            "==================================================================\n"
            "\n\ntrain_lines[0] %s\n"
            "train_string_0 %s\n\n"
            "==================================================================\n",
            train_lines[0].strip(),
            train_string_0,
        )
        assert train_lines[0].strip() == train_string_0

        logger.info(
            "==================================================================\n"
            "\n\ntrain_lines[1] %s\n"
            "train_string_1 %s\n\n"
            "==================================================================\n",
            train_lines[1].strip(),
            train_string_1,
        )
        assert train_lines[1].strip() == train_string_1

    def test_parse_annotations_from_csv(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from csv:\n"
            "#      test_parse_annotations_from_csv(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_pascal-voc_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        train_path = os.path.join(self.project_root, "test_output", "test-task_train.csv")

        annotations = annotation_handler.parse_annotations_from_csv(csv_file_path=train_path)

        assert annotations[0].image_shape == (500, 406)
        assert annotations[0].image_path == os.path.join(
            self.project_root,
            "test_data/images/dummy_task/cars.jpg",
        )
        assert annotations[0].image_dir == os.path.join(
            self.project_root,
            "test_data/images",
        )
        assert annotations[0].annotation_path == os.path.join(
            self.project_root,
            "test_data/annotations/pascal_voc/dummy_task/cars.xml",
        )
        assert annotations[0].annotation_dir == os.path.join(
            self.project_root,
            "test_data/annotations/pascal_voc",
        )

        assert annotations[0].bounding_boxes[0].class_id == 2
        assert annotations[0].bounding_boxes[0].class_name == "car"
        assert annotations[0].bounding_boxes[0].difficult is False
        assert annotations[0].bounding_boxes[0].score == 1.0

        assert annotations[0].bounding_boxes[0].box.xmin == 63
        assert annotations[0].bounding_boxes[0].box.ymin == 60
        assert annotations[0].bounding_boxes[0].box.xmax == 298
        assert annotations[0].bounding_boxes[0].box.ymax == 381

    def test_parse_from_pascal_voc_xml(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from pascal-voc xml:\n"
            "#      test_parse_from_pascal_voc_xml(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_pascal-voc_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # TODO: add detailed check for parsed annotations
        annotations = annotation_handler.parse_annotations_from_xml()

    def test_parse_from_pascal_voc_xml_ignore_missing_images(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from pascal-voc xml:\n"
            "#      test_parse_from_pascal_voc_xml(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_pascal-voc_test_ignore_missing_images.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # TODO: add detailed check for parsed annotations
        annotations = annotation_handler.parse_annotations_from_xml()

    def test_parse_from_alternate_coco(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from coco json:\n"
            "#      test_parse_from_alternate_coco(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_alternate_coco_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_coco()

        assert annotations[0].bounding_boxes[0].difficult is False
        assert annotations[0].bounding_boxes[0].occluded is False
        assert annotations[0].bounding_boxes[0].content == ""

    def test_parse_from_coco(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from coco json:\n"
            "#      test_parse_annotations_from_csv(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_coco_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # TODO: add detailed check for parsed annotations
        annotations = annotation_handler.parse_annotations_from_coco()

    def test_parse_from_label_studio(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from label studio json:\n"
            "#      test_parse_from_label_studio(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_label-studio_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_label_studio()

        annotations = sorted(annotations, key=lambda x: x.image_path)

        # TODO: add detailed check for parsed annotations
        assert len(annotations) == 3
        assert annotations[0].image_path == os.path.join(
            self.project_root, "test_data/images/dummy_task/cars.jpg"
        )
        assert annotations[1].image_path == os.path.join(
            self.project_root, "test_data/images/dummy_task/person.jpg"
        )
        assert annotations[2].image_path == os.path.join(
            self.project_root, "test_data/images/dummy_task/truck.jpg"
        )

    def test_parse_from_label_studio_errors(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from label studio json:\n"
            "#      test_parse_from_label_studio(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_label-studio_errors_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_label_studio()

        annotations = sorted(annotations, key=lambda x: x.image_path)

        # TODO: add detailed check for parsed annotations
        assert len(annotations) == 2
        assert annotations[0].image_path == os.path.join(
            self.project_root, "test_data/images/dummy_task/cars.jpg"
        )
        assert annotations[1].image_path == os.path.join(
            self.project_root, "test_data/images/dummy_task/cars.jpg"
        )

    def test_parse_from_coco_multiple_sources(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from multiple coco json:\n"
            "#      test_parse_from_coco_multiple_sources(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_coco_test_multiple_sources.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # TODO: add detailed check for parsed annotations
        annotations = annotation_handler.parse_annotations_from_coco()

        annotation_handler.generate_csv(
            annotations=annotations, output_string_format=CSVOutputStringFormats.BASE
        )

        timestamp_string = get_current_timestamp(
            annotation_handler.configuration.write_output.csv_annotation.timestamp_format
        )

        file_extension = ".csv"

        csv_base_file_name = CSVAnnotationWriter.get_csv_base_file_name(
            timestamp_string=timestamp_string,
            csv_annotation=annotation_handler.configuration.write_output.csv_annotation,
        )

        csv_file_path = CSVAnnotationWriter.generate_csv_path(
            csv_base_file_name=csv_base_file_name,
            file_extension=file_extension,
            file_identifier=CSVFileNameIdentifier.VALIDATION,
            csv_annotation=annotation_handler.configuration.write_output.csv_annotation,
        )

        annotations = annotation_handler.parse_annotations_from_csv(csv_file_path=csv_file_path)

        for index, annotation in enumerate(annotations):
            if not os.path.isfile(annotation.image_path):
                raise ValueError(
                    f"annotation.image_path '{annotation.image_path}' "
                    f"at index {index} parsed from '{csv_file_path}' does not exist"
                )

    def test_parse_from_cvat(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from cvat xml:\n"
            "#      test_parse_from_cvat(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_cvat_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        show_image = False

        # TODO: add detailed check for parsed annotations
        annotations = annotation_handler.parse_annotations_from_cvat()

    def test_parse_from_custom(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_mot-custom_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_mot()

        logger.debug("Parsed Annotation Object: %s", annotations)

        assert len(annotations[0].bounding_boxes) == 2
        assert annotations[0].bounding_boxes[0].class_name == "Person_Pedestrian"
        assert annotations[0].bounding_boxes[1].class_name == "Person_Pedestrian"
        assert len(annotations[1].bounding_boxes) == 1
        assert annotations[1].bounding_boxes[0].class_name == "Person_Pedestrian"
        assert len(annotations[2].bounding_boxes) == 2
        assert annotations[2].bounding_boxes[0].class_name == "Vehicle_Car"
        assert annotations[2].bounding_boxes[1].class_name == "Other_Reflection"

    def test_parse_from_mot2015(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_mot2015_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_mot()

        logger.debug("Parsed Annotation Object: %s", annotations)

        # TODO: use os.path
        # assert annotations[0].image_shape == (500, 353)
        assert annotations[0].image_path == os.path.join(
            self.project_root,
            "test_data/images/dummy_task/cars.jpg",
        )
        assert annotations[0].image_dir == os.path.join(
            self.project_root,
            "test_data/images/dummy_task",
        )
        assert annotations[0].annotation_path == os.path.join(
            self.project_root,
            "test_data/annotations/mot/mot_ground-truth_2015.txt",
        )
        assert annotations[0].annotation_dir == os.path.join(
            self.project_root,
            "test_data/annotations/mot",
        )

        assert len(annotations[0].bounding_boxes) == 2

        assert annotations[0].bounding_boxes[0].class_id == 1
        assert annotations[0].bounding_boxes[0].class_name == "pedestrian"
        assert annotations[0].bounding_boxes[0].difficult is False
        assert annotations[0].bounding_boxes[0].score == 1.0

        assert annotations[0].bounding_boxes[0].box.xmin == 50
        assert annotations[0].bounding_boxes[0].box.ymin == 200
        assert annotations[0].bounding_boxes[0].box.xmax == 250
        assert annotations[0].bounding_boxes[0].box.ymax == 400

        assert annotations[0].bounding_boxes[1].class_id == 1
        assert annotations[0].bounding_boxes[1].class_name == "pedestrian"
        assert annotations[0].bounding_boxes[1].difficult is False
        assert annotations[0].bounding_boxes[1].score == 1.0

        assert annotations[0].bounding_boxes[1].box.xmin == 100
        assert annotations[0].bounding_boxes[1].box.ymin == 150
        assert annotations[0].bounding_boxes[1].box.xmax == 200
        assert annotations[0].bounding_boxes[1].box.ymax == 450

        assert len(annotations[1].bounding_boxes) == 2

        assert annotations[1].bounding_boxes[0].class_id == 1
        assert annotations[1].bounding_boxes[0].class_name == "pedestrian"
        assert annotations[1].bounding_boxes[0].difficult is False
        assert annotations[1].bounding_boxes[0].score == 1.0

        assert annotations[1].bounding_boxes[0].box.xmin == 50
        assert annotations[1].bounding_boxes[0].box.ymin == 200
        assert annotations[1].bounding_boxes[0].box.xmax == 200
        assert annotations[1].bounding_boxes[0].box.ymax == 400

        assert annotations[1].bounding_boxes[1].class_id == 1
        assert annotations[1].bounding_boxes[0].class_name == "pedestrian"
        assert annotations[1].bounding_boxes[1].difficult is False
        assert annotations[1].bounding_boxes[1].score == 1.0

        assert annotations[1].bounding_boxes[1].box.xmin == 100
        assert annotations[1].bounding_boxes[1].box.ymin == 200
        assert annotations[1].bounding_boxes[1].box.xmax == 250
        assert annotations[1].bounding_boxes[1].box.ymax == 250

        assert len(annotations[2].bounding_boxes) == 1

        assert annotations[2].bounding_boxes[0].class_id == 1
        assert annotations[2].bounding_boxes[0].class_name == "pedestrian"
        assert annotations[2].bounding_boxes[0].difficult is False
        assert annotations[2].bounding_boxes[0].score == 1.0

        assert annotations[2].bounding_boxes[0].box.xmin == 100
        assert annotations[2].bounding_boxes[0].box.ymin == 100
        assert annotations[2].bounding_boxes[0].box.xmax == 200
        assert annotations[2].bounding_boxes[0].box.ymax == 400

    def test_parse_from_mot201617(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_mot201617_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_mot()

        logger.debug("Parsed Annotation Object: %s", annotations)

        # TODO: use os.path
        # assert annotations[0].image_shape == (500, 353)
        assert annotations[0].image_path == os.path.join(
            self.project_root,
            "test_data/images/dummy_task/cars.jpg",
        )
        assert annotations[0].image_dir == os.path.join(
            self.project_root,
            "test_data/images/dummy_task",
        )
        assert annotations[0].annotation_path == os.path.join(
            self.project_root,
            "test_data/annotations/mot/mot_ground-truth_201617.txt",
        )
        assert annotations[0].annotation_dir == os.path.join(
            self.project_root,
            "test_data/annotations/mot",
        )

        assert len(annotations[0].bounding_boxes) == 2

        assert annotations[0].bounding_boxes[0].class_id == 1
        assert annotations[0].bounding_boxes[0].class_name == "pedestrian"
        assert annotations[0].bounding_boxes[0].difficult is False
        assert annotations[0].bounding_boxes[0].score == 1.0

        assert annotations[0].bounding_boxes[0].box.xmin == 50
        assert annotations[0].bounding_boxes[0].box.ymin == 200
        assert annotations[0].bounding_boxes[0].box.xmax == 250
        assert annotations[0].bounding_boxes[0].box.ymax == 400

        assert annotations[0].bounding_boxes[1].class_id == 3
        assert annotations[0].bounding_boxes[1].class_name == "car"
        assert annotations[0].bounding_boxes[1].difficult is False
        assert annotations[0].bounding_boxes[1].score == 1.0

        assert annotations[0].bounding_boxes[1].box.xmin == 100
        assert annotations[0].bounding_boxes[1].box.ymin == 200
        assert annotations[0].bounding_boxes[1].box.xmax == 200
        assert annotations[0].bounding_boxes[1].box.ymax == 450

        assert len(annotations[1].bounding_boxes) == 2

        assert annotations[1].bounding_boxes[0].class_id == 1
        assert annotations[1].bounding_boxes[0].class_name == "pedestrian"
        assert annotations[1].bounding_boxes[0].difficult is False
        assert annotations[1].bounding_boxes[0].score == 1.0

        assert annotations[1].bounding_boxes[0].box.xmin == 50
        assert annotations[1].bounding_boxes[0].box.ymin == 200
        assert annotations[1].bounding_boxes[0].box.xmax == 200
        assert annotations[1].bounding_boxes[0].box.ymax == 400

        assert annotations[1].bounding_boxes[1].class_id == 1
        assert annotations[1].bounding_boxes[0].class_name == "pedestrian"
        assert annotations[1].bounding_boxes[1].difficult is False
        assert annotations[1].bounding_boxes[1].score == 1.0

        assert annotations[1].bounding_boxes[1].box.xmin == 100
        assert annotations[1].bounding_boxes[1].box.ymin == 200
        assert annotations[1].bounding_boxes[1].box.xmax == 250
        assert annotations[1].bounding_boxes[1].box.ymax == 250

        assert len(annotations[2].bounding_boxes) == 1

        assert annotations[2].bounding_boxes[0].class_id == 1
        assert annotations[2].bounding_boxes[0].class_name == "pedestrian"
        assert annotations[2].bounding_boxes[0].difficult is False
        assert (
            annotations[2].bounding_boxes[0].score == 1.0
        )  # ToDo check which score should be used

        assert annotations[2].bounding_boxes[0].box.xmin == 100
        assert annotations[2].bounding_boxes[0].box.ymin == 100
        assert annotations[2].bounding_boxes[0].box.xmax == 200
        assert annotations[2].bounding_boxes[0].box.ymax == 400

    def test_parse_from_mot2020(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_mot2020_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_mot()

        logger.debug("Parsed Annotation Object: %s", annotations)

        assert annotations[0].image_path == os.path.join(
            self.project_root,
            "test_data/images/dummy_task/cars.jpg",
        )
        assert annotations[0].image_dir == os.path.join(
            self.project_root,
            "test_data/images/dummy_task",
        )
        assert annotations[0].annotation_path == os.path.join(
            self.project_root,
            "test_data/annotations/mot/mot_ground-truth_2020.txt",
        )
        assert annotations[0].annotation_dir == os.path.join(
            self.project_root,
            "test_data/annotations/mot",
        )

        assert len(annotations[0].bounding_boxes) == 2

        assert annotations[0].bounding_boxes[0].class_id == 1
        assert annotations[0].bounding_boxes[0].class_name == "pedestrian"
        assert annotations[0].bounding_boxes[0].difficult is False
        assert (
            annotations[0].bounding_boxes[0].score == 1.0
        )  # ToDo check which score should be used

        assert annotations[0].bounding_boxes[0].box.xmin == 50
        assert annotations[0].bounding_boxes[0].box.ymin == 200
        assert annotations[0].bounding_boxes[0].box.xmax == 250
        assert annotations[0].bounding_boxes[0].box.ymax == 400

        assert annotations[0].bounding_boxes[1].class_id == 1
        assert annotations[0].bounding_boxes[1].class_name == "pedestrian"
        assert annotations[0].bounding_boxes[1].difficult is False
        assert (
            annotations[0].bounding_boxes[1].score == 1.0
        )  # ToDo check which score should be used

        assert annotations[0].bounding_boxes[1].box.xmin == 100
        assert annotations[0].bounding_boxes[1].box.ymin == 150
        assert annotations[0].bounding_boxes[1].box.xmax == 200
        assert annotations[0].bounding_boxes[1].box.ymax == 450

        assert len(annotations[1].bounding_boxes) == 1

        assert annotations[1].bounding_boxes[0].class_id == 1
        assert annotations[1].bounding_boxes[0].class_name == "pedestrian"
        assert annotations[1].bounding_boxes[0].difficult is False
        assert annotations[1].bounding_boxes[0].score == 1.0

        assert annotations[1].bounding_boxes[0].box.xmin == 50
        assert annotations[1].bounding_boxes[0].box.ymin == 200
        assert annotations[1].bounding_boxes[0].box.xmax == 200
        assert annotations[1].bounding_boxes[0].box.ymax == 400

        assert len(annotations[2].bounding_boxes) == 2

        assert annotations[2].bounding_boxes[0].class_id == 3  # ToDo insert correct class_id
        assert annotations[2].bounding_boxes[0].class_name == "car"
        assert annotations[2].bounding_boxes[0].difficult is False
        assert annotations[2].bounding_boxes[0].score == 1.0

        assert annotations[2].bounding_boxes[0].box.xmin == 100
        assert annotations[2].bounding_boxes[0].box.ymin == 100
        assert annotations[2].bounding_boxes[0].box.xmax == 200
        assert annotations[2].bounding_boxes[0].box.ymax == 400

        assert annotations[2].bounding_boxes[1].class_id == 10
        assert annotations[2].bounding_boxes[1].class_name == "occluderonground"
        assert annotations[2].bounding_boxes[1].difficult is False
        assert (
            annotations[2].bounding_boxes[1].score == 1.0
        )  # ToDo check which score should be used

        assert annotations[2].bounding_boxes[1].box.xmin == 100
        assert annotations[2].bounding_boxes[1].box.ymin == 300
        assert annotations[2].bounding_boxes[1].box.xmax == 250
        assert annotations[2].bounding_boxes[1].box.ymax == 350

    def test_parse_from_mot_invalid_format_in_yaml(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_mot-invalid-format_test.yaml",
        )

        with self.assertRaises(ValueError):
            AnnotationHandler(
                yaml_config_path=yaml_config_path,
                string_replacement_map=self.string_replacement_map,
            )

    def test_parse_from_mot_builder_invalid_format(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_mot2020_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )
        annotation_handler.configuration.mot_input_data[0].mot_format = "2020"

        with self.assertRaises(ValueError):
            annotation_handler.parse_annotations_from_mot()

    def test_parse_mot_with_mapping_error(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_mot_missing-class_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        with self.assertRaises(ClassMappingNotFoundError):
            annotation_handler.parse_annotations_from_mot()

    def test_parse_from_mot2020_no_gt(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_mot2020_no-gt-data_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_mot()

        logger.info("Parsed annotations: %s", annotations)

        # assert annotations[0].image_shape == (500, 353)
        assert annotations[0].image_path == os.path.join(
            self.project_root,
            "test_data/images/dummy_task/cars.jpg",
        )
        assert annotations[0].image_dir == os.path.join(
            self.project_root,
            "test_data/images/dummy_task",
        )
        assert annotations[0].annotation_path == os.path.join(
            self.project_root,
            "test_data/annotations/mot/mot_predictions_2020.txt",
        )
        assert annotations[0].annotation_dir == os.path.join(
            self.project_root,
            "test_data/annotations/mot",
        )

        assert len(annotations[0].bounding_boxes) == 1

        assert annotations[0].bounding_boxes[0].class_id == 1
        assert annotations[0].bounding_boxes[0].class_name == "pedestrian"
        assert annotations[0].bounding_boxes[0].difficult is False
        assert (
            annotations[0].bounding_boxes[0].score == 0.9
        )  # ToDo check which score should be used

        assert annotations[0].bounding_boxes[0].box.xmin == 50
        assert annotations[0].bounding_boxes[0].box.ymin == 200
        assert annotations[0].bounding_boxes[0].box.xmax == 250
        assert annotations[0].bounding_boxes[0].box.ymax == 400

        assert len(annotations[1].bounding_boxes) == 1

        assert annotations[1].bounding_boxes[0].class_id == 1
        assert annotations[1].bounding_boxes[0].class_name == "pedestrian"
        assert annotations[1].bounding_boxes[0].difficult is False
        assert annotations[1].bounding_boxes[0].score == 0.75

        assert annotations[1].bounding_boxes[0].box.xmin == 50
        assert annotations[1].bounding_boxes[0].box.ymin == 200
        assert annotations[1].bounding_boxes[0].box.xmax == 200
        assert annotations[1].bounding_boxes[0].box.ymax == 400

        assert len(annotations[2].bounding_boxes) == 1

        assert annotations[2].bounding_boxes[0].class_id == 3  # ToDo insert correct class_id
        assert annotations[2].bounding_boxes[0].class_name == "car"
        assert annotations[2].bounding_boxes[0].difficult is False
        assert annotations[2].bounding_boxes[0].score == 0.6

        assert annotations[2].bounding_boxes[0].box.xmin == 100
        assert annotations[2].bounding_boxes[0].box.ymin == 100
        assert annotations[2].bounding_boxes[0].box.xmax == 200
        assert annotations[2].bounding_boxes[0].box.ymax == 400

    def test_ignore_class_mot_annotation_parser(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_mot2020_ignore-class-names_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_mot()

        logger.info("Parsed annotations: %s", annotations)

        assert annotations[0].image_path == os.path.join(
            self.project_root,
            "test_data/images/dummy_task/cars.jpg",
        )
        assert annotations[0].image_dir == os.path.join(
            self.project_root,
            "test_data/images/dummy_task",
        )
        assert annotations[0].annotation_path == os.path.join(
            self.project_root,
            "test_data/annotations/mot/mot_ground-truth_2020.txt",
        )
        assert annotations[0].annotation_dir == os.path.join(
            self.project_root,
            "test_data/annotations/mot",
        )

        assert len(annotations) == 3

        assert len(annotations[0].bounding_boxes) == 0
        assert len(annotations[1].bounding_boxes) == 0
        assert len(annotations[2].bounding_boxes) == 2

        assert annotations[2].bounding_boxes[0].class_id == 3
        assert annotations[2].bounding_boxes[0].class_name == "car"
        assert annotations[2].bounding_boxes[0].difficult is False
        assert annotations[2].bounding_boxes[0].score == 1.0

        assert annotations[2].bounding_boxes[0].box.xmin == 100
        assert annotations[2].bounding_boxes[0].box.ymin == 100
        assert annotations[2].bounding_boxes[0].box.xmax == 200
        assert annotations[2].bounding_boxes[0].box.ymax == 400

        assert annotations[2].bounding_boxes[1].class_id == 10
        assert annotations[2].bounding_boxes[1].class_name == "occluderonground"
        assert annotations[2].bounding_boxes[1].difficult is False
        assert annotations[2].bounding_boxes[1].score == 1.0

        assert annotations[2].bounding_boxes[1].box.xmin == 100
        assert annotations[2].bounding_boxes[1].box.ymin == 300
        assert annotations[2].bounding_boxes[1].box.xmax == 250
        assert annotations[2].bounding_boxes[1].box.ymax == 350

    def test_parse_from_all_sources(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from all sources:\n"
            "#      test_parse_from_all_sources(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_all_sources_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # TODO: add detailed check for parsed annotations
        annotations = annotation_handler.parse_training_annotations()

    def test_write_to_cvat(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing to cvat xml:\n"
            "#      test_parse_to_cvat(self)\n"
            "############################################################"
        )
        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_cvat_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # compare initial file wth new file
        initial_cvat_xml_path = annotation_handler.configuration.cvat_input_data[0].input_path

        annotations = annotation_handler.parse_annotations_from_cvat()

        output_file_path = os.path.join(self.project_root, "test_output", "test_cvat.xml")

        cvat_annotation_writer = CVATAnnotationWriter(
            cvat_xml_input_path=initial_cvat_xml_path,
            clean_boxes=False,
            clean_segmentations=False,
            clean_tags=False,
            output_file_path=output_file_path,
        )

        cvat_annotation_writer.write(
            annotations=annotations,
        )

        # TODO: adjust equality test, what about undefined handling of attributes
        #  (eg difficult false)
        assert _xml_equal(xml_path_1=initial_cvat_xml_path, xml_path_2=output_file_path)

    def test_clean_write_to_cvat(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST clean parsing to cvat xml:\n"
            "#      test_parse_to_cvat(self)\n"
            "############################################################"
        )
        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_cvat_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # create new dummy annotations per known annotations
        annotations = annotation_handler.parse_annotations_from_cvat()

        from mlcvzoo_base.api.data.classification import Classification

        test_class_identifier = ClassIdentifier(
            class_id=0,
            class_name="test_class",
        )
        test_classifications = [
            Classification(
                class_identifier=test_class_identifier,
                model_class_identifier=test_class_identifier,
                score=1.0,
            )
        ]
        for annotation in annotations:
            annotation.bounding_boxes = []
            annotation.segmentations = []
            annotation.classifications = test_classifications

        output_file_path = os.path.join(self.project_root, "test_output", "test_clean_cvat.xml")

        cvat_annotation_writer = CVATAnnotationWriter(
            cvat_xml_input_path=annotation_handler.configuration.cvat_input_data[0].input_path,
            clean_boxes=True,
            clean_segmentations=True,
            clean_tags=True,
            output_file_path=output_file_path,
        )

        cvat_annotation_writer.write(
            annotations=annotations,
        )

        # compare initial file wth new file
        clean_cvat_xml_path = os.path.join(
            self.project_root,
            "test_data/annotations/cvat/cvat-for-clean-annotations.xml",
        )

        assert _xml_equal(xml_path_1=output_file_path, xml_path_2=clean_cvat_xml_path)

    def test_load_meta_info_from_cvat(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing meta info from cvat xml:\n"
            "#      test_parse_meta_info_from_cvat(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "mlcvzoo_base/mlcvzoo_base/tests/test_data",
            "test_AnnotationHandler",
            "annotation-handler_cvat_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        meta = annotation_handler.parse_meta_info_from_cvat()[0]
        tasks = meta.findall("task")
        dumped = meta.findall("dumped")

        assert len(tasks) == 1
        assert len(dumped) == 1

        labels = tasks[0].findall("labels")
        assert len(labels) == 1

        label_tags = labels[0].findall("label")
        assert len(label_tags) == 4

        segments = tasks[0].findall("segments")
        assert len(segments) == 1

        segment_tags = segments[0].findall("segment")
        assert len(segment_tags) == 1

    def test_reduce_annotations(self):
        annotations = [
            BaseAnnotation(
                image_path="TEST_PATH/test.jpg",
                classifications=[
                    Classification(
                        class_identifier=ClassIdentifier(
                            class_id=0,
                            class_name="person",
                        ),
                        score=0.7,
                    ),
                    Classification(
                        class_identifier=ClassIdentifier(
                            class_id=1,
                            class_name="car",
                        ),
                        score=0.7,
                    ),
                ],
                bounding_boxes=[
                    BoundingBox(
                        box=Box(xmin=0, ymin=0, xmax=100, ymax=100),
                        class_identifier=ClassIdentifier(
                            class_id=0,
                            class_name="person",
                        ),
                        score=0.7,
                        difficult=False,
                        occluded=False,
                        content="",
                    ),
                    BoundingBox(
                        box=Box(xmin=0, ymin=0, xmax=100, ymax=100),
                        class_identifier=ClassIdentifier(
                            class_id=1,
                            class_name="car",
                        ),
                        score=0.8,
                        difficult=False,
                        occluded=False,
                        content="",
                    ),
                ],
                segmentations=[
                    Segmentation(
                        polygon=[(0, 0), (100, 0), (100, 100), (0, 100)],
                        box=Box(xmin=0, ymin=0, xmax=100, ymax=100),
                        class_identifier=ClassIdentifier(
                            class_id=0,
                            class_name="person",
                        ),
                        score=0.8,
                        difficult=False,
                        occluded=False,
                        content="",
                    ),
                    Segmentation(
                        polygon=[(0, 0), (100, 0), (100, 100), (0, 100)],
                        box=Box(xmin=0, ymin=0, xmax=100, ymax=100),
                        class_identifier=ClassIdentifier(
                            class_id=1,
                            class_name="car",
                        ),
                        score=0.7,
                        difficult=False,
                        occluded=False,
                        content="",
                    ),
                ],
                annotation_path="TEST_PATH/test.xml",
                image_shape=(1, 1),
                image_dir="",
                annotation_dir="",
                replacement_string="",
            )
        ]

        reduced_annotations = AnnotationHandler.reduce_annotations(
            annotations=annotations,
            mapper=AnnotationClassMapper(
                class_mapping=ClassMappingConfig(
                    mapping=[],
                    model_classes=[
                        ClassMappingModelClassesConfig(class_id=0, class_name="person"),
                        ClassMappingModelClassesConfig(class_id=1, class_name="car"),
                    ],
                    number_model_classes=2,
                ),
                reduction_mapping=ReductionMappingConfig(
                    mapping=[
                        ReductionMappingMappingConfig(
                            model_class_ids=[0],
                            output_class_id=10,
                            output_class_name="human",
                        )
                    ]
                ),
            ),
        )

        # Check classifications
        assert reduced_annotations[0].classifications[0].class_id == 10
        assert reduced_annotations[0].classifications[0].class_name == "human"
        assert reduced_annotations[0].classifications[1].class_id == 1
        assert reduced_annotations[0].classifications[1].class_name == "car"

        # Check bounding boxes
        assert reduced_annotations[0].bounding_boxes[0].class_id == 10
        assert reduced_annotations[0].bounding_boxes[0].class_name == "human"
        assert reduced_annotations[0].bounding_boxes[1].class_id == 1
        assert reduced_annotations[0].bounding_boxes[1].class_name == "car"

        # Check segmentations
        assert reduced_annotations[0].segmentations[0].class_id == 10
        assert reduced_annotations[0].segmentations[0].class_name == "human"
        assert reduced_annotations[0].segmentations[1].class_id == 1
        assert reduced_annotations[0].segmentations[1].class_name == "car"


if __name__ == "__main__":
    main()

# Contributing to MLCVZoo

All kinds of contributions are welcome, including but not limited to the following:

- Bug fixes
- Refactoring
- Add new features

## Workflow

1. Pull the latest main
2. Checkout new branch
3. Create merge request into the main branch. Please use the default template merge request template.
   - Mark your MR as draft until it is ready to be merged
   - Please put yourself as assignee and a maintainer as reviewer
   - Follow the acceptance criteria of the merge request template

## Code Style:

We adopt [PEP8](https://www.python.org/dev/peps/pep-0008/) as the preferred code style.

We use the following tools for linting, formatting and type checking:

- [pylint](https://github.com/PyCQA/pylint): It's not just a linter that annoys you!
- [isort](https://github.com/timothycrosley/isort): A Python utility / library to sort imports
- [black](https://github.com/psf/black): The Uncompromising Code Formatter
- [mypy](https://github.com/python/mypy): Optional static typing for Python


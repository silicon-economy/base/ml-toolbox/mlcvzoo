# MLCVZoo Darknet

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_darknet** is the wrapper module for the
[darknet framework](https://github.com/AlexeyAB/darknet).

Further information about the MLCVZoo can be found [here](../README.md).

## Install
`
pip install mlcvzoo-darknet
`

## Technology stack

- Python

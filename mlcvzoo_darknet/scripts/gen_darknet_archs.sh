#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 1
fi

ARG_LIST=" "
for i in ${1//;/ }; do
	GEN=$(echo "$i" | sed 's/\.//g')
	if [ "x$GEN" = "x61" ]; then
	  ARG_LIST="$ARG_LIST -gencode arch=compute_$GEN,code=sm_$GEN -gencode arch=compute_$GEN,code=compute_$GEN"
	else
    ARG_LIST="$ARG_LIST -gencode arch=compute_$GEN,code=[sm_$GEN,compute_$GEN]"
  fi
done
echo $ARG_LIST

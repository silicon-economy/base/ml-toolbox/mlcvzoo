# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.
import logging
import os
import shutil
from typing import Dict
from unittest import TestCase

import cv2
import torch.cuda
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information

from mlcvzoo_yolox.model import YOLOXModel

logger = logging.getLogger(__name__)


class TestYoloxLModelTRT(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=3, code_base="mlcvzoo_yolox"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

    def tearDown(self) -> None:
        shutil.rmtree(
            path=os.path.join(self.project_root, "test_output"), ignore_errors=True
        )

    def test_yolox_l_coco_trt_inference_with_conversion(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST yolox model inference with TensorRT conversion:\n"
            "#      test_yolox_l_coco_trt_inference_with_conversion(self)\n"
            "############################################################"
        )

        trt_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_l_coco_trt_conversion_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
            load_tensorrt_model=False,
        )
        try:
            trt_model.convert_to_tensorrt()

            del trt_model.net
            trt_model.net = None

            torch.cuda.empty_cache()

            trt_model.load_tensorrt_model = True
            trt_model.restore(
                checkpoint_path=trt_model.configuration.trt_config.trt_checkpoint_path
            )

            test_image_path = os.path.join(
                self.project_root,
                "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
            )
            test_image = cv2.imread(test_image_path)

            _, bounding_boxes = trt_model.predict(data_item=test_image)

            logger.debug(
                "\n==============================================================\n"
                "PREDICTED BOUNDING-BOXES: \n"
                f" {bounding_boxes}\n"
                "\n==============================================================\n"
            )

            if len(bounding_boxes) == 0:
                raise ValueError("No bounding boxes have been predicted!")

            expected_bounding_box_0 = BoundingBox(
                box=Box(xmin=258, ymin=719, xmax=1976, ymax=2148),
                class_identifier=ClassIdentifier(
                    class_id=41,
                    class_name="cup",
                ),
                score=0.9604013562202454,
                difficult=False,
                occluded=False,
                content="",
            )

            is_correct_0 = (
                expected_bounding_box_0.box == bounding_boxes[0].box
                and expected_bounding_box_0.class_id == bounding_boxes[0].class_id
                and expected_bounding_box_0.class_name == bounding_boxes[0].class_name
            )

            if not is_correct_0:
                logger.error(
                    "Found wrong bounding_box: \n"
                    f"  Expected 0:  {expected_bounding_box_0}\n"
                    f"  Predicted 0: {bounding_boxes[0]}\n"
                )

                raise ValueError("Output is not valid!")
        except ModuleNotFoundError as me:
            if "No module named 'torch2trt'" in str(me):
                self.skipTest(
                    "Could not test conversion of yolox to TensorRT "
                    "because torch2trt is not installed"
                )
            else:
                raise me
        except RuntimeError as re:
            if "CUDA out of memory" in str(re):
                self.skipTest(
                    "Could not test training of yolox. GPU memory is to small"
                )
            else:
                raise re

    def test_yolox_l_coco_trt_inference_with_preconverted(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST yolox model inference with preconverted TensorRT:\n"
            "#      test_yolox_l_coco_trt_inference_with_preconverted(self)\n"
            "############################################################"
        )

        try:
            trt_model = YOLOXModel(
                from_yaml=os.path.join(
                    self.project_root,
                    "mlcvzoo_yolox/mlcvzoo_yolox/"
                    "tests/test_data/test_yolox/yolox_l_coco_trt_preconverted_test.yaml",
                ),
                string_replacement_map=self.string_replacement_map,
                init_for_inference=True,
                load_tensorrt_model=True,
            )

            test_image_path = os.path.join(
                self.project_root,
                "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
            )
            test_image = cv2.imread(test_image_path)

            _, bounding_boxes = trt_model.predict(data_item=test_image)

            logger.debug(
                "\n==============================================================\n"
                "PREDICTED BOUNDING-BOXES: \n"
                f" {bounding_boxes}\n"
                "\n==============================================================\n"
            )

            if len(bounding_boxes) == 0:
                raise ValueError("No bounding boxes have been predicted!")

            expected_bounding_box_0 = BoundingBox(
                box=Box(xmin=258, ymin=719, xmax=1976, ymax=2148),
                class_identifier=ClassIdentifier(
                    class_id=41,
                    class_name="cup",
                ),
                score=0.9604013562202454,
                difficult=False,
                occluded=False,
                content="",
            )

            is_correct_0 = (
                expected_bounding_box_0.box == bounding_boxes[0].box
                and expected_bounding_box_0.class_id == bounding_boxes[0].class_id
                and expected_bounding_box_0.class_name == bounding_boxes[0].class_name
            )

            if not is_correct_0:
                logger.error(
                    "Found wrong bounding_box: \n"
                    f"  Expected 0:  {expected_bounding_box_0}\n"
                    f"  Predicted 0: {bounding_boxes[0]}\n"
                )

                raise ValueError("Output is not valid!")
        except ModuleNotFoundError as me:
            if "No module named 'torch2trt'" in str(me):
                self.skipTest(
                    "Could not test conversion of yolox to TensorRT "
                    "because torch2trt is not installed"
                )
            else:
                raise me

# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.
import logging
import os
import shutil
from typing import Dict
from unittest import TestCase

from mlcvzoo_base.api.data.annotation_class_mapper import AnnotationClassMapper
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information

from mlcvzoo_yolox.exp.custom_yolox_exp import CustomYOLOXExp
from mlcvzoo_yolox.model import YOLOXModel

logger = logging.getLogger(__name__)


class TestYoloxModelTRT(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=3, code_base="mlcvzoo_yolox"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

    def tearDown(self) -> None:
        shutil.rmtree(
            path=os.path.join(self.project_root, "test_output"), ignore_errors=True
        )

    def test_yolox_coco_trt_inference_with_conversion_no_net_error(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST yolox model inference with conversion no net error:\n"
            "#      test_yolox_coco_trt_inference_with_conversion_no_net_error(self)\n"
            "############################################################"
        )

        trt_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_x_coco_trt_conversion_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
            load_tensorrt_model=False,
        )

        trt_model.net = None

        with self.assertRaises(ValueError):
            trt_model.convert_to_tensorrt()

    def test_yolox_coco_trt_load_no_gpu(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST throws ValueError when loading trt model without gpu:\n"
            "#      test_yolox_coco_trt_load_no_gpu(self)\n"
            "############################################################"
        )

        trt_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_x_coco_trt_conversion_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
            load_tensorrt_model=False,
        )

        with self.assertRaises(ValueError):
            trt_model.configuration.inference_config.device = "cpu"
            trt_model.load_tensorrt_model = True

    def test_yolox_coco_trt_init_inference_no_config(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST yolox trt inference without config:\n"
            "#      test_yolox_coco_trt_init_inference_no_config(self)\n"
            "############################################################"
        )

        with self.assertRaises(ValueError):
            YOLOXModel(
                from_yaml=os.path.join(
                    self.project_root,
                    "mlcvzoo_yolox/mlcvzoo_yolox/"
                    "tests/test_data/test_yolox/"
                    "yolox_x_coco_trt_no_config.yaml",
                ),
                string_replacement_map=self.string_replacement_map,
                init_for_inference=True,
                load_tensorrt_model=True,
            )

    def test_yolox_coco_trt_init_inference_no_checkpoint(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST yolox trt inference without checkpoint:\n"
            "#      test_yolox_coco_trt_init_inference_no_checkpoint(self)\n"
            "############################################################"
        )

        with self.assertRaises(ValueError):
            YOLOXModel(
                from_yaml=os.path.join(
                    self.project_root,
                    "mlcvzoo_yolox/mlcvzoo_yolox/"
                    "tests/test_data/test_yolox/"
                    "yolox_x_coco_trt_no_checkpoint.yaml",
                ),
                string_replacement_map=self.string_replacement_map,
                init_for_inference=True,
                load_tensorrt_model=True,
            )

    def test_yolox_init_experiment_no_training_config(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST yolox initializatino without training config:\n"
            "#      test_yolox_init_experiment_no_training_config(self)\n"
            "############################################################"
        )

        configuration = YOLOXModel.create_configuration(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/"
                "yolox_tiny_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        exp = CustomYOLOXExp(
            configuration=configuration,
            mapper=AnnotationClassMapper(
                class_mapping=configuration.class_mapping,
                reduction_mapping=configuration.inference_config.reduction_class_mapping,
            ),
        )

        with self.assertRaises(ValueError):
            exp.get_data_loader(batch_size=2, is_distributed=False)
        with self.assertRaises(ValueError):
            exp.get_eval_loader(batch_size=2, is_distributed=False)

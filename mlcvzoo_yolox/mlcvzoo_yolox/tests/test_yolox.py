# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.
import logging
import os
import shutil
from typing import Dict, cast
from unittest import TestCase

import cv2
import torch
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information
from related import to_model

from mlcvzoo_yolox.configuration import YOLOXConfig
from mlcvzoo_yolox.evaluators.evaluator import MLCVZooEvaluator
from mlcvzoo_yolox.model import YOLOXModel

logger = logging.getLogger(__name__)


class TestYoloxModel(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=3, code_base="mlcvzoo_yolox"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

    def tearDown(self) -> None:
        shutil.rmtree(
            path=os.path.join(self.project_root, "test_output"), ignore_errors=True
        )

    def test_num_classes(self):
        yolox_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_tiny_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        assert yolox_model.num_classes == 80

    def test_configuration(self):
        configuration = YOLOXModel.create_configuration(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_tiny_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        assert configuration is not None

        configuration_2 = cast(
            YOLOXConfig, to_model(YOLOXConfig, configuration.to_dict())
        )
        assert configuration_2 is not None

    def test_yolox_tiny_coco_inference(self) -> None:
        yolox_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_tiny_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        test_image_path = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )

        test_image = cv2.imread(test_image_path)

        _, bounding_boxes = yolox_model.predict(data_item=test_image)

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED BOUNDING-BOXES: \n"
            " %s\n"
            "\n==============================================================\n",
            bounding_boxes,
        )

        if len(bounding_boxes) == 0:
            raise ValueError("No bounding boxes have been predicted!")

        expected_bounding_box_0 = BoundingBox(
            box=Box(xmin=201, ymin=684, xmax=1973, ymax=2158),
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.801031768321991,
            difficult=False,
            occluded=False,
            content="",
        )

        is_correct_0 = (
            expected_bounding_box_0.box == bounding_boxes[0].box
            and expected_bounding_box_0.class_id == bounding_boxes[0].class_id
            and expected_bounding_box_0.class_name == bounding_boxes[0].class_name
        )

        if not is_correct_0:
            logger.error(
                "Found wrong bounding_box: \n"
                "  Expected 0:  %s\n"
                "  Predicted 0: %s\n",
                expected_bounding_box_0,
                bounding_boxes[0],
            )

            raise ValueError("Output is not valid!")

    def test_yolox_s_coco_inference(self) -> None:
        yolox_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_s_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        test_image_path = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )

        test_image = cv2.imread(test_image_path)

        _, bounding_boxes = yolox_model.predict(data_item=test_image)

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED BOUNDING-BOXES: \n"
            " %s\n"
            "\n==============================================================\n",
            bounding_boxes,
        )

        if len(bounding_boxes) == 0:
            raise ValueError("No bounding boxes have been predicted!")

        expected_bounding_box_0 = BoundingBox(
            box=Box(xmin=234, ymin=678, xmax=1960, ymax=2125),
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.9225202202796936,
            difficult=False,
            occluded=False,
            content="",
        )

        is_correct_0 = (
            expected_bounding_box_0.box == bounding_boxes[0].box
            and expected_bounding_box_0.class_id == bounding_boxes[0].class_id
            and expected_bounding_box_0.class_name == bounding_boxes[0].class_name
        )

        if not is_correct_0:
            logger.error(
                "Found wrong bounding_box: \n"
                "  Expected 0:  %s\n"
                "  Predicted 0: %s\n",
                expected_bounding_box_0,
                bounding_boxes[0],
            )

            raise ValueError("Output is not valid!")

    def test_yolox_m_coco_inference(self) -> None:
        yolox_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_m_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        test_image_path = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )

        test_image = cv2.imread(test_image_path)

        _, bounding_boxes = yolox_model.predict(data_item=test_image)

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED BOUNDING-BOXES: \n"
            " %s\n"
            "\n==============================================================\n",
            bounding_boxes,
        )

        if len(bounding_boxes) == 0:
            raise ValueError("No bounding boxes have been predicted!")

        expected_bounding_box_0 = BoundingBox(
            box=Box(xmin=223, ymin=705, xmax=1975, ymax=2160),
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.9496504068374634,
            difficult=False,
            occluded=False,
            content="",
        )

        is_correct_0 = (
            expected_bounding_box_0.box == bounding_boxes[0].box
            and expected_bounding_box_0.class_id == bounding_boxes[0].class_id
            and expected_bounding_box_0.class_name == bounding_boxes[0].class_name
        )

        if not is_correct_0:
            logger.error(
                "Found wrong bounding_box: \n"
                "  Expected 0:  %s\n"
                "  Predicted 0: %s\n",
                expected_bounding_box_0,
                bounding_boxes[0],
            )

            raise ValueError("Output is not valid!")

    def test_yolox_l_coco_inference(self) -> None:
        yolox_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_l_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        test_image_path = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )

        test_image = cv2.imread(test_image_path)

        _, bounding_boxes = yolox_model.predict(data_item=test_image)

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED BOUNDING-BOXES: \n"
            " %s\n"
            "\n==============================================================\n",
            bounding_boxes,
        )

        if len(bounding_boxes) == 0:
            raise ValueError("No bounding boxes have been predicted!")

        expected_bounding_box_0 = BoundingBox(
            box=Box(xmin=258, ymin=719, xmax=1976, ymax=2148),
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.9604013562202454,
            difficult=False,
            occluded=False,
            content="",
        )

        is_correct_0 = (
            expected_bounding_box_0.box == bounding_boxes[0].box
            and expected_bounding_box_0.class_id == bounding_boxes[0].class_id
            and expected_bounding_box_0.class_name == bounding_boxes[0].class_name
        )

        if not is_correct_0:
            logger.error(
                "Found wrong bounding_box: \n"
                "  Expected 0:  %s\n"
                "  Predicted 0: %s\n",
                expected_bounding_box_0,
                bounding_boxes[0],
            )

            raise ValueError("Output is not valid!")

    def test_yolox_x_coco_inference(self) -> None:
        yolox_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_x_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        test_image_path = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )

        test_image = cv2.imread(test_image_path)

        _, bounding_boxes = yolox_model.predict(data_item=test_image)

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED BOUNDING-BOXES: \n"
            " %s\n"
            "\n==============================================================\n",
            bounding_boxes,
        )

        if len(bounding_boxes) == 0:
            raise ValueError("No bounding boxes have been predicted!")

        expected_bounding_box_0 = BoundingBox(
            box=Box(xmin=255, ymin=718, xmax=1993, ymax=2183),
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.9583499431610107,
            difficult=False,
            occluded=False,
            content="",
        )

        is_correct_0 = (
            expected_bounding_box_0.box == bounding_boxes[0].box
            and expected_bounding_box_0.class_id == bounding_boxes[0].class_id
            and expected_bounding_box_0.class_name == bounding_boxes[0].class_name
        )

        if not is_correct_0:
            logger.error(
                "Found wrong bounding_box: \n"
                "  Expected 0:  %s\n"
                "  Predicted 0: %s\n",
                expected_bounding_box_0,
                bounding_boxes[0],
            )

            raise ValueError("Output is not valid!")

    def test_yolox_ensure_num_classes_overwrite(self) -> None:
        # NOTE:
        # We simply want to test that the model is loaded with the
        # correct number of classes. Therefore, we expect the error
        # beneath, which simply states that a checkpoint from an
        # 80-class model can not be loaded in a 1-class model
        try:
            YOLOXModel(
                from_yaml=os.path.join(
                    self.project_root,
                    "mlcvzoo_yolox/mlcvzoo_yolox/"
                    "tests/test_data/test_yolox/yolox_x_custom_test.yaml",
                ),
                string_replacement_map=self.string_replacement_map,
                init_for_inference=True,
            )
        except RuntimeError as runtime_error:
            assert str(runtime_error) == (
                "Error(s) in loading state_dict for YOLOX:\n"
                "\tsize mismatch for head.cls_preds.0.weight: copying a param with shape "
                "torch.Size([80, 320, 1, 1]) from checkpoint, the shape in current model is "
                "torch.Size([3, 320, 1, 1]).\n"
                "\tsize mismatch for head.cls_preds.0.bias: copying a param with shape "
                "torch.Size([80]) from checkpoint, the shape in current model is "
                "torch.Size([3]).\n"
                "\tsize mismatch for head.cls_preds.1.weight: copying a param with shape "
                "torch.Size([80, 320, 1, 1]) from checkpoint, the shape in current model is "
                "torch.Size([3, 320, 1, 1]).\n"
                "\tsize mismatch for head.cls_preds.1.bias: copying a param with shape "
                "torch.Size([80]) from checkpoint, the shape in current model is "
                "torch.Size([3]).\n"
                "\tsize mismatch for head.cls_preds.2.weight: copying a param with shape "
                "torch.Size([80, 320, 1, 1]) from checkpoint, the shape in current model is "
                "torch.Size([3, 320, 1, 1]).\n"
                "\tsize mismatch for head.cls_preds.2.bias: copying a param with shape "
                "torch.Size([80]) from checkpoint, the shape in current model is "
                "torch.Size([3])."
            )

    def test_yolox_nano_coco_inference(self) -> None:
        yolox_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_nano_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        test_image_path = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )

        test_image = cv2.imread(test_image_path)

        _, bounding_boxes = yolox_model.predict(data_item=test_image)

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED BOUNDING-BOXES: \n"
            " %s\n"
            "\n==============================================================\n",
            bounding_boxes,
        )

        if len(bounding_boxes) == 0:
            raise ValueError("No bounding boxes have been predicted!")

        expected_bounding_box_0 = BoundingBox(
            box=Box(xmin=294, ymin=671, xmax=1998, ymax=2206),
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.898169994354248,
            difficult=False,
            occluded=False,
            content="",
        )

        is_correct_0 = (
            expected_bounding_box_0.box == bounding_boxes[0].box
            and expected_bounding_box_0.class_id == bounding_boxes[0].class_id
            and expected_bounding_box_0.class_name == bounding_boxes[0].class_name
        )

        if not is_correct_0:
            logger.error(
                "Found wrong bounding_box: \n"
                "  Expected 0:  %s\n"
                "  Predicted 0: %s\n",
                expected_bounding_box_0,
                bounding_boxes[0],
            )

            raise ValueError("Output is not valid!")

    def test_yolox_s_multi_gpu_training(self) -> None:
        if torch.cuda.device_count() < 2:
            self.skipTest(
                f"Could not test multi gpu training of yolox. Available GPU devices: {torch.cuda.device_count()} / 2+"
            )

        yolox_configuration = YOLOXModel.create_configuration(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_s_multi_gpu_training_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        yolox_model = YOLOXModel(
            configuration=yolox_configuration,
            init_for_inference=False,
        )

        try:
            yolox_model.train()
        except RuntimeError as re:
            if "CUDA out of memory" in str(re):
                self.skipTest(
                    "Could not test training of yolox. GPU memory is to small"
                )
            else:
                raise re

        assert os.path.isfile(
            os.path.join(
                self.project_root,
                "test_output/yolox/yolox_s_multi_gpu_training_test/yolox_s_multi_gpu_training_test_8_ckpt.pth",
            )
        )

        # Initialize a model using the generated checkpoint of the training
        yolox_model = YOLOXModel(
            configuration=yolox_configuration,
            init_for_inference=True,
        )

        _, bounding_boxes = yolox_model.predict(
            data_item=os.path.join(
                self.project_root, "test_data/images/dummy_task/cars.jpg"
            )
        )

        assert bounding_boxes == []

    def test_yolox_tiny_custom_training(self) -> None:
        yolox_configuration = YOLOXModel.create_configuration(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_tiny_custom_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        yolox_model = YOLOXModel(
            configuration=yolox_configuration,
            init_for_inference=False,
        )

        try:
            yolox_model.train()
        except RuntimeError as re:
            if "CUDA out of memory" in str(re):
                self.skipTest(
                    "Could not test training of yolox. GPU memory is to small"
                )
            else:
                raise re

        assert os.path.isfile(
            os.path.join(
                self.project_root,
                "test_output/yolox/yolox_tiny_custom_test/yolox_tiny_custom_test_8_ckpt.pth",
            )
        )

        # Initialize a model using the generated checkpoint of the training
        yolox_model = YOLOXModel(
            configuration=yolox_configuration,
            init_for_inference=True,
        )

        _, bounding_boxes = yolox_model.predict(
            data_item=os.path.join(
                self.project_root, "test_data/images/dummy_task/cars.jpg"
            )
        )

        assert bounding_boxes == []

    def test_yolox_tiny_custom_evaluator(self) -> None:
        yolox_configuration = YOLOXModel.create_configuration(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_tiny_coco_test_eval.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        yolox_model = YOLOXModel(
            configuration=yolox_configuration,
            init_for_inference=True,
        )

        evaluator = yolox_model.exp.get_evaluator(batch_size=1, is_distributed=False)

        ap50_95, ap50, _ = yolox_model.exp.eval(
            yolox_model.get_net(), evaluator, is_distributed=False
        )

        assert ap50 == 0.0
        assert ap50_95 == 0.0

    def test_yolox_coco_wrong_exp_type(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST inference of yolox model trained on coco-data :\n"
            "#      test_yolox_coco_inference(self)\n"
            "############################################################"
        )

        with self.assertRaises(ValueError) as ve:
            YOLOXModel(
                from_yaml=os.path.join(
                    self.project_root,
                    "mlcvzoo_yolox/mlcvzoo_yolox/"
                    "tests/test_data/test_yolox/yolox_x_coco_test_wrong_exp_type.yaml",
                ),
                string_replacement_map=self.string_replacement_map,
                init_for_inference=True,
            )
            logger.info("test_yolox_coco_wrong_exp_type has raised: %s\n" % ve)

    def test_yolox_coco_wrong_overwrite_attribute(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST inference of yolox model trained on coco-data :\n"
            "#      test_yolox_coco_inference(self)\n"
            "############################################################"
        )

        with self.assertRaises(ValueError) as ve:
            YOLOXModel(
                from_yaml=os.path.join(
                    self.project_root,
                    "mlcvzoo_yolox/mlcvzoo_yolox/"
                    "tests/test_data/test_yolox/yolox_x_coco_test_wrong_overwrite_attribute.yaml",
                ),
                string_replacement_map=self.string_replacement_map,
                init_for_inference=True,
            )
            logger.info(
                "test_yolox_coco_wrong_overwrite_attribute has raised: %s\n" % ve
            )

    def test_yolox_train_no_config_error(self):
        yolox_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_tiny_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=False,
        )

        with self.assertRaises(ValueError):
            yolox_model.train()

    def test_yolox_inference_net_not_initialized_error(self):
        yolox_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_tiny_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=False,
        )

        with self.assertRaises(ValueError):
            yolox_model.predict(
                data_item=os.path.join(
                    self.project_root,
                    "test_data/images/test_inference_task/"
                    "test_object-detection_inference_image.jpg",
                )
            )

    def test_yolox_restore_net_not_initialized_error(self):
        yolox_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_tiny_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=False,
        )

        with self.assertRaises(ValueError):
            yolox_model.restore(
                checkpoint_path=yolox_model.configuration.inference_config.checkpoint_path
            )

    def test_yolox_local_checkpoint_not_exist_error(self):
        yolox_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_tiny_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        with self.assertRaises(ValueError):
            yolox_model.restore(checkpoint_path="NO_PATH")

    def test_yolox_get_net(self):
        yolox_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_tiny_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        assert yolox_model.get_net() is not None

    def test_yolox_get_classes_id_dict(self):
        yolox_model = YOLOXModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_yolox/mlcvzoo_yolox/"
                "tests/test_data/test_yolox/yolox_tiny_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        assert yolox_model.get_net() is not None

#!/bin/sh

if [ $# -lt 1 ]; then
  echo "Usage: $0 POETRY_ARGS"
  return 1
fi

# Install yolox afterwards as it fails in poetry due to pep517, and others fail without it:
poetry "$@" && poetry run pip install \
  --no-deps \
  --no-build-isolation \
  --no-use-pep517 \
  "git+https://github.com/Megvii-BaseDetection/YOLOX.git@419778480ab6ec0590e5d3831b3afb3b46ab2aa3"

# MLCVZoo YOLOX

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_yolox** is the wrapper module for
the [yolox Object Detector](https://github.com/Megvii-BaseDetection/YOLOX).

Further information about the MLCVZoo can be found [here](../README.md).

## Install
`
pip install mlcvzoo-yolox
`

## Technology stack

- Python

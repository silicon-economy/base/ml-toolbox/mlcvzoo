# MLCVZoo MMDetection

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_mmdetection** is the wrapper module for
the [mmdetection framework](https://github.com/open-mmlab/mmdetection).

Further information about the MLCVZoo can be found [here](../README.md).

## Install
`
pip install mlcvzoo-mmdetection
`

## Technology stack

- Python

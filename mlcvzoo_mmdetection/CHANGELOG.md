# MLCVZoo mlcvzoo_mmdetection module Versions:

5.1.1 (2023-05-10):
------------------
Add the MMSegmentationModel model

5.0.1 (2023-05-03):
------------------
Python 3.10 compatibility

5.0.0 (2023-02-14):
------------------
Implement API changes introduces by mlcvzoo-base version 5.0.0
- Remove detector-config and use the feature of the single ModelConfiguration
- Remove duplicate attributes

4.0.2 (2022-10-17):
------------------
Fix bug in restore method: Ensure that the checkpoint is used which is passed to the method

4.0.1 (2022-09-09):
------------------
Ensure ConfigBuilder version 7 compatibility

4.0.0 (2022-08-08):
------------------
- Adapt to mlcvzoo-base 4.0.0
- Refactor and enhance mlcvzoo_mmdetection
  - The MMDetectionModel is now the base of all models of open-mmlab
  - Add MMObjectDetectionModel as dedicated model for object detection
  - Remove the CSVDataset and replace it with the MLCVZooMMDetDataset
  - Add latest commandline parameter for training mmdet models

3.0.1 (2022-07-11):
------------------
Prepare package for PyPi

3.0.0 (2022-05-16):
------------------
Use new features from AnnotationClassMapper that have been added with mlcvzoo_base v3.0.0

2.0.1 (2022-05-16)
------------------
Changed python executable for distributed training
- It can happen that the system python and python for running code are not the same. When starting distributed training, the system python was called.
- Now the python executable that runs the code is also executed when starting distributed (multi gpu) training.

2.0.0 (2022-04-05)
------------------
- initial release of the package

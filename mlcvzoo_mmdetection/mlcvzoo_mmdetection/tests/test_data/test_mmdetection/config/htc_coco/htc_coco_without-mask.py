_base_ = "MMDETECTION_DIR/configs/htc/htc_x101_64x4d_fpn_dconv_c3-c5_mstrain_400_1400_16x1_20e_coco.py"
model = dict(
    roi_head=dict(
        semantic_roi_extractor=None,
        semantic_head=None,
        mask_roi_extractor=None,
        mask_head=None,
    ),
)

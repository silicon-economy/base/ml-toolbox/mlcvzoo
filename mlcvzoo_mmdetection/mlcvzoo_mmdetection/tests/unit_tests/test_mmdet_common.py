# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import copy
import logging
import os
import typing
from typing import Dict, cast
from unittest import TestCase

from config_builder import ConfigBuilder
from mlcvzoo_base.api.data.annotation_class_mapper import AnnotationClassMapper
from mlcvzoo_base.configuration.class_mapping_config import (
    ClassMappingConfig,
    ClassMappingModelClassesConfig,
)
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information
from mmcv import Config
from mmcv.utils.config import ConfigDict
from mmdet import __version__ as framework_version
from mmdet.models import build_detector
from related import to_model

from mlcvzoo_mmdetection.configuration import MMDetectionConfig
from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel
from mlcvzoo_mmdetection.third_party.mmdetection import set_checkpoint_config
from mlcvzoo_mmdetection.utils import init_mmdetection_config

logger = logging.getLogger(__name__)


class TestMMDetectionYolov3(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_mmdetection"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

    def test_configuration(self):
        configuration = MMObjectDetectionModel.create_configuration(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmdetection",
                "mlcvzoo_mmdetection",
                "tests",
                "test_data",
                "test_mmdetection",
                "config",
                "yolov3_coco",
                "yolov3_coco_test_config.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        assert configuration is not None

        configuration_2 = cast(
            MMDetectionConfig, to_model(MMDetectionConfig, configuration.to_dict())
        )
        assert configuration_2 is not None

    def test_mmdet_api_methods(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST that the base_config is correctly set when using multiple models:\n"
            "#      test_ensure_correct_base_config(self)\n"
            "############################################################"
        )

        mmdet_model = MMObjectDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmdetection",
                "mlcvzoo_mmdetection",
                "tests",
                "test_data",
                "test_mmdetection",
                "config",
                "yolov3_coco",
                "yolov3_coco_test_config.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        self.string_replacement_map["MODEL_CONFIG_ROOT_DIR"] = os.path.join(
            self.project_root,
            "mlcvzoo_mmdetection/mlcvzoo_mmdetection/tests/test_data/test_mmdetection/config/htc_coco",
        )

        assert mmdet_model.get_training_output_dir() == os.path.join(
            self.project_root, "test_output/yolov3_coco_test"
        )

        assert mmdet_model.unique_name == "yolov3_coco_test"
        assert mmdet_model.get_checkpoint_filename_suffix() == ".pth"

    def test_init_mmdet_from_config(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST that a mmdetection model can be initialized via a given "
            "  configuration object:\n"
            "#      test_init_mmdet_from_config(self)\n"
            "############################################################"
        )

        from_yaml = os.path.join(
            self.project_root,
            "mlcvzoo_mmdetection",
            "mlcvzoo_mmdetection",
            "tests",
            "test_data",
            "test_mmdetection",
            "config",
            "htc_coco",
            "htc_coco_test_config.yaml",
        )

        self.string_replacement_map["MODEL_CONFIG_ROOT_DIR"] = os.path.join(
            self.project_root,
            "mlcvzoo_mmdetection/mlcvzoo_mmdetection/tests/test_data/test_mmdetection/config/htc_coco",
        )

        config_builder = ConfigBuilder(
            class_type=MMDetectionConfig,
            yaml_config_path=from_yaml,
            string_replacement_map=self.string_replacement_map,
        )

        configuration = typing.cast(
            MMDetectionConfig, copy.deepcopy(config_builder.configuration)
        )
        del config_builder

        configuration.recursive_string_replacement()

        mmdet_model = MMObjectDetectionModel(
            configuration=configuration,
            init_for_inference=True,
        )

        assert mmdet_model is not None

    def test_set_checkpoint_config(self) -> None:
        _, new_config_path = init_mmdetection_config(
            config_path=os.path.join(
                self.string_replacement_map["MMDETECTION_DIR"],
                "configs/yolo/yolov3_d53_mstrain-608_273e_coco.py",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        cfg = Config.fromfile(new_config_path)

        mapper = AnnotationClassMapper(
            class_mapping=ClassMappingConfig(
                model_classes=[
                    ClassMappingModelClassesConfig(class_name="test_class", class_id=0)
                ],
                number_model_classes=1,
            ),
        )

        model = build_detector(
            cfg.model, train_cfg=cfg.get("train_cfg"), test_cfg=cfg.get("test_cfg")
        )

        cfg.checkpoint_config = ConfigDict({"interval": 1})

        cfg = set_checkpoint_config(
            cfg=cfg,
            framework_version=framework_version,
            classes=mapper.get_model_class_names(),
            model=model,
        )

        assert model.CLASSES == mapper.get_model_class_names()
        assert cfg.checkpoint_config.meta == dict(
            mmdet_version=framework_version,
            CLASSES=mapper.get_model_class_names(),
        )

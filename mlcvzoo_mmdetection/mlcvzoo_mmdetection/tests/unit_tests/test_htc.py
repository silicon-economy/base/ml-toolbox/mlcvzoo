# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.
import logging
import os
from typing import Dict
from unittest import TestCase, main

import cv2
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information

from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel
from mlcvzoo_mmdetection.segmentation_model import MMSegmentationModel

logger = logging.getLogger(__name__)


class TestMMDetectionHTC(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_mmdetection"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

        logger.debug(
            "Setup finished: \n"
            " - this_dir: %s\n"
            " - project_root: %s\n"
            " - code_root: %s\n",
            self.this_dir,
            self.project_root,
            self.code_root,
        )

    def test_htc_coco_inference(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST inference of htc-detector trained on coco-data (mmdetection):\n"
            "#      test_htc_coco_inference(self)\n"
            "############################################################"
        )

        self.string_replacement_map["MODEL_CONFIG_ROOT_DIR"] = os.path.join(
            self.project_root,
            "mlcvzoo_mmdetection/mlcvzoo_mmdetection/"
            "tests/test_data/test_mmdetection/config/htc_coco",
        )

        mmdet_model = MMObjectDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmdetection",
                "mlcvzoo_mmdetection",
                "tests",
                "test_data",
                "test_mmdetection",
                "config",
                "htc_coco",
                "htc_coco_without-mask_test_config.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        test_image_path = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )

        _, bounding_boxes = mmdet_model.predict(data_item=test_image_path)

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED BOUNDING-BOXES: \n"
            " %s\n"
            "\n==============================================================\n",
            bounding_boxes,
        )

        expected_bounding_box_0 = BoundingBox(
            box=Box(xmin=242, ymin=719, xmax=1980, ymax=2199),
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.9980654120445251,
            difficult=False,
            occluded=False,
            content="",
        )

        expected_bounding_box_1 = BoundingBox(
            box=Box(xmin=1999, ymin=1719, xmax=3356, ymax=2373),
            class_identifier=ClassIdentifier(
                class_id=43,
                class_name="knife",
            ),
            score=0.8231878876686096,
            difficult=False,
            occluded=False,
            content="",
        )

        expected_bounding_box_2 = BoundingBox(
            box=Box(xmin=0, ymin=1686, xmax=3634, ymax=2707),
            class_identifier=ClassIdentifier(
                class_id=60,
                class_name="diningtable",
            ),
            score=0.8231878876686096,
            difficult=False,
            occluded=False,
            content="",
        )

        is_correct_0 = (
            expected_bounding_box_0.box == bounding_boxes[0].box
            and expected_bounding_box_0.class_id == bounding_boxes[0].class_id
            and expected_bounding_box_0.class_name == bounding_boxes[0].class_name
        )

        is_correct_1 = (
            expected_bounding_box_1.box == bounding_boxes[1].box
            and expected_bounding_box_1.class_id == bounding_boxes[1].class_id
            and expected_bounding_box_1.class_name == bounding_boxes[1].class_name
        )

        is_correct_2 = (
            expected_bounding_box_2.box == bounding_boxes[2].box
            and expected_bounding_box_2.class_id == bounding_boxes[2].class_id
            and expected_bounding_box_2.class_name == bounding_boxes[2].class_name
        )

        if not is_correct_0:
            logger.error(
                "Found wrong bounding_box: \n"
                "  Expected 0:  %s\n"
                "  Predicted 0: %s\n",
                expected_bounding_box_0,
                bounding_boxes[0],
            )

            raise ValueError("Output is not valid!")

        if not is_correct_1:
            logger.error(
                "Found wrong bounding_box: \n"
                "  Expected 1:  %s\n"
                "  Predicted 1: %s\n",
                expected_bounding_box_1,
                bounding_boxes[1],
            )

            raise ValueError("Output is not valid!")

        if not is_correct_2:
            logger.error(
                "Found wrong bounding_box: \n"
                "  Expected 1:  %s\n"
                "  Predicted 1: %s\n",
                expected_bounding_box_2,
                bounding_boxes[2],
            )

            raise ValueError("Output is not valid!")

    def test_htc_coco_training(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST training of htc-detector trained on coco-data (mmdetection):\n"
            "#      test_htc_coco_inference(self)\n"
            "############################################################"
        )

        mmdet_model = MMObjectDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmdetection",
                "mlcvzoo_mmdetection",
                "tests",
                "test_data",
                "test_mmdetection",
                "config",
                "htc_coco",
                "htc_coco_without-mask_test_config.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=False,
        )

        try:
            mmdet_model.train()
        except RuntimeError as re:
            if "CUDA out of memory" in str(re):
                self.skipTest("Could not test training of htc. GPU memory is to small")
            else:
                raise re

        input_checkpoint_path = os.path.join(
            mmdet_model.configuration.train_config.argparse_config.work_dir,
            "latest.pth",
        )
        output_checkpoint_path = os.path.join(
            mmdet_model.configuration.train_config.argparse_config.work_dir,
            "reduced_cp.pth",
        )

        mmdet_model.save_reduced_checkpoint(
            input_checkpoint_path=input_checkpoint_path,
            output_checkpoint_path=output_checkpoint_path,
        )

    def test_htc_coco_distributed_training(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST distributed training of htc-detector trained on coco-data (mmdetection):\n"
            "#      test_htc_coco_distributed_training (self)\n"
            "############################################################"
        )

        model_config_root_dir = os.path.join(
            self.project_root,
            "mlcvzoo_mmdetection/mlcvzoo_mmdetection/"
            "tests/test_data/test_mmdetection/config/htc_coco_distributed",
        )

        mmdet_model = MMObjectDetectionModel(
            from_yaml=os.path.join(
                model_config_root_dir,
                "htc_coco_without-mask_test_distributed_config.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=False,
        )

        try:
            mmdet_model.train()
        except RuntimeError as re:
            if "CUDA out of memory" in str(re):
                self.skipTest(
                    "Could not test distributed training of htc (mmdetection). "
                    "GPU memory is to small"
                )
            else:
                raise re

    def test_htc_coco_instance_segmentation_inference(self) -> None:
        mmdet_model = MMSegmentationModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmdetection",
                "mlcvzoo_mmdetection",
                "tests",
                "test_data",
                "test_mmdetection",
                "config",
                "htc_coco_seg",
                "htc_coco_seg_test_config.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        test_image_path = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )

        image = cv2.imread(test_image_path)

        _, segmentations = mmdet_model.predict(data_item=image)

        assert len(segmentations) == 3
        assert segmentations[0].class_id == 41
        assert segmentations[0].class_name == "cup"
        assert segmentations[0].model_class_identifier.class_id == 41
        assert segmentations[0].model_class_identifier.class_name == "cup"
        assert segmentations[0].box == Box(xmin=247, ymin=716, xmax=1986, ymax=2190)
        assert segmentations[0].content == ""
        assert not segmentations[0].occluded
        assert len(segmentations[0].polygon) == 5110
        # don't check for exact polygon coordinates as these could change, and they are too many

        assert segmentations[1].class_id == 43
        assert segmentations[1].class_name == "knife"
        assert segmentations[1].model_class_identifier.class_id == 43
        assert segmentations[1].model_class_identifier.class_name == "knife"
        assert segmentations[1].box == Box(xmin=2006, ymin=1726, xmax=3351, ymax=2370)
        assert segmentations[1].content == ""
        assert not segmentations[1].occluded
        assert len(segmentations[1].polygon) == 2781

        assert segmentations[2].class_id == 60
        assert segmentations[2].class_name == "diningtable"
        assert segmentations[2].model_class_identifier.class_id == 60
        assert segmentations[2].model_class_identifier.class_name == "diningtable"
        assert segmentations[2].box == Box(xmin=9, ymin=1707, xmax=3630, ymax=2702)
        assert segmentations[2].content == ""
        assert not segmentations[2].occluded
        assert len(segmentations[2].polygon) == 9029


if __name__ == "__main__":
    main()

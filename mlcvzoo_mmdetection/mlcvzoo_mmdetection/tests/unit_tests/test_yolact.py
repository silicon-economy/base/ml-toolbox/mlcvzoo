# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import os
from typing import Dict
from unittest import TestCase, main, skip

import cv2
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils import draw_on_image, generate_detector_colors
from mlcvzoo_base.utils.file_utils import get_file_list, get_project_path_information

from mlcvzoo_mmdetection.segmentation_model import MMSegmentationModel

logger = logging.getLogger(__name__)


class TestMMDetectionYOLACT(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_mmdetection"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

        logger.debug(
            "Setup finished: \n"
            " - this_dir: %s\n"
            " - project_root: %s\n"
            " - code_root: %s\n",
            self.this_dir,
            self.project_root,
            self.code_root,
        )

    def test_yolact_coco_inference(self) -> None:
        mmdet_model = MMSegmentationModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmdetection",
                "mlcvzoo_mmdetection",
                "tests",
                "test_data",
                "test_mmdetection",
                "config",
                "yolact_coco",
                "yolact_coco_test_config.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        test_image_path = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )

        image = cv2.imread(test_image_path)

        _, segmentations = mmdet_model.predict(data_item=image)

        assert len(segmentations) == 3
        assert segmentations[0].class_id == 41
        assert segmentations[0].class_name == "cup"
        assert segmentations[0].model_class_identifier.class_id == 41
        assert segmentations[0].model_class_identifier.class_name == "cup"
        assert segmentations[0].box == Box(xmin=245, ymin=695, xmax=1934, ymax=2180)
        assert segmentations[0].content == ""
        assert not segmentations[0].occluded
        assert len(segmentations[0].polygon) == 5214
        # don't check for exact polygon coordinates as these could change, and they are too many

        assert segmentations[1].class_id == 60
        assert segmentations[1].class_name == "diningtable"
        assert segmentations[1].model_class_identifier.class_id == 60
        assert segmentations[1].model_class_identifier.class_name == "diningtable"
        assert segmentations[1].box == Box(xmin=52, ymin=1428, xmax=3527, ymax=2710)
        assert segmentations[1].content == ""
        assert not segmentations[1].occluded
        assert len(segmentations[1].polygon) == 210

        assert segmentations[2].class_id == 60
        assert segmentations[2].class_name == "diningtable"
        assert segmentations[2].model_class_identifier.class_id == 60
        assert segmentations[2].model_class_identifier.class_name == "diningtable"
        assert segmentations[2].box == Box(xmin=218, ymin=882, xmax=2791, ymax=2501)
        assert segmentations[2].content == ""
        assert not segmentations[2].occluded
        assert len(segmentations[2].polygon) == 9819

    def test_yolact_train_custom(self) -> None:
        mmdet_model = MMSegmentationModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmdetection/mlcvzoo_mmdetection/"
                "tests/test_data/test_mmdetection/config/yolact_custom/yolact_custom_config.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=False,
        )

        mmdet_model.train()

        assert os.path.isfile(
            os.path.join(
                self.project_root,
                "test_output/yolact_r101_1x8_coco_custom/epoch_65.pth",
            )
        )


if __name__ == "__main__":
    main()

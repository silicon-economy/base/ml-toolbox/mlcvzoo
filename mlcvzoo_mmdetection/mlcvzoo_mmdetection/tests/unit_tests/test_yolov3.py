# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.
import logging
import os
from typing import Dict
from unittest import TestCase, main

import cv2
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information

from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel

logger = logging.getLogger(__name__)


# TODO: build an overall Object-Detection Suite for all relevant models
# TODO: configure test to use visualization etc.
class TestMMDetectionYolov3(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_mmdetection"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

    def test_yolov3_coco_inference(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST inference of yolov3-detector trained on coco-data (mmdetection):\n"
            "#      test_yolov3_coco_inference(self)\n"
            "############################################################"
        )

        mmdet_model = MMObjectDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmdetection",
                "mlcvzoo_mmdetection",
                "tests",
                "test_data",
                "test_mmdetection",
                "config",
                "yolov3_coco",
                "yolov3_coco_test_config.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        test_image_path = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )

        test_image = cv2.imread(test_image_path)

        _, bounding_boxes = mmdet_model.predict(data_item=test_image)

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED BOUNDING-BOXES: \n"
            " %s\n"
            "\n==============================================================\n",
            bounding_boxes,
        )

        expected_bounding_box_0 = BoundingBox(
            box=Box(xmin=309, ymin=751, xmax=2024, ymax=2179),
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.9378350973129272,
            difficult=False,
            occluded=False,
            content="",
        )

        is_correct = (
            expected_bounding_box_0.box == bounding_boxes[0].box
            and expected_bounding_box_0.class_id == bounding_boxes[0].class_id
            and expected_bounding_box_0.class_name == bounding_boxes[0].class_name
        )

        if not is_correct:
            logger.error(
                "Found wrong bounding_box: \n" "  Expected:  %s\n" "  Predicted: %s\n",
                expected_bounding_box_0,
                bounding_boxes[0],
            )

            logger.error("All predictions: \n %s", bounding_boxes)

            raise ValueError("Output is not valid!")

    def test_yolov3_coco_training(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST training of yolov3-detector trained on coco-data (mmdetection):\n"
            "#      test_yolov3_coco_training(self)\n"
            "############################################################"
        )

        mmdet_model = MMObjectDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmdetection",
                "mlcvzoo_mmdetection",
                "tests",
                "test_data",
                "test_mmdetection",
                "config",
                "yolov3_coco",
                "yolov3_coco_test_config.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=False,
        )

        try:
            mmdet_model.train()
        except RuntimeError as re:
            if "CUDA out of memory" in str(re):
                self.skipTest(
                    "Could not test training of yolov3 (mmdetection). GPU memory is to small"
                )
            else:
                raise re


if __name__ == "__main__":
    main()

# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.
import logging
import os
from typing import Dict
from unittest import TestCase, main

import cv2
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information

from mlcvzoo_mmdetection.segmentation_model import MMSegmentationModel

logger = logging.getLogger(__name__)


class TestMMDetectionMask2Former(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_mmdetection"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

        logger.debug(
            "Setup finished: \n"
            " - this_dir: %s\n"
            " - project_root: %s\n"
            " - code_root: %s\n",
            self.this_dir,
            self.project_root,
            self.code_root,
        )

    def test_mask2former_coco_inference(self) -> None:
        mmdet_model = MMSegmentationModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmdetection",
                "mlcvzoo_mmdetection",
                "tests",
                "test_data",
                "test_mmdetection",
                "config",
                "mask2former",
                "mask2former_coco_test_config.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        test_image_path = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )

        image = cv2.imread(test_image_path)

        _, segmentations = mmdet_model.predict(data_item=image)

        # TODO: Add checks for correct segmentations
        logger.info("Predictions: %r" % segmentations)

        assert len(segmentations) == 4
        assert segmentations[0].class_id == 41
        assert segmentations[0].class_name == "cup"
        assert segmentations[0].model_class_identifier.class_id == 41
        assert segmentations[0].model_class_identifier.class_name == "cup"
        assert segmentations[0].box is None
        assert segmentations[0].content == ""
        assert not segmentations[0].occluded
        assert len(segmentations[0].polygon) == 5183
        # don't check for exact polygon coordinates as these could change, and they are too many

        assert segmentations[1].class_id == 133
        assert segmentations[1].class_name == "unknown_133"
        assert segmentations[1].model_class_identifier.class_id == 133
        assert segmentations[1].model_class_identifier.class_name == "unknown_133"
        assert segmentations[1].box is None
        assert segmentations[1].content == ""
        assert not segmentations[1].occluded
        assert len(segmentations[1].polygon) == 197

        assert segmentations[2].class_id == 131
        assert segmentations[2].class_name == "unknown_131"
        assert segmentations[2].model_class_identifier.class_id == 131
        assert segmentations[2].model_class_identifier.class_name == "unknown_131"
        assert segmentations[2].box is None
        assert segmentations[2].content == ""
        assert not segmentations[2].occluded
        assert len(segmentations[2].polygon) == 13047

        assert segmentations[3].class_id == 121
        assert segmentations[3].class_name == "unknown_121"
        assert segmentations[3].model_class_identifier.class_id == 121
        assert segmentations[3].model_class_identifier.class_name == "unknown_121"
        assert segmentations[3].box is None
        assert segmentations[3].content == ""
        assert not segmentations[3].occluded
        assert len(segmentations[3].polygon) == 9719


if __name__ == "__main__":
    main()

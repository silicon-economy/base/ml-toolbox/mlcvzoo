# MLCVZoo MMOCR

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_mmocr** is the wrapper module for
the [mmocr framework](https://github.com/open-mmlab/mmocr).

Further information about the MLCVZoo can be found [here](../README.md).

## Install
`
pip install mlcvzoo-mmocr
`

## Technology stack

- Python

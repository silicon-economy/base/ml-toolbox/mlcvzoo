ARG BASE_TAG=latest
ARG BASE_IMAGE=nexus.apps.sele.iml.fraunhofer.de/sele/ml-toolbox/mlcvzoo-ci-image
FROM $BASE_IMAGE:$BASE_TAG

# Disable CUDA detection to build with CUDA without a GPU
ENV FORCE_CUDA="1"

# setup development environment:
ENV BUILD_ENV_DIR="/build-env"
ENV PROJECT_ROOT_DIR="$BUILD_ENV_DIR/MLCVZoo"
# External Projects:
ENV EXTERNAL_DIR="$BUILD_ENV_DIR/external"

ENV SUB_PROJECT_DIR="$PROJECT_ROOT_DIR/mlcvzoo_mmocr"
RUN mkdir -p "$SUB_PROJECT_DIR" "$PROJECT_ROOT_DIR/config" "$EXTERNAL_DIR"

# Now set python path to include src directory and external dependencies
ENV PYTHONPATH="$PYTHONPATH:$SUB_PROJECT_DIR/:$EXTERNAL_DIR"

COPY pyproject.toml "$SUB_PROJECT_DIR/"
COPY poetry.lock "$SUB_PROJECT_DIR/"
COPY build.sh "$SUB_PROJECT_DIR/"

# Remove lockfile on ARM as some dependencies might be different than on x86_64
RUN (test "x$(uname -m)" = "xaarch64") && rm "$SUB_PROJECT_DIR/poetry.lock" || true

WORKDIR "$SUB_PROJECT_DIR"

ENV VIRTUAL_ENV=/usr/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN python3 -m venv $VIRTUAL_ENV \
  && poetry run pip install --upgrade pip \
  && ./build.sh install --no-interaction --no-ansi --no-root --all-extras

# ====================================================================
# Checkout mmocr
# NOTE: The mmocr repostiory is only needed for configs and their tools/train.py script.
#       An dedicated installation is not neccessary and already handled by poetry.
ENV MMOCR_DIR="$EXTERNAL_DIR/mmocr_repo"
RUN (poetry show mmocr >/dev/null 2>&1 && \
    git clone --depth=1 https://github.com/open-mmlab/mmocr.git --branch "v$(poetry show mmocr | grep version | awk '{print $3}')" "$MMOCR_DIR" \
    && rm -rf "$MMOCR_DIR"/mmocr ) \
    || echo "MM OCR not installed, not cloning its repo"

# Cleanup poetry cache
RUN rm -rf ~/.cache/pypoetry

# ====================================================================
# Label the image

ARG VERSION
LABEL org.opencontainers.image.authors="Maximilian Otten <maximilian.otten@iml.fraunhofer.de>, Christian Hoppe <christian.hoppe@iml.fraunhofer.de" \
      org.opencontainers.image.version=${VERSION} \
      org.opencontainers.image.vendor="Fraunhofer IML" \
      org.opencontainers.image.title="MLCVZoo MMOCR - Main GPU-enabled gitlab-runner container" \
      org.opencontainers.image.description="Container image for GPU enabled integration testing and continuous delivery for MLCVZoo MMOCR"

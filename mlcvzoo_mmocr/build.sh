#!/bin/sh

if [ $# -lt 1 ]; then
  echo "Usage: $0 POETRY_ARGS"
  return 1
fi

# Perform the poetry step
poetry "$@"
# Reinstall mmcv-full with legacy setup afterwards as the variant installed with poetry fails to run
poetry run python -m pip uninstall -y mmcv-full
# MMCV_WITH_OPS is not strictly necessary (its implied by -full) but here for robustness
MMCV_WITH_OPS=1 poetry run python -m pip install \
  --no-cache \
  --no-binary :all: \
  --no-deps \
  --no-build-isolation \
  --no-use-pep517 \
  mmcv-full=="$(poetry show mmcv-full | grep version | awk '{print $3}')"

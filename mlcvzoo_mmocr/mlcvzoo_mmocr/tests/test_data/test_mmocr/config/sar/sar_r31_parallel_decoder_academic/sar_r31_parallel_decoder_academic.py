_base_ = ["MMOCR_DIR/configs/textrecog/sar/sar_r31_parallel_decoder_academic.py"]

img_norm_cfg = dict(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
train_pipeline = [
    dict(type="LoadImageFromFile"),
    dict(
        type="ResizeOCR",
        height=48,
        min_width=48,
        max_width=160,
        keep_aspect_ratio=True,
        width_downsample_ratio=0.25,
    ),
    dict(type="ToTensorOCR"),
    dict(type="NormalizeOCR", **img_norm_cfg),
    dict(
        type="Collect",
        keys=["img"],
        meta_keys=["filename", "ori_shape", "resize_shape", "text", "valid_ratio"],
    ),
]
test_pipeline = [
    dict(type="LoadImageFromFile"),
    dict(
        type="MultiRotateAugOCR",
        rotate_degrees=[0, 90, 270],
        transforms=[
            dict(
                type="ResizeOCR",
                height=48,
                min_width=48,
                max_width=160,
                keep_aspect_ratio=True,
                width_downsample_ratio=0.25,
            ),
            dict(type="ToTensorOCR"),
            dict(type="NormalizeOCR", **img_norm_cfg),
            dict(
                type="Collect",
                keys=["img"],
                meta_keys=[
                    "filename",
                    "ori_shape",
                    "resize_shape",
                    "valid_ratio",
                    "img_norm_cfg",
                    "ori_filename",
                    "img_shape",
                ],
            ),
        ],
    ),
]


dataset_type = "OCRDataset"
train_dict_list = [
    dict(
        type=dataset_type,
        img_prefix="PROJECT_ROOT_DIR",
        ann_file="PROJECT_ROOT_DIR/test_data/annotations/text_recognition_test_label.txt",
        loader=dict(
            type="HardDiskLoader",
            repeat=10,
            parser=dict(
                type="LineStrParser",
                keys=["filename", "text"],
                keys_idx=[0, 1],
                separator=" ",
            ),
        ),
        test_mode=False,
        pipeline=train_pipeline,
    )
]

train_dict_list_test = [
    dict(
        type=dataset_type,
        img_prefix="PROJECT_ROOT_DIR",
        ann_file="PROJECT_ROOT_DIR/test_data/annotations/text_recognition_test_label.txt",
        loader=dict(
            type="HardDiskLoader",
            repeat=2,
            parser=dict(
                type="LineStrParser",
                keys=["filename", "text"],
                keys_idx=[0, 1],
                separator=" ",
            ),
        ),
        test_mode=False,
        pipeline=test_pipeline,
    )
]

data = dict(
    samples_per_gpu=2,
    workers_per_gpu=0,
    val_dataloader=dict(samples_per_gpu=1),
    test_dataloader=dict(samples_per_gpu=1),
    train=dict(type="ConcatDataset", datasets=train_dict_list, pipeline=train_pipeline),
    val=dict(type="ConcatDataset", datasets=train_dict_list, pipeline=test_pipeline),
    test=dict(type="ConcatDataset", datasets=train_dict_list, pipeline=test_pipeline),
)

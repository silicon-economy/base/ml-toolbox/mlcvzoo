# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.
import logging
import os
from typing import Dict
from unittest import TestCase, main, skip
from unittest.mock import MagicMock

from mlcvzoo_base.api.data.ocr_perception import OCRPerception
from mlcvzoo_base.configuration.replacement_config import ReplacementConfig
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information
from pytest import fixture, mark
from pytest_mock import MockerFixture

from mlcvzoo_mmocr.text_recognition_model import MMOCRTextRecognitionModel

logger = logging.getLogger(__name__)


@fixture(scope="function")
def filter_score_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_mmocr.text_recognition_model.MMOCRTextRecognitionModel._MMOCRTextRecognitionModel__filter_result_score_by_mean",
        return_value=None,
    )


@fixture(scope="function")
def predict_many_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_mmocr.model.MMOCRModel._predict",
        return_value=[
            {"text": "", "score": []},
            {"text": "", "score": []},
            {"text": "", "score": []},
            {"text": "", "score": []},
        ],
    )


class TestMMOCRSAR(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_mmocr"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

        logger.debug(
            "Setup finished: \n"
            " - this_dir: %s\n"
            " - project_root: %s\n"
            " - code_root: %s\n",
            self.this_dir,
            self.project_root,
            self.code_root,
        )

    def test_sar_r31_parallel_decoder_academic_inference(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST inference of sar text-recognition detector trained on "
            "several text datasets (mmocr):\n"
            "#      test_sar_r31_parallel_decoder_academic_inference(self)\n"
            "############################################################"
        )

        mmocr_model = MMOCRTextRecognitionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmocr",
                "mlcvzoo_mmocr",
                "tests",
                "test_data",
                "test_mmocr",
                "config",
                "sar",
                "sar_r31_parallel_decoder_academic",
                "sar_r31_parallel_decoder_academic.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        mmocr_dir = mmocr_model.configuration.string_replacement_map[
            ReplacementConfig.MMOCR_DIR_KEY
        ]

        test_image_path_0 = os.path.join(
            mmocr_dir,
            "tests/data/ocr_char_ann_toy_dataset/imgs/resort_95_53_6.png",
        )

        _, ocr_perceptions_0 = mmocr_model.predict(data_item=test_image_path_0)

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED OCR-Perceptions: \n"
            " %s\n"
            "\n==============================================================\n",
            ocr_perceptions_0,
        )

        expected_ocr_perception_0 = OCRPerception(
            score=0.99,
            content="out",
        )

        is_correct_0 = (
            expected_ocr_perception_0.score - 0.5 <= ocr_perceptions_0[0].score <= 1.0
            and expected_ocr_perception_0.content == ocr_perceptions_0[0].content
        )

        if not is_correct_0:
            logger.error(
                "Found wrong ocr-perception: \n"
                "  Expected 0:  %s\n"
                "  Predicted 0: %s\n",
                expected_ocr_perception_0,
                ocr_perceptions_0[0],
            )

            raise ValueError("Output is not valid!")

        test_image_path_1 = os.path.join(
            mmocr_dir,
            "tests",
            "data",
            "test_img1.png",
        )

        _, ocr_perceptions_1 = mmocr_model.predict(data_item=test_image_path_1)

        assert len(ocr_perceptions_1) == 0

    def test_sar_r31_parallel_decoder_academic_inference_filtered_by_score(
        self,
    ) -> None:
        logger.info(
            "############################################################\n"
            "# TEST inference of sar text-recognition detector trained on "
            "several text datasets (mmocr):\n"
            "#      test_sar_r31_parallel_decoder_academic_inference(self)\n"
            "############################################################"
        )

        mmocr_model = MMOCRTextRecognitionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmocr",
                "mlcvzoo_mmocr",
                "tests",
                "test_data",
                "test_mmocr",
                "config",
                "sar",
                "sar_r31_parallel_decoder_academic",
                "sar_r31_parallel_decoder_academic.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        mmocr_dir = mmocr_model.configuration.string_replacement_map[
            ReplacementConfig.MMOCR_DIR_KEY
        ]

        test_image_path_0 = os.path.join(
            mmocr_dir,
            "tests",
            "data",
            "test_img1.png",
        )

        _, ocr_perceptions_0 = mmocr_model.predict(data_item=test_image_path_0)

        assert len(ocr_perceptions_0) == 0

    def test_sar_r31_parallel_decoder_academic_inference_many(self) -> None:
        mmocr_model = MMOCRTextRecognitionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmocr",
                "mlcvzoo_mmocr",
                "tests",
                "test_data",
                "test_mmocr",
                "config",
                "sar",
                "sar_r31_parallel_decoder_academic",
                "sar_r31_parallel_decoder_academic_batch.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        mmocr_dir = mmocr_model.configuration.string_replacement_map[
            ReplacementConfig.MMOCR_DIR_KEY
        ]

        test_image_path_0 = os.path.join(
            mmocr_dir,
            "tests/data/ocr_char_ann_toy_dataset/imgs/resort_95_53_6.png",
        )

        ocr_predictions = mmocr_model.predict_many(
            data_items=[
                test_image_path_0,
                test_image_path_0,
                test_image_path_0,
                test_image_path_0,
            ]
        )

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED OCR-Perceptions: \n"
            " %s\n"
            "\n==============================================================\n",
            ocr_predictions,
        )

        expected_ocr_perception_0 = OCRPerception(
            score=0.99,
            content="out",
        )
        for _, ocr_perceptions in ocr_predictions:
            is_correct_0 = (
                expected_ocr_perception_0.score - 0.5 <= ocr_perceptions[0].score <= 1.0
                and expected_ocr_perception_0.content == ocr_perceptions[0].content
            )

            if not is_correct_0:
                logger.error(
                    "Found wrong ocr-perception: \n"
                    "  Expected 0:  %s\n"
                    "  Predicted 0: %s\n",
                    expected_ocr_perception_0,
                    ocr_perceptions[0],
                )

                raise ValueError("Output is not valid!")

    @mark.usefixtures("predict_many_mock")
    def test_sar_r31_parallel_decoder_academic_inference_many_no_score(self) -> None:
        mmocr_model = MMOCRTextRecognitionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmocr",
                "mlcvzoo_mmocr",
                "tests",
                "test_data",
                "test_mmocr",
                "config",
                "sar",
                "sar_r31_parallel_decoder_academic",
                "sar_r31_parallel_decoder_academic_batch.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        test_image_path_0 = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_text-recognition_inference_image.jpg",
        )

        ocr_predictions = mmocr_model.predict_many(
            data_items=[
                test_image_path_0,
                test_image_path_0,
                test_image_path_0,
                test_image_path_0,
            ]
        )

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED OCR-Perceptions: \n"
            " %s\n"
            "\n==============================================================\n",
            ocr_predictions,
        )

        expected_ocr_predictions = [
            (test_image_path_0, []),
            (test_image_path_0, []),
            (test_image_path_0, []),
            (test_image_path_0, []),
        ]

        assert ocr_predictions == expected_ocr_predictions

    @mark.usefixtures("filter_score_mock")
    def test_sar_r31_parallel_decoder_academic_inference_many_no_predictions(
        self,
    ) -> None:
        mmocr_model = MMOCRTextRecognitionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmocr",
                "mlcvzoo_mmocr",
                "tests",
                "test_data",
                "test_mmocr",
                "config",
                "sar",
                "sar_r31_parallel_decoder_academic",
                "sar_r31_parallel_decoder_academic_batch.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        mmocr_dir = mmocr_model.configuration.string_replacement_map[
            ReplacementConfig.MMOCR_DIR_KEY
        ]

        test_image_path_0 = os.path.join(
            mmocr_dir,
            "tests/data/ocr_char_ann_toy_dataset/imgs/resort_95_53_6.png",
        )

        ocr_predictions = mmocr_model.predict_many(
            data_items=[
                test_image_path_0,
                test_image_path_0,
                test_image_path_0,
                test_image_path_0,
            ]
        )

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED OCR-Perceptions: \n"
            " %s\n"
            "\n==============================================================\n",
            ocr_predictions,
        )

        expected_ocr_predictions = [
            (test_image_path_0, []),
            (test_image_path_0, []),
            (test_image_path_0, []),
            (test_image_path_0, []),
        ]

        assert ocr_predictions == expected_ocr_predictions

    def test_sar_r31_parallel_decoder_academic_training(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST training of sar-detector (mmocr):\n"
            "#      test_sar_r31_parallel_decoder_academic_training(self)\n"
            "############################################################"
        )

        mmocr_model = MMOCRTextRecognitionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmocr",
                "mlcvzoo_mmocr",
                "tests",
                "test_data",
                "test_mmocr",
                "config",
                "sar",
                "sar_r31_parallel_decoder_academic",
                "sar_r31_parallel_decoder_academic.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=False,
        )

        try:
            mmocr_model.train()

            assert os.path.isfile(
                os.path.join(
                    self.project_root,
                    "test_output/sar_r31_parallel_decoder_academic/latest.pth",
                )
            )

            del mmocr_model
        except RuntimeError as re:
            del mmocr_model

            if "CUDA out of memory" in str(re):
                self.skipTest(
                    "Could not test training of sar (mmocr). GPU memory is to small"
                )
            else:
                raise re


if __name__ == "__main__":
    main()

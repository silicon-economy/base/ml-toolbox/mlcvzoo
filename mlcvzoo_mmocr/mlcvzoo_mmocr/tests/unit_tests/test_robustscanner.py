# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import os
from typing import Dict
from unittest import TestCase, main, skip
from unittest.mock import MagicMock

from mlcvzoo_base.api.data.ocr_perception import OCRPerception
from mlcvzoo_base.configuration.replacement_config import ReplacementConfig
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information
from pytest import fixture, mark
from pytest_mock import MockerFixture

from mlcvzoo_mmocr.text_recognition_model import MMOCRTextRecognitionModel

logger = logging.getLogger(__name__)


class TestMMOCRSAR(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_mmocr"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

        logger.debug(
            "Setup finished: \n"
            " - this_dir: %s\n"
            " - project_root: %s\n"
            " - code_root: %s\n",
            self.this_dir,
            self.project_root,
            self.code_root,
        )

    def test_robustscanner_inference(self) -> None:
        mmocr_model = MMOCRTextRecognitionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmocr/mlcvzoo_mmocr/tests/test_data/test_mmocr/config/robustscanner/"
                "robustscanner_r31_academic.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        mmocr_dir = mmocr_model.configuration.string_replacement_map[
            ReplacementConfig.MMOCR_DIR_KEY
        ]

        test_image_path_0 = os.path.join(
            mmocr_dir,
            "tests/data/ocr_char_ann_toy_dataset/imgs/resort_95_53_6.png",
        )

        _, ocr_perceptions_0 = mmocr_model.predict(data_item=test_image_path_0)

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED OCR-Perceptions: \n"
            " %s\n"
            "\n==============================================================\n",
            ocr_perceptions_0,
        )

        expected_ocr_perception_0 = OCRPerception(
            score=0.99,
            content="out",
        )

        is_correct_0 = (
            expected_ocr_perception_0.score - 0.5 <= ocr_perceptions_0[0].score <= 1.0
            and expected_ocr_perception_0.content == ocr_perceptions_0[0].content
        )

        if not is_correct_0:
            logger.error(
                "Found wrong ocr-perception: \n"
                "  Expected 0:  %s\n"
                "  Predicted 0: %s\n",
                expected_ocr_perception_0,
                ocr_perceptions_0[0],
            )

            raise ValueError("Output is not valid!")


if __name__ == "__main__":
    main()

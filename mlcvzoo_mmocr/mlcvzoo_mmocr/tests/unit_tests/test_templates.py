# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import os
from typing import Dict, List, Type
from unittest import TestCase, main

from config_builder import BaseConfigClass, ConfigBuilder
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information

from mlcvzoo_mmocr.configuration import MMOCRConfig

logger = logging.getLogger(__name__)


class TestTemplates(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_mmocr"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

    def test_config_templates(self) -> None:
        template_path_dict: Dict[Type[BaseConfigClass], List[str]] = {
            MMOCRConfig: [
                os.path.join(
                    self.project_root,
                    "config",
                    "templates",
                    "models",
                    "mmocr",
                    "panet",
                    "panet_r18_fpem_ffm_600e_ctw1500",
                    "panet_r18_fpem_ffm_600e_ctw1500.yaml",
                )
            ],
        }

        for config_class_type, template_path_list in template_path_dict.items():
            for template_path in template_path_list:
                logger.info(
                    "=================================================================\n"
                    "CHECK correct build of configuration class %s "
                    "with template-config-path '%s'\n",
                    config_class_type,
                    template_path,
                )

                config_builder = ConfigBuilder(
                    class_type=config_class_type,
                    yaml_config_path=template_path,
                    string_replacement_map=self.string_replacement_map,
                    no_checks=True,
                )

                logger.info(
                    "================================================================="
                )
                assert config_builder.configuration is not None


if __name__ == "__main__":
    main()

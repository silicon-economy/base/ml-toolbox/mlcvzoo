# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.
import logging
import os
from typing import Dict
from unittest import TestCase, main, skip

from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_base.configuration.replacement_config import ReplacementConfig
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information

from mlcvzoo_mmocr.configuration import MMOCRConfig
from mlcvzoo_mmocr.text_detection_model import MMOCRTextDetectionModel

logger = logging.getLogger(__name__)


class TestMMOCRPanet(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_mmocr"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

        logger.debug(
            "Setup finished: \n"
            " - this_dir: %s\n"
            " - project_root: %s\n"
            " - code_root: %s\n",
            self.this_dir,
            self.project_root,
            self.code_root,
        )

    def test_panet_ctw1500_inference(self) -> None:
        mmocr_model = MMOCRTextDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmocr",
                "mlcvzoo_mmocr",
                "tests",
                "test_data",
                "test_mmocr",
                "config",
                "panet",
                "panet_r18_fpem_ffm_600e_ctw1500",
                "panet_r18_fpem_ffm_600e_ctw1500.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        mmocr_dir = mmocr_model.configuration.string_replacement_map[
            ReplacementConfig.MMOCR_DIR_KEY
        ]

        test_image_path = os.path.join(
            mmocr_dir,
            "tests",
            "data",
            "test_img1.png",
        )

        _, segmentations = mmocr_model.predict(data_item=test_image_path)

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED Segmentations: \n"
            " %s\n"
            "\n==============================================================\n",
            segmentations,
        )

        expected_segmentation_0 = Segmentation(
            polygon=[
                (1132.1999435424805, 1438.762004852295),
                (1116.89994430542, 1454.0679836273193),
                (1116.89994430542, 1499.9859199523926),
                (1346.3999328613281, 1499.9859199523926),
                (1361.6999320983887, 1515.291898727417),
                (1422.8999290466309, 1515.291898727417),
                (1438.1999282836914, 1499.9859199523926),
                (1453.499927520752, 1499.9859199523926),
                (1468.7999267578125, 1515.291898727417),
                (1545.2999229431152, 1515.291898727417),
                (1560.5999221801758, 1499.9859199523926),
                (1774.7999114990234, 1499.9859199523926),
                (1774.7999114990234, 1484.6799411773682),
                (1759.499912261963, 1469.3739624023438),
                (1744.1999130249023, 1469.3739624023438),
                (1728.8999137878418, 1454.0679836273193),
                (1239.2999382019043, 1454.0679836273193),
                (1223.9999389648438, 1438.762004852295),
            ],
            class_identifier=ClassIdentifier(
                MMOCRConfig.__text_class_id__,
                class_name=MMOCRConfig.__text_class_name__,
            ),
            score=0.9520,
            difficult=False,
            occluded=False,
            content="",
        )

        expected_segmentation_1 = Segmentation(
            polygon=[
                (290.6999855041504, 1515.291898727417),
                (290.6999855041504, 1561.2098350524902),
                (627.2999687194824, 1561.2098350524902),
                (627.2999687194824, 1515.291898727417),
            ],
            class_identifier=ClassIdentifier(
                MMOCRConfig.__text_class_id__,
                class_name=MMOCRConfig.__text_class_name__,
            ),
            score=0.9735,
            difficult=False,
            occluded=False,
            content="",
        )

        expected_segmentation_2 = Segmentation(
            polygon=[
                (948.5999526977539, 1607.1277713775635),
                (933.2999534606934, 1622.433750152588),
                (933.2999534606934, 1637.7397289276123),
                (948.5999526977539, 1653.0457077026367),
                (1407.5999298095703, 1653.0457077026367),
                (1422.8999290466309, 1668.3516864776611),
                (1927.799903869629, 1668.3516864776611),
                (1943.0999031066895, 1653.0457077026367),
                (1958.39990234375, 1653.0457077026367),
                (1958.39990234375, 1637.7397289276123),
                (1943.0999031066895, 1637.7397289276123),
                (1927.799903869629, 1622.433750152588),
                (1637.0999183654785, 1622.433750152588),
                (1621.799919128418, 1607.1277713775635),
                (1514.6999244689941, 1607.1277713775635),
                (1499.3999252319336, 1622.433750152588),
                (1438.1999282836914, 1622.433750152588),
                (1422.8999290466309, 1607.1277713775635),
            ],
            class_identifier=ClassIdentifier(
                MMOCRConfig.__text_class_id__,
                class_name=MMOCRConfig.__text_class_name__,
            ),
            score=0.9561,
            difficult=False,
            occluded=False,
            content="",
        )

        prediction_float_margin = 0.1

        is_correct_0 = True
        for index, polygon_tuple in enumerate(expected_segmentation_0.polygon):
            is_correct_0 = (
                is_correct_0
                and polygon_tuple[0] - prediction_float_margin
                <= segmentations[0].polygon[index][0]
                <= polygon_tuple[0] + prediction_float_margin
                and polygon_tuple[1] - prediction_float_margin
                <= segmentations[0].polygon[index][1]
                <= polygon_tuple[1] + prediction_float_margin
            )

        is_correct_1 = True
        for index, polygon_tuple in enumerate(expected_segmentation_1.polygon):
            is_correct_1 = (
                is_correct_1
                and polygon_tuple[0] - prediction_float_margin
                <= segmentations[1].polygon[index][0]
                <= polygon_tuple[0] + prediction_float_margin
                and polygon_tuple[1] - prediction_float_margin
                <= segmentations[1].polygon[index][1]
                <= polygon_tuple[1] + prediction_float_margin
            )

        is_correct_2 = True
        for index, polygon_tuple in enumerate(expected_segmentation_2.polygon):
            is_correct_2 = (
                is_correct_2
                and polygon_tuple[0] - prediction_float_margin
                <= segmentations[2].polygon[index][0]
                <= polygon_tuple[0] + prediction_float_margin
                and polygon_tuple[1] - prediction_float_margin
                <= segmentations[2].polygon[index][1]
                <= polygon_tuple[1] + prediction_float_margin
            )

        if not is_correct_0:
            logger.error(
                "Found wrong segmentation: \n"
                "  Expected 0:  %s\n"
                "  Predicted 0: %s\n",
                expected_segmentation_0,
                segmentations[0],
            )

            raise ValueError("Output is not valid!")

        if not is_correct_1:
            logger.error(
                "Found wrong segmentation: \n"
                "  Expected 1:  %s\n"
                "  Predicted 1: %s\n",
                expected_segmentation_1,
                segmentations[1],
            )

            raise ValueError("Output is not valid!")

        if not is_correct_2:
            logger.error(
                "Found wrong segmentation: \n"
                "  Expected 1:  %s\n"
                "  Predicted 1: %s\n",
                expected_segmentation_2,
                segmentations[2],
            )

            raise ValueError("Output is not valid!")

    def test_panet_ctw1500_inference_many(self) -> None:
        mmocr_model = MMOCRTextDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmocr",
                "mlcvzoo_mmocr",
                "tests",
                "test_data",
                "test_mmocr",
                "config",
                "panet",
                "panet_r18_fpem_ffm_600e_ctw1500",
                "panet_r18_fpem_ffm_600e_ctw1500_batch.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        mmocr_dir = mmocr_model.configuration.string_replacement_map[
            ReplacementConfig.MMOCR_DIR_KEY
        ]

        test_image_path = os.path.join(
            mmocr_dir,
            "tests",
            "data",
            "test_img1.png",
        )

        predictions = mmocr_model.predict_many(
            data_items=[test_image_path, test_image_path, test_image_path]
        )

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED Segmentations: \n"
            " %s\n"
            "\n==============================================================\n",
            predictions,
        )

        expected_segmentation_0 = Segmentation(
            polygon=[
                (1132.1999435424805, 1438.762004852295),
                (1116.89994430542, 1454.0679836273193),
                (1116.89994430542, 1499.9859199523926),
                (1346.3999328613281, 1499.9859199523926),
                (1361.6999320983887, 1515.291898727417),
                (1422.8999290466309, 1515.291898727417),
                (1438.1999282836914, 1499.9859199523926),
                (1453.499927520752, 1499.9859199523926),
                (1468.7999267578125, 1515.291898727417),
                (1545.2999229431152, 1515.291898727417),
                (1560.5999221801758, 1499.9859199523926),
                (1774.7999114990234, 1499.9859199523926),
                (1774.7999114990234, 1484.6799411773682),
                (1759.499912261963, 1469.3739624023438),
                (1744.1999130249023, 1469.3739624023438),
                (1728.8999137878418, 1454.0679836273193),
                (1239.2999382019043, 1454.0679836273193),
                (1223.9999389648438, 1438.762004852295),
            ],
            class_identifier=ClassIdentifier(
                MMOCRConfig.__text_class_id__,
                class_name=MMOCRConfig.__text_class_name__,
            ),
            score=0.9520,
            difficult=False,
            occluded=False,
            content="",
        )

        expected_segmentation_1 = Segmentation(
            polygon=[
                (290.6999855041504, 1515.291898727417),
                (290.6999855041504, 1561.2098350524902),
                (627.2999687194824, 1561.2098350524902),
                (627.2999687194824, 1515.291898727417),
            ],
            class_identifier=ClassIdentifier(
                MMOCRConfig.__text_class_id__,
                class_name=MMOCRConfig.__text_class_name__,
            ),
            score=0.9735,
            difficult=False,
            occluded=False,
            content="",
        )

        expected_segmentation_2 = Segmentation(
            polygon=[
                (948.5999526977539, 1607.1277713775635),
                (933.2999534606934, 1622.433750152588),
                (933.2999534606934, 1637.7397289276123),
                (948.5999526977539, 1653.0457077026367),
                (1407.5999298095703, 1653.0457077026367),
                (1422.8999290466309, 1668.3516864776611),
                (1927.799903869629, 1668.3516864776611),
                (1943.0999031066895, 1653.0457077026367),
                (1958.39990234375, 1653.0457077026367),
                (1958.39990234375, 1637.7397289276123),
                (1943.0999031066895, 1637.7397289276123),
                (1927.799903869629, 1622.433750152588),
                (1637.0999183654785, 1622.433750152588),
                (1621.799919128418, 1607.1277713775635),
                (1514.6999244689941, 1607.1277713775635),
                (1499.3999252319336, 1622.433750152588),
                (1438.1999282836914, 1622.433750152588),
                (1422.8999290466309, 1607.1277713775635),
            ],
            class_identifier=ClassIdentifier(
                MMOCRConfig.__text_class_id__,
                class_name=MMOCRConfig.__text_class_name__,
            ),
            score=0.9561,
            difficult=False,
            occluded=False,
            content="",
        )

        prediction_float_margin = 0.1

        for _, segmentations in predictions:
            is_correct_0 = True
            for index, polygon_tuple in enumerate(expected_segmentation_0.polygon):
                is_correct_0 = (
                    is_correct_0
                    and polygon_tuple[0] - prediction_float_margin
                    <= segmentations[0].polygon[index][0]
                    <= polygon_tuple[0] + prediction_float_margin
                    and polygon_tuple[1] - prediction_float_margin
                    <= segmentations[0].polygon[index][1]
                    <= polygon_tuple[1] + prediction_float_margin
                )

            is_correct_1 = True
            for index, polygon_tuple in enumerate(expected_segmentation_1.polygon):
                is_correct_1 = (
                    is_correct_1
                    and polygon_tuple[0] - prediction_float_margin
                    <= segmentations[1].polygon[index][0]
                    <= polygon_tuple[0] + prediction_float_margin
                    and polygon_tuple[1] - prediction_float_margin
                    <= segmentations[1].polygon[index][1]
                    <= polygon_tuple[1] + prediction_float_margin
                )

            is_correct_2 = True
            for index, polygon_tuple in enumerate(expected_segmentation_2.polygon):
                is_correct_2 = (
                    is_correct_2
                    and polygon_tuple[0] - prediction_float_margin
                    <= segmentations[2].polygon[index][0]
                    <= polygon_tuple[0] + prediction_float_margin
                    and polygon_tuple[1] - prediction_float_margin
                    <= segmentations[2].polygon[index][1]
                    <= polygon_tuple[1] + prediction_float_margin
                )

            if not is_correct_0:
                logger.error(
                    "Found wrong segmentation: \n"
                    "  Expected 0:  %s\n"
                    "  Predicted 0: %s\n",
                    expected_segmentation_0,
                    segmentations[0],
                )

                raise ValueError("Output is not valid!")

            if not is_correct_1:
                logger.error(
                    "Found wrong segmentation: \n"
                    "  Expected 1:  %s\n"
                    "  Predicted 1: %s\n",
                    expected_segmentation_1,
                    segmentations[1],
                )

                raise ValueError("Output is not valid!")

            if not is_correct_2:
                logger.error(
                    "Found wrong segmentation: \n"
                    "  Expected 1:  %s\n"
                    "  Predicted 1: %s\n",
                    expected_segmentation_2,
                    segmentations[2],
                )

                raise ValueError("Output is not valid!")

    def test_basic_methods(self):
        mmocr_model = MMOCRTextDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmocr",
                "mlcvzoo_mmocr",
                "tests",
                "test_data",
                "test_mmocr",
                "config",
                "panet",
                "panet_r18_fpem_ffm_600e_ctw1500",
                "panet_r18_fpem_ffm_600e_ctw1500.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        assert mmocr_model.num_classes == 1
        assert mmocr_model.get_classes_id_dict() == {
            MMOCRConfig.__text_class_id__: MMOCRConfig.__text_class_name__
        }

    def test_panet_ctw1500_inference_rect_polygon(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST inference of panet-detector trained on ctw1500-data (mmocr):\n"
            "#      test_panet_ctw1500_inference(self)\n"
            "############################################################"
        )

        mmocr_model = MMOCRTextDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmocr",
                "mlcvzoo_mmocr",
                "tests",
                "test_data",
                "test_mmocr",
                "config",
                "panet",
                "panet_r18_fpem_ffm_600e_ctw1500",
                "panet_r18_fpem_ffm_600e_ctw1500.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        mmocr_model.configuration.inference_config.to_rect_polygon = True

        mmocr_dir = mmocr_model.configuration.string_replacement_map[
            ReplacementConfig.MMOCR_DIR_KEY
        ]

        test_image_path = os.path.join(
            mmocr_dir,
            "tests",
            "data",
            "test_img1.png",
        )

        _, segmentations = mmocr_model.predict(data_item=test_image_path)

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED Segmentations: \n"
            " %s\n"
            "\n==============================================================\n",
            segmentations,
        )

        expected_segmentation_0 = Segmentation(
            polygon=[
                (1115.0, 1433.0),
                (1775.0, 1455.0),
                (1772.0, 1528.0),
                (1112.0, 1506.0),
            ],
            class_identifier=ClassIdentifier(
                class_id=MMOCRConfig.__text_class_id__,
                class_name=MMOCRConfig.__text_class_name__,
            ),
            score=0.9561,
            difficult=False,
            occluded=False,
            content="",
        )

        expected_segmentation_1 = Segmentation(
            polygon=[
                (290.0, 1515.0),
                (627.0, 1515.0),
                (627.0, 1561.0),
                (290.0, 1561.0),
            ],
            class_identifier=ClassIdentifier(
                class_id=MMOCRConfig.__text_class_id__,
                class_name=MMOCRConfig.__text_class_name__,
            ),
            score=0.9735,
            difficult=False,
            occluded=False,
            content="",
        )

        expected_segmentation_2 = Segmentation(
            polygon=[
                (933.0, 1607.0),
                (1958.0, 1607.0),
                (1958.0, 1668.0),
                (933.0, 1668.0),
            ],
            class_identifier=ClassIdentifier(
                class_id=MMOCRConfig.__text_class_id__,
                class_name=MMOCRConfig.__text_class_name__,
            ),
            score=0.9561,
            difficult=False,
            occluded=False,
            content="",
        )

        # Because of rounding errors, the rect polygon coordinates can
        # be a bit more off than usual
        prediction_float_margin = 1.0

        is_correct_0 = True
        for index, polygon_tuple in enumerate(expected_segmentation_0.polygon):
            is_correct_0 = (
                is_correct_0
                and polygon_tuple[0] - prediction_float_margin
                <= segmentations[0].polygon[index][0]
                <= polygon_tuple[0] + prediction_float_margin
                and polygon_tuple[1] - prediction_float_margin
                <= segmentations[0].polygon[index][1]
                <= polygon_tuple[1] + prediction_float_margin
            )

        is_correct_1 = True
        for index, polygon_tuple in enumerate(expected_segmentation_1.polygon):
            is_correct_1 = (
                is_correct_1
                and polygon_tuple[0] - prediction_float_margin
                <= segmentations[1].polygon[index][0]
                <= polygon_tuple[0] + prediction_float_margin
                and polygon_tuple[1] - prediction_float_margin
                <= segmentations[1].polygon[index][1]
                <= polygon_tuple[1] + prediction_float_margin
            )

        is_correct_2 = True
        for index, polygon_tuple in enumerate(expected_segmentation_2.polygon):
            is_correct_2 = (
                is_correct_2
                and polygon_tuple[0] - prediction_float_margin
                <= segmentations[2].polygon[index][0]
                <= polygon_tuple[0] + prediction_float_margin
                and polygon_tuple[1] - prediction_float_margin
                <= segmentations[2].polygon[index][1]
                <= polygon_tuple[1] + prediction_float_margin
            )

        if not is_correct_0:
            logger.error(
                "Found wrong segmentation: \n"
                "  Expected 0:  %s\n"
                "  Predicted 0: %s\n",
                expected_segmentation_0,
                segmentations[0],
            )

            raise ValueError("Output is not valid!")

        if not is_correct_1:
            logger.error(
                "Found wrong segmentation: \n"
                "  Expected 1:  %s\n"
                "  Predicted 1: %s\n",
                expected_segmentation_1,
                segmentations[1],
            )

            raise ValueError("Output is not valid!")

        if not is_correct_2:
            logger.error(
                "Found wrong segmentation: \n"
                "  Expected 1:  %s\n"
                "  Predicted 1: %s\n",
                expected_segmentation_2,
                segmentations[2],
            )

            raise ValueError("Output is not valid!")

        del mmocr_model

    def test_panet_ctw1500_training(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST training of panet-detector trained on ctw1500-data (mmocr):\n"
            "#      test_panet_ctw1500_training(self)\n"
            "############################################################"
        )

        mmocr_model = MMOCRTextDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmocr",
                "mlcvzoo_mmocr",
                "tests",
                "test_data",
                "test_mmocr",
                "config",
                "panet",
                "panet_r18_fpem_ffm_600e_ctw1500",
                "panet_r18_fpem_ffm_600e_ctw1500.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=False,
        )

        try:
            mmocr_model.train()

            assert os.path.isfile(
                os.path.join(
                    self.project_root,
                    "test_output/panet_r18_fpem_ffm_600e_ctw1500/latest.pth",
                )
            )

            del mmocr_model
        except RuntimeError as re:
            del mmocr_model

            if "CUDA out of memory" in str(re):
                self.skipTest(
                    "Could not test training of panet (mmocr). GPU memory is to small"
                )
            else:
                raise re

    def test_panet_ctw1500_training_distributed(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST distributed training of panet-detector trained on ctw1500-data (mmocr):\n"
            "#      test_panet_ctw1500_training_distributed(self)\n"
            "############################################################"
        )

        mmocr_model = MMOCRTextDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_mmocr",
                "mlcvzoo_mmocr",
                "tests",
                "test_data",
                "test_mmocr",
                "config",
                "panet",
                "panet_r18_fpem_ffm_600e_ctw1500__distributed",
                "panet_r18_fpem_ffm_600e_ctw1500__distributed.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=False,
        )

        try:
            mmocr_model.train()

            del mmocr_model
        except RuntimeError as re:
            del mmocr_model

            if "CUDA out of memory" in str(re):
                self.skipTest(
                    "Could not test distributed training of panet (mmocr). GPU memory is to small"
                )
            else:
                raise re


if __name__ == "__main__":
    main()

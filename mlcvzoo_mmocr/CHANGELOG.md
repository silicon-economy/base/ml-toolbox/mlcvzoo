# MLCVZoo mlcvzoo_mmocr module Versions:

5.0.1 (2023-05-11):
----------------
Python 3.10 compatibility

5.0.0 (2023-02-10):
----------------
Implement API changes introduced by mlcvzoo-base version 5.0.0 and
mlcvzoo-mmdetetection version 5.0.0:
- Remove detector-config and use the feature of the single ModelConfiguration
- Remove duplicate attributes

4.1.0 (2022-11-18):
------------------
Make the "from_yaml" of the MMOCRModel, MMOCRTextDetectionModel and MMOCRTextRecognitionModel constructor Optional

4.0.2 (2022-09-09):
------------------
Ensure ConfigBuilder version 7 compatibility

4.0.1 (2022-08-11):
------------------
Fix processing of result in predict many function

4.0.0 (2022-08-09):
------------------
- Adapt to mlcvzoo-base 4.0.0 and mlcvzoo-mmdetection 4.0.0
- Refactor and enhance mlcvzoo_mmocr
  - The MMOCRModel inherits from MMDetectionModel which is the base of all models of open-mmlab
  - Remove the SegmentationDataset and replace it with the MLCVZooMMOcrDataset

3.1.1 (2022-07-14):
------------------
Prepare package for PyPi

3.1.0 (2022-07-13):
------------------
Add feature: Implement API feature "predict on many data-items" respectively "batch-inference"
for the MMOCRTextDetectionModel and MMOCRTextRecognitionModel

3.0.0 (2022-05-17):
------------------
Use new features from AnnotationClassMapper that have been added with mlcvzoo_base v3.0.0

2.0.1 (2022-05-16)
------------------
Changed python executable for distributed training
- It can happen that the system python and python for running code are not the same. When starting distributed training, the system python was called.
- Now the python executable that runs the code is also executed when starting distributed (multi gpu) training.

2.0.0 (2022-04-05)
------------------
- initial release of the package

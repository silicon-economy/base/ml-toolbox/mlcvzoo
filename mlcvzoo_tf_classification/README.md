# MLCVZoo TF Classification

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_tf_classification** is the wrapper module
for classification algorithms that are implemented using tensorflow.

Further information about the MLCVZoo can be found [here](../README.md).

## Install
`
pip install mlcvzoo-tf-classification
`

## Technology stack

- Python

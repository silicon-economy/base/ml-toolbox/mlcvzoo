# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Test class for CustomBlockModel class.
"""

import logging
import os
from typing import Dict, cast
from unittest import TestCase, main, skip

import tensorflow as tf
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils.file_utils import get_project_path_information
from related import to_model
from tensorflow.keras.preprocessing.image import img_to_array, load_img

from mlcvzoo_tf_classification.custom_block.configuration import CustomBlockConfig
from mlcvzoo_tf_classification.custom_block.model import CustomBlockModel

logger = logging.getLogger(__name__)


class TestCustomBlockModel(TestCase):
    """
    Test class for CustomBLockModel class.
    """

    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=5, code_base="mlcvzoo_tf_classification"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

        # setting seed is necessary for deterministic prediction results
        tf.random.set_seed(42)  # TODO: add option in net and net config

    def __custom_block_predict_and_evaluate(self, data_item) -> None:
        """
        Calls the model's predict method and runs evaluation of prediction for
        a given data item.
        """

        yaml_path = os.path.join(
            self.project_root,
            "mlcvzoo_tf_classification",
            "mlcvzoo_tf_classification",
            "tests",
            "test_data",
            "test_CustomBlock",
            "custom_block_from_scratch_imagenet.yaml",
        )

        custom_block_model = CustomBlockModel(
            from_yaml=yaml_path, string_replacement_map=self.string_replacement_map
        )

        _, predictions = custom_block_model.predict(data_item)

        if len(predictions) > 0:
            logger.info(
                "predictions:\n" "amount %s, type: %s\n" "top entries: %s,\n",
                len(predictions),
                type(predictions),
                predictions[: custom_block_model.configuration.inference_config.top],
            )
        else:
            raise ValueError("No predictions")

        # TODO: Is this even possible?
        for prediction in predictions:
            if not prediction.class_id and prediction.class_name:
                raise ValueError("Broken prediction %r", prediction)

    def test_configuration(self):
        configuration = CustomBlockModel.create_configuration(
            from_yaml=os.path.join(
                self.project_root,
                "mlcvzoo_tf_classification",
                "mlcvzoo_tf_classification",
                "tests",
                "test_data",
                "test_CustomBlock",
                "custom_block_from_scratch_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        assert configuration is not None

        configuration_2 = cast(
            CustomBlockConfig, to_model(CustomBlockConfig, configuration.to_dict())
        )
        assert configuration_2 is not None

    def test_custom_block_inference_from_file(self) -> None:
        """Tests whether prediction from a file works"""

        test_image_path = os.path.join(
            self.project_root, "test_data/images/dummy_task/cars.jpg"
        )

        self.__custom_block_predict_and_evaluate(test_image_path)

    def test_custom_block_inference_from_array(self) -> None:
        """Tests whether prediction from a loaded data array works."""

        test_image_path = os.path.join(
            self.project_root, "test_data/images/dummy_task/cars.jpg"
        )

        image = load_img(test_image_path)
        data_array = img_to_array(image)

        self.__custom_block_predict_and_evaluate(data_array)

    def test_custom_block_training_from_scratch(self) -> None:
        """
        Tests whether training of a model from scratch works when
        loading data from a data frame.
        """

        yaml_path = os.path.join(
            self.project_root,
            "mlcvzoo_tf_classification",
            "mlcvzoo_tf_classification",
            "tests",
            "test_data",
            "test_CustomBlock",
            "custom_block_from_scratch_test.yaml",
        )

        custom_block_model = CustomBlockModel(
            from_yaml=yaml_path, string_replacement_map=self.string_replacement_map
        )
        custom_block_model.train()

    def test_custom_block_training_from_scratch_from_dir(self) -> None:
        """
        Tests whether training of a model from scratch works when
        loading data from a directory.
        """

        yaml_path = os.path.join(
            self.project_root,
            "mlcvzoo_tf_classification",
            "mlcvzoo_tf_classification",
            "tests",
            "test_data",
            "test_CustomBlock",
            "custom_block_from_scratch_from_dir_test.yaml",
        )

        custom_block_model = CustomBlockModel(
            from_yaml=yaml_path, string_replacement_map=self.string_replacement_map
        )
        custom_block_model.train()

    def test_custom_block_save_and_load_model(self) -> None:
        """
        Tests whether saving of a model and resuming training
        from the saved state works.
        """

        yaml_path = os.path.join(
            self.project_root,
            "mlcvzoo_tf_classification",
            "mlcvzoo_tf_classification",
            "tests",
            "test_data",
            "test_CustomBlock",
            "custom_block_from_scratch_from_dir_test.yaml",
        )

        custom_block_model = CustomBlockModel(
            from_yaml=yaml_path, string_replacement_map=self.string_replacement_map
        )

        # train model and save checkpoints along the way
        custom_block_model.train()

        # save model
        model_path = os.path.join(
            custom_block_model.configuration.train_config.model_checkpoint_config.work_dir,
            "model",
        )
        custom_block_model.store(checkpoint_path=model_path)

        # load model
        custom_block_model.restore(checkpoint_path=model_path)

        # resume training with restored weights
        custom_block_model.train()

    def test_custom_block_save_and_load_checkpoints(self) -> None:
        """
        Tests whether saving checkpoints of a model and resuming
        training from the saved state works.
        """

        yaml_path = os.path.join(
            self.project_root,
            "mlcvzoo_tf_classification",
            "mlcvzoo_tf_classification",
            "tests",
            "test_data",
            "test_CustomBlock",
            "custom_block_from_scratch_from_dir_test.yaml",
        )

        custom_block_model = CustomBlockModel(
            from_yaml=yaml_path, string_replacement_map=self.string_replacement_map
        )

        # train model and save checkpoints along the way
        custom_block_model.train()

        # load model from checkpoint
        checkpoint_id = "{0:04d}".format(
            custom_block_model.configuration.train_config.epochs
        )
        checkpoint_path = os.path.join(
            custom_block_model.configuration.train_config.model_checkpoint_config.work_dir,
            "cp-" + checkpoint_id + ".ckpt",
        )
        custom_block_model.restore(checkpoint_path=checkpoint_path)

        # resume training with restored weights
        custom_block_model.train()

    # def test_custom_block_training_pretrained(self) -> None:
    #     """
    #     Tests whether training of a pretrained model works when
    #     loading data from a directory.
    #     """
    #
    #     raise NotImplementedError(self.__class__.__name__
    #                               + '.test_custom_block_training_pretrained')
    #
    # def test_custom_block_inference_top(self) -> None:
    #     """Tests whether prediction from a file delivers top x classes as configured."""
    #
    #     raise NotImplementedError(self.__class__.__name__
    #                               + '.test_custom_block_inference_top')
    #
    # def test_custom_block_inference_score(self) -> None:
    #     """
    #     Tests whether prediction from a file delivers classes that have at
    #     minimum a prediction score as configured.
    #     """
    #
    #     raise NotImplementedError(self.__class__.__name__
    #                               + '.test_custom_block_inference_score')


if __name__ == "__main__":
    main()

# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Package that handles the wrapper implementation of the tensorflow Xception model.
A model for image classification.
"""

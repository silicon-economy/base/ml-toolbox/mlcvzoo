# MLCVZoo-Service Example:

Quick steps to try out this "Hello MLCVZoo"-Example:

Runs the app on http://127.0.0.1:5000:
```
'flask --app index run'
```

Call default() to get the job_queue
```
'curl 127.0.0.1:5000'
```

Call train() to put an additional json item into the job queue:
```
'curl -X POST -H "Content-Type: application/json" -d '{"foo":"bar"}' 127.0.0.1:5000/train'
```

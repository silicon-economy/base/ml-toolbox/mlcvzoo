# MLCVZoo Examples

This is a simple collection of best-practice examples of using the MLCVZoo components.
Every example is designed as a python and a bash script. It is recommended to simply use
the bash script for running an example. Additionally, you can install the necessary packages via the
[requirements.txt](requirements.txt) and run the python script itself.

## Example 1 - [Inference and ModelRegistry](mlcvzoo_inference_and_model_registry_example/mlcvzoo_inference_and_model_registry_example.py)

Simple example of using the MLCVZoo ModelRegistry for an inference of a
generic Objection Detection Model. The heart is a model-registry.yaml that
defines all the specific model configurations, while the ModelRegistry handles
the initialisation of specific MLCVZoo modules. The script of the example can be found
[here](mlcvzoo_inference_and_model_registry_example/mlcvzoo_inference_and_model_registry_example.sh).

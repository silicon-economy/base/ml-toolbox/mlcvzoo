import argparse
import logging
import os
import time
from typing import List, Tuple, cast

import cv2
import numpy as np
import related
from config_builder import BaseConfigClass, ConfigBuilder

from mlcvzoo_base.api.data.annotation_class_mapper import AnnotationClassMapper
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.model import ObjectDetectionModel
from mlcvzoo_base.configuration.model_config import ModelConfig
from mlcvzoo_base.configuration.replacement_config import ReplacementConfig
from mlcvzoo_base.models.model_registry import ModelRegistry
from mlcvzoo_base.utils import (
    draw_bbox_cv2,
    draw_on_image,
    generate_detector_colors,
    get_project_path_information,
)
from mlcvzoo_tracker.configuration import TrackerConfig
from mlcvzoo_tracker.hungarian_image_tracker import HungarianImageTracker
from mlcvzoo_util.image_io_utils import VideoFileInput, VideoLiveOutput

logger = logging.getLogger(__name__)


@related.mutable(strict=True)
class ExampleConfig(BaseConfigClass):
    od_model_config: ModelConfig = related.ChildField(cls=ModelConfig)
    tracker_config: TrackerConfig = related.ChildField(cls=TrackerConfig)


def parse_arguments() -> argparse.Namespace:
    """
    Returns:
        args: Parsed commandline arguments
    """

    parser = argparse.ArgumentParser(
        description="ḾLCVZoo example for the inference of an ObjectDetectionModel"
        " utilizing the ModelRegistry"
    )

    parser.add_argument(
        "--visualize-output",
        action="store_true",
        help="Show the predicted bounding-box in an image",
    )

    return parser.parse_args()


def annotate_frame(
    frame: np.ndarray,
    trackers: List[HungarianImageTracker],
    tracker_rgb_colors: List[Tuple[int, int, int]],
):
    for tracker in trackers:
        for track_id, track in enumerate(tracker.get_tracks()):
            # only continue with valid tracks
            if not track.is_valid():
                continue

            track_event = track.get_latest_track_event()

            if track_event is not None:
                frame = draw_bbox_cv2(
                    frame=frame,
                    color=tracker_rgb_colors[track_id],
                    box=track_event.bounding_box.box,
                    flip_image=False,
                    label=track_event.bounding_box.class_name,
                    draw_caption=False,
                )

    return frame


def main() -> None:
    # ======================================================================================
    # Basic initialisation of logging setup and configuration
    #

    args = parse_arguments()

    (
        this_dir,
        project_root,
        code_root,
    ) = get_project_path_information(
        file_path=__file__, dir_depth=2, code_base="examples"
    )

    try:
        from mlcvzoo_util.logger.logger import Logger

        Logger.init_logging_basic(
            log_dir=os.path.join(project_root, "logs"),
            log_file_postfix="mlcvzoo_tracker_example",
            no_stdout=False,
            root_log_level=logging.DEBUG,
        )
    except ImportError:
        print("Logger not available, don't initialize logging for now")

    replacement_map = {
        ReplacementConfig.CONFIG_ROOT_DIR_DIR_KEY: os.path.join(
            project_root, "examples/config"
        ),
        ReplacementConfig.PROJECT_ROOT_DIR_KEY: project_root,
    }

    example_config = cast(
        ExampleConfig,
        ConfigBuilder(
            class_type=ExampleConfig,
            yaml_config_path=os.path.join(
                project_root,
                "examples/config/mlcvzoo_tracker_example_config.yaml",
            ),
            string_replacement_map=replacement_map,
        ).configuration,
    )

    # example_config.od_model_config.set_inference(inference=True)
    object_detection_model = ModelRegistry().init_model(
        model_config=example_config.od_model_config,
        string_replacement_map=example_config.string_replacement_map,
    )

    if not isinstance(object_detection_model, ObjectDetectionModel):
        raise ValueError(
            "This evaluation can only be used with models that "
            "inherit from 'mlcvzoo.api.model.ObjectDetectionModel'"
        )

    mapper: AnnotationClassMapper = object_detection_model.mapper

    tracker_rgb_colors = generate_detector_colors(num_classes=mapper.num_classes)

    images = [
        os.path.join(
            project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )
    ] * 10

    video_live_output = VideoLiveOutput(
        window_name="Tracking", window_size=650, mode=VideoLiveOutput.MODE_STEP
    )

    prediction_time_list = list()
    tracking_time_list = list()

    trackers = [
        HungarianImageTracker(
            configuration=example_config.tracker_config,
            object_class_identifier=ClassIdentifier(
                class_id=class_id, class_name=class_name
            ),
        )
        for class_id, class_name in mapper.annotation_class_id_to_model_class_name_map.items()
    ]

    for frame_number, image_path in enumerate(images):
        frame = cv2.imread(image_path)

        # ===================================================================================
        # RUN Object Detection
        # ===================================================================================

        start_predict = time.time()

        _, bounding_boxes = object_detection_model.predict(data_item=frame)

        end_predict = time.time() - start_predict
        prediction_time_list.append(end_predict)

        if len(prediction_time_list) > 0:
            avg_predict_time = sum(prediction_time_list) / len(prediction_time_list)
        else:
            avg_predict_time = 0.0

        # ===================================================================================
        # RUN Tracking
        # ===================================================================================

        start_track = time.time()

        # Call next_frame for every tracker as "sensor update"
        for index, tracker in enumerate(trackers):
            tracker.next(
                [
                    d
                    for d in bounding_boxes
                    if d.class_id == tracker.object_class_identifier.class_id
                ],
                frame,
            )

        end_track = time.time() - start_track
        tracking_time_list.append(end_track)

        if len(tracking_time_list) > 0:
            avg_tracking_time = sum(tracking_time_list) / len(tracking_time_list)
        else:
            avg_tracking_time = 0.0

        # ======================================================================================
        # POST PROCESS OUTPUT
        # ======================================================================================

        logger.info(
            "prediction time: {:.4f}, avg: {:.4f} | tracking time: {:.4f}, avg: {:.4f}".format(
                end_predict, avg_predict_time, end_track, avg_tracking_time
            )
        )

        logger.info(f"bbs: {bounding_boxes}")

        # ======================================================================================
        # Visualize the output
        # ======================================================================================

        video_live_output.output_frame(
            annotate_frame(
                frame=frame,
                trackers=trackers,
                tracker_rgb_colors=tracker_rgb_colors,
            )
        )


if __name__ == "__main__":
    main()

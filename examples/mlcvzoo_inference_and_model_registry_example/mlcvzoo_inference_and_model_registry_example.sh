#!/bin/bash

# NOTE: This script is meant to be executed from the project root directory

export PROJECT_ROOT_DIR="$1"

export PYTHONPATH="$PROJECT_ROOT_DIR/mlcvzoo_base"

python3 "$PROJECT_ROOT_DIR/examples/mlcvzoo_inference_and_model_registry_example/mlcvzoo_inference_and_model_registry_example.py" $2

# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import argparse
import logging
import os
from typing import cast

import cv2
import related
from config_builder import BaseConfigClass, ConfigBuilder

from mlcvzoo_base.api.model import ObjectDetectionModel
from mlcvzoo_base.configuration.model_config import ModelConfig
from mlcvzoo_base.configuration.replacement_config import ReplacementConfig
from mlcvzoo_base.models.model_registry import ModelRegistry
from mlcvzoo_base.utils.draw_utils import draw_on_image, generate_detector_colors
from mlcvzoo_base.utils.file_utils import get_project_path_information

logger = logging.getLogger(__name__)


@related.mutable(strict=True)
class ExampleConfig(BaseConfigClass):
    object_detection_model: ModelConfig = related.ChildField(cls=ModelConfig)


def parse_arguments() -> argparse.Namespace:
    """
    Returns:
        args: Parsed commandline arguments
    """

    parser = argparse.ArgumentParser(
        description="ḾLCVZoo example for the inference of an ObjectDetectionModel"
        " utilizing the ModelRegistry"
    )

    parser.add_argument(
        "--visualize-output",
        action="store_true",
        help="Show the predicted bounding-box in an image",
    )

    return parser.parse_args()


def main():
    # ======================================================================================
    # Basic initialisation of logging setup and configuration
    #

    args = parse_arguments()

    (
        this_dir,
        project_root,
        code_root,
    ) = get_project_path_information(
        file_path=__file__, dir_depth=2, code_base="examples"
    )

    try:
        from mlcvzoo_base.tools.logger import Logger

        Logger.init_logging_basic(
            log_dir=os.path.join(project_root, "logs"),
            log_file_postfix="mlcvzoo_inference_and_model_registry_example",
            no_stdout=False,
            root_log_level=logging.DEBUG,
        )
    except ImportError:
        print("Logger not available, don't initialize logging for now")

    replacement_map = {
        ReplacementConfig.CONFIG_ROOT_DIR_DIR_KEY: os.path.join(
            project_root, "examples/config"
        ),
        ReplacementConfig.PROJECT_ROOT_DIR_KEY: project_root,
    }

    example_config = cast(
        ExampleConfig,
        ConfigBuilder(
            class_type=ExampleConfig,
            yaml_config_path=os.path.join(
                project_root,
                "examples/config/mlcvzoo_inference_and_model_registry_example_config.yaml",
            ),
            string_replacement_map=replacement_map,
        ).configuration,
    )

    # ======================================================================================
    # Generic initialisation of an ObjectDetectionModel
    #
    # In the ExampleConfig, the object detection model is defined by an relative import via the
    # !join_object object yaml constructor that is provided by the config-builder:
    #
    # object_detection_model: !join_object ["read_from_file_v1", "CONFIG_ROOT_DIR/model-registry.yaml"]
    #
    # By using this functionality, it is enough to exchange the first parameter
    # "read_from_file_v1" to get the desired model-config of the model-registry.yaml.
    # In the model-registry.yaml you can "register" any model by listing a model-config
    # beneath a unique model-identifier.

    model_registry = ModelRegistry()

    object_detection_model = model_registry.init_model(
        model_config=example_config.object_detection_model,
        string_replacement_map=replacement_map,
    )

    if not isinstance(object_detection_model, ObjectDetectionModel):
        raise ValueError("Please provide a ObjectDetectionModel configuration")

    # ======================================================================================
    # Use the ObjectDetectionModel to predict bounding boxes of an image
    #

    image_path = os.path.join(
        project_root,
        "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
    )

    _, bounding_boxes = object_detection_model.predict(data_item=image_path)

    logger.info(
        "Predicted bounding-boxes: \n\t-%s"
        % "\n\t-".join([str(b) for b in bounding_boxes])
    )

    # ======================================================================================
    # Visualize the output
    #
    if args.visualize_output:
        image = cv2.imread(image_path)

        draw_image = draw_on_image(
            frame=image,
            rgb_colors=generate_detector_colors(
                num_classes=object_detection_model.num_classes
            ),
            bounding_boxes=bounding_boxes,
            draw_caption=True,
            font_scale=3.0,
        )

        window_name = "Detections"
        cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
        cv2.imshow(window_name, draw_image)
        cv2.waitKey(0)


if __name__ == "__main__":
    main()

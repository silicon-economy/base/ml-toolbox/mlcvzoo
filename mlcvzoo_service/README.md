# MLCVZoo Service

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_service** is a  REST service to train
arbitrary MLCVZoo models.

Further information about the MLCVZoo can be found [here](../README.md).

## Install
`
pip install mlcvzoo-service
`

## Technology stack

- Python

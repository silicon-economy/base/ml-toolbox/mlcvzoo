# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
TODO
"""

from __future__ import annotations

import logging
import os
import uuid
from multiprocessing import Event
from multiprocessing.synchronize import Event as EventType
from pathlib import Path
from threading import Thread
from typing import Any, Callable, Dict, List, Optional, cast

import mlflow
from mlcvzoo_base.api.configuration import ModelConfiguration
from mlcvzoo_base.api.interfaces import NetBased, Trainable
from mlcvzoo_base.api.model import Model
from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig
from mlcvzoo_base.configuration.model_config import ModelConfig
from mlcvzoo_base.models.model_registry import ModelRegistry
from mlcvzoo_util.model_evaluator.configuration import (
    CheckpointConfig,
    ModelEvaluatorCLIConfig,
)
from mlcvzoo_util.model_evaluator.model_evaluator import run_evaluation
from mlcvzoo_util.model_trainer.configuration import ModelTrainerConfig
from mlcvzoo_util.model_trainer.model_trainer import ModelTrainer
from mlflow import ActiveRun
from related import to_model

from mlcvzoo_service.adapter.data_location import Location
from mlcvzoo_service.adapter.mlflow_credentials import MLFlowCredentials
from mlcvzoo_service.adapter.s3.adapter import S3StorageAdapter
from mlcvzoo_service.constants import TrainingStatus
from mlcvzoo_service.utils import update_string_replacement_map

logger = logging.getLogger(__name__)


class TrainingJob(Thread):
    """
    Encapsulates a single training job in the mlcvzoo service.
    """

    __training_output_dir_key__: str = "TRAINING_OUTPUT_DIR"

    def __init__(
        self,
        image_locations: List[Location],
        annotation_locations: List[Location],
        initial_model_checkpoint_location: Location,
        model_type_name: str,
        model_configuration_dict: Dict[str, Any],
        evaluator_configuration_dict: Dict[str, Any],
        execute_as_thread: bool,
        job_finished_callback: Callable[[TrainingJob], None],
        result_model_checkpoint_location: Optional[Location] = None,
        mlflow_credentials: Optional[MLFlowCredentials] = None,
    ):
        self.__id: uuid.UUID = uuid.uuid4()

        self._status: TrainingStatus = TrainingStatus.NEW
        logger.info("New Training job initiated")

        self.image_locations: List[Location] = image_locations
        self.annotation_locations: List[Location] = annotation_locations
        self.initial_model_checkpoint_location: Location = (
            initial_model_checkpoint_location
        )
        self.result_model_checkpoint_location: Optional[
            Location
        ] = result_model_checkpoint_location

        self.job_finished_callback: Callable[
            [TrainingJob], None
        ] = job_finished_callback
        self.mlflow_credentials: Optional[MLFlowCredentials] = mlflow_credentials

        self._processing_shutdown_event: EventType = Event()
        self.__training_data_dir: Path
        self._name = "TrainingJob"

        self.model_type_name = model_type_name
        self.model_configuration: ModelConfiguration = (
            TrainingJob.build_model_configuration(
                model_type_name=self.model_type_name,
                model_configuration_dict=model_configuration_dict,
            )
        )
        self.model_config: ModelConfig = TrainingJob.build_model_config(
            model_type_name=self.model_type_name,
            model_configuration=self.model_configuration,
        )

        self.evaluator_configuration = TrainingJob.build_model_evaluator_config(
            model_config=self.model_config,
            evaluator_configuration_dict=evaluator_configuration_dict,
        )

        self.string_replacement_map: Dict[str, str] = {}

        if execute_as_thread:
            Thread.__init__(
                self,
            )

    @property
    def status(self) -> TrainingStatus:
        return self._status

    @property
    def id(self) -> uuid.UUID:
        return self.__id

    @staticmethod
    def build_model_configuration(
        model_type_name: str, model_configuration_dict: Dict[str, Any]
    ) -> ModelConfiguration:
        return cast(
            ModelConfiguration,
            to_model(
                cls=ModelRegistry().determine_config_class(
                    model_type_name=model_type_name
                ),
                value=model_configuration_dict,
            ),
        )

    @staticmethod
    def build_model_config(
        model_type_name: str, model_configuration: Any
    ) -> ModelConfig:
        return ModelConfig(
            class_type=model_type_name,
            constructor_parameters={
                "configuration": model_configuration,
            },
        )

    @staticmethod
    def build_model_trainer_config(model_config: ModelConfig) -> ModelTrainerConfig:
        return ModelTrainerConfig(model_config=model_config)

    @staticmethod
    def build_model_evaluator_config(
        model_config: ModelConfig,
        evaluator_configuration_dict: Dict[str, Any],
    ) -> ModelEvaluatorCLIConfig:
        return ModelEvaluatorCLIConfig(
            iou_thresholds=evaluator_configuration_dict["iou_thresholds"],
            model_config=model_config,
            checkpoint_config=CheckpointConfig(
                checkpoint_dir="", checkpoint_filename_suffix=""
            ),
            annotation_handler_config=to_model(
                cls=AnnotationHandlerConfig,
                value=evaluator_configuration_dict["annotation_handler_config"],
            ),
        )

    def run(self) -> None:
        """
        Main function of the TrainingJob.
        By calling TrainingJob.start() it is executed as in a Thread

        Returns:
            None
        """
        self._status = TrainingStatus.RUNNING

        logger.info("Start training of TrainingJob: '%s'" % self.id)

        try:
            string_replacement_map: Dict[str, str] = self.__load_training_data()
            self.__train(string_replacement_map=string_replacement_map)

            self._status = TrainingStatus.FINISHED
        except Exception as e:
            logger.exception(e)
            self._status = TrainingStatus.CRASHED
        finally:
            self.job_finished_callback(self)

    def __load_training_data(self) -> Dict[str, str]:
        """
        Loads the necessary data to start the training run. This includes the training images, the
        corresponding annotation files and the initial training checkpoint

        Returns:
            None
        """

        string_replacement_map: Dict[str, str] = {}

        for location in (
            [self.initial_model_checkpoint_location]
            + self.image_locations
            + self.annotation_locations
        ):
            logger.info("Downloading from location {}".format(location))

            if isinstance(location, Location):
                adapter = S3StorageAdapter(location)
                string_replacement_map, downloaded_files = adapter.download_objects(
                    string_replacement_map=self.string_replacement_map
                )
                string_replacement_map.update(string_replacement_map)
            else:
                raise KeyError("Storage type of class %s not supported", type(location))

        return string_replacement_map

    def __push_checkpoint(self, checkpoint_path: str) -> None:
        """
        Push checkpoint to a storage

        Args:
            checkpoint_path: Path of the checkpoint to push
        """
        if self.result_model_checkpoint_location is not None:
            adapter = S3StorageAdapter(self.result_model_checkpoint_location)
            adapter.upload_object(checkpoint_path)

    @staticmethod
    def __register_mlflow_model(unique_name: str) -> None:
        active_run: Optional[ActiveRun] = mlflow.active_run()
        if active_run is not None:
            # Register Model before starting the training
            run_id = mlflow.active_run().info.run_id
            model_uri = f"runs:/{run_id}"

            mlflow.register_model(
                model_uri=model_uri,
                name=unique_name,
            )

    def __train(self, string_replacement_map: Dict[str, str]) -> None:
        training_output_dir = os.getenv(
            TrainingJob.__training_output_dir_key__, os.getcwd()
        )

        update_string_replacement_map(
            string_replacement_map=string_replacement_map,
            replacement_key=TrainingJob.__training_output_dir_key__,
            replacement_value=training_output_dir,
        )

        # TODO: Add config-builder feature that allows to run the replacement
        #       on an BaseConfigClass object inside a dictionary
        cast(
            ModelConfiguration,
            self.model_config.constructor_parameters["configuration"],
        ).recursive_string_replacement(string_replacement_map=string_replacement_map)

        self.evaluator_configuration.recursive_string_replacement(
            string_replacement_map=string_replacement_map
        )

        if self.mlflow_credentials is not None:
            self.mlflow_credentials.update_os_environment()

            # set uri for tracking server
            mlflow.set_tracking_uri(self.mlflow_credentials.server_uri)

            # TODO: When we execute more than one mlflow run at a time,
            #       we have to make sure that they don't share the same
            #       mlflow run
            mlflow.end_run()
            mlflow.start_run(run_id=self.mlflow_credentials.run_id)

        model_trainer = ModelTrainer(
            configuration=TrainingJob.build_model_trainer_config(
                model_config=self.model_config
            )
        )

        trained_model = model_trainer.run_training()

        assert isinstance(trained_model, Trainable) and isinstance(
            trained_model, NetBased
        )

        self.evaluator_configuration.checkpoint_config = CheckpointConfig(
            checkpoint_dir=trained_model.get_training_output_dir(),
            checkpoint_filename_suffix=trained_model.get_checkpoint_filename_suffix(),
        )

        _, best_checkpoint_info = run_evaluation(
            configuration=self.evaluator_configuration, single_mode=False
        )

        if mlflow.active_run() is not None:
            mlflow.log_param(
                "best_checkpoint", os.path.basename(best_checkpoint_info.path)
            )
            mlflow.log_metric("score", best_checkpoint_info.score)

        self.__push_checkpoint(checkpoint_path=best_checkpoint_info.path)

        assert isinstance(trained_model, Model)
        self.__register_mlflow_model(unique_name=self.model_configuration.unique_name)

        del trained_model
        mlflow.end_run()

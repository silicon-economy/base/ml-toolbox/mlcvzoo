# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import os
import unittest
from datetime import datetime
from typing import Dict, Optional
from unittest import TestCase, mock
from unittest.mock import MagicMock

from botocore.exceptions import EndpointConnectionError
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils import get_project_path_information
from pytest import fixture, mark
from pytest_mock import MockerFixture

from mlcvzoo_service.adapter.data_location import Location
from mlcvzoo_service.adapter.s3.adapter import S3Data, S3StorageAdapter

logger = logging.getLogger(__name__)


@fixture(scope="function")
def list_object_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_service.adapter.s3.adapter.S3StorageAdapter._list_objects",
        return_value={
            "ResponseMetadata": {
                "RequestId": "1749E48BEB07EFC5",
                "HostId": "",
                "HTTPStatusCode": 200,
                "HTTPHeaders": {
                    "accept-ranges": "bytes",
                    "content-length": "694",
                    "content-security-policy": "block-all-mixed-content",
                    "content-type": "application/xml",
                    "server": "s3",
                    "strict-transport-security": "max-age=31536000; includeSubDomains",
                    "vary": "Origin, Accept-Encoding",
                    "x-amz-bucket-region": "eu-central-1",
                    "x-amz-request-id": "1749E48BEB07EFC5",
                    "x-content-type-options": "nosniff",
                    "x-xss-protection": "1; mode=block",
                    "date": "Mon, 06 Mar 2023 17:27:05 GMT",
                    "set-cookie": "3b030adc0366ddb7aecd60a4ff017306=5ab7b0c5034fa99ea424d2bb5bd92ea9; path=/; HttpOnly; Secure; SameSite=None",
                    "cache-control": "private",
                },
                "RetryAttempts": 0,
            },
            "IsTruncated": False,
            "Contents": [
                {
                    "Key": "test-bucket/test_file.txt",
                    "LastModified": datetime(2023, 3, 6, 17, 25, 42, 91000),
                    "ETag": '"2f50e06ff9729cf41b81112d48c3c1c4"',
                    "Size": 7694953,
                    "StorageClass": "STANDARD",
                    "Owner": {
                        "DisplayName": "s3",
                        "ID": "02d6176db174dc93cb1b899f7c6078f08654445fe8cf1b6ce98d8855f66bdbf4",
                    },
                }
            ],
            "Name": "bucket",
            "Prefix": "test-bucket/test_file.txt",
            "Delimiter": "",
            "MaxKeys": 1000,
            "EncodingType": "url",
            "KeyCount": 1,
        },
    )


@fixture(scope="function")
def list_object_no_files_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_service.adapter.s3.adapter.S3StorageAdapter._list_objects",
        return_value={
            "ResponseMetadata": {
                "RequestId": "1749E48BEB07EFC5",
                "HostId": "",
                "HTTPStatusCode": 200,
                "HTTPHeaders": {
                    "accept-ranges": "bytes",
                    "content-length": "694",
                    "content-security-policy": "block-all-mixed-content",
                    "content-type": "application/xml",
                    "server": "s3",
                    "strict-transport-security": "max-age=31536000; includeSubDomains",
                    "vary": "Origin, Accept-Encoding",
                    "x-amz-bucket-region": "eu-central-1",
                    "x-amz-request-id": "1749E48BEB07EFC5",
                    "x-content-type-options": "nosniff",
                    "x-xss-protection": "1; mode=block",
                    "date": "Mon, 06 Mar 2023 17:27:05 GMT",
                    "set-cookie": "3b030adc0366ddb7aecd60a4ff017306=5ab7b0c5034fa99ea424d2bb5bd92ea9; path=/; HttpOnly; Secure; SameSite=None",
                    "cache-control": "private",
                },
                "RetryAttempts": 0,
            },
            "IsTruncated": False,
            "Contents": [],
            "Name": "bucket",
            "Prefix": "test-bucket/test_file.txt",
            "Delimiter": "",
            "MaxKeys": 1000,
            "EncodingType": "url",
            "KeyCount": 1,
        },
    )


@fixture(scope="function")
def download_file_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_service.adapter.s3.adapter.S3StorageAdapter._download_file",
        return_value="test_file.txt",
    )


class TestStorageAdapter(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_service"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

    @mark.usefixtures(
        "list_object_mock",
        "download_file_mock",
    )
    def test_download_objects(self) -> None:
        os_value = os.getenv(key="TEST_OUTPUT")

        os.environ["TEST_OUTPUT"] = os.path.join(self.project_root, "test_output")

        exception: Optional[Exception] = None
        try:
            storage_adapter = S3StorageAdapter(
                location=Location(
                    uri="http://127.0.0.1:9000/test-bucket/test_file.txt",
                    username="testuser",
                    password="12345678",
                    location_id="TEST_OUTPUT",
                )
            )

            string_replacement_map, downloaded_files = storage_adapter.download_objects(
                string_replacement_map={}
            )

            assert string_replacement_map == {
                "TEST_OUTPUT": os.path.join(self.project_root, "test_output")
            }
            assert len(downloaded_files) == 1
            assert downloaded_files[0] == "test_file.txt"
        except Exception as e:
            exception = e

        if os_value is None:
            os.environ.pop("TEST_OUTPUT")
        else:
            os.environ["TEST_OUTPUT"] = os_value

        if exception is not None:
            raise exception

    @mark.usefixtures(
        "list_object_no_files_mock",
        "download_file_mock",
    )
    def test_download_objects_no_files(self) -> None:
        os_value = os.getenv(key="TEST_OUTPUT")

        os.environ["TEST_OUTPUT"] = os.path.join(self.project_root, "test_output")

        exception: Optional[Exception] = None
        try:
            storage_adapter = S3StorageAdapter(
                location=Location(
                    uri="http://127.0.0.1:9000/test-bucket/test_file.txt",
                    username="testuser",
                    password="12345678",
                    location_id="TEST_OUTPUT",
                )
            )

            string_replacement_map, downloaded_files = storage_adapter.download_objects(
                string_replacement_map={}
            )

            assert string_replacement_map == {
                "TEST_OUTPUT": os.path.join(self.project_root, "test_output")
            }
            assert len(downloaded_files) == 0
        except Exception as e:
            exception = e

        if os_value is None:
            os.environ.pop("TEST_OUTPUT")
        else:
            os.environ["TEST_OUTPUT"] = os_value

        if exception is not None:
            raise exception

    def test_upload_objects(self) -> None:
        storage_adapter = S3StorageAdapter(
            location=Location(
                uri="http://127.0.0.1:9000/test-bucket/checkpoints/result_model.pth",
                username="testuser",
                password="12345678",
                location_id="TEST_OUTPUT",
            )
        )

        with self.assertRaises(EndpointConnectionError):
            # TODO: What is an potential assert for which we can ask here?
            #       Checking that the file is stored in the s3 instance?
            storage_adapter.upload_object(
                src_file=os.path.join(
                    self.project_root, "test_data/checkpoints/model.pth"
                )
            )

    def test_s3_data_init_error(self) -> None:
        with self.assertRaises(ValueError):
            S3Data(uri="http://127.0.0.1:9000")


if __name__ == "__main__":
    unittest.main()

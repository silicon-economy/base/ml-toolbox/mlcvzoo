# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import os
import time
import unittest
import uuid
from typing import Any, Dict, List, Optional, Tuple
from unittest import TestCase, mock
from unittest.mock import MagicMock

import related
from config_builder import BaseConfigClass
from mlcvzoo_base.api.configuration import ModelConfiguration
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.interfaces import NetBased, Trainable
from mlcvzoo_base.api.model import DataType, ObjectDetectionModel
from mlcvzoo_base.configuration.annotation_handler_config import (
    AnnotationHandlerConfig,
    AnnotationHandlerPASCALVOCInputDataConfig,
)
from mlcvzoo_base.configuration.class_mapping_config import ClassMappingConfig
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.models.read_from_file.model import ReadFromFileConfig
from mlcvzoo_base.utils import get_project_path_information
from mlcvzoo_util.model_evaluator.structs import CheckpointInfo
from pytest import fixture, mark
from pytest_mock import MockerFixture
from related import to_dict
from werkzeug.test import TestResponse

from mlcvzoo_service.adapter.data_location import Location
from mlcvzoo_service.constants import RequestKeys, TrainingStatus
from mlcvzoo_service.index import app
from mlcvzoo_service.training_job import TrainingJob
from mlcvzoo_service.training_job_manager import TrainingJobManager

logger = logging.getLogger(__name__)


class TestConfig(ModelConfiguration):
    test_config: str = related.StringField(required=False, default="Hello World")


class TestModel(ObjectDetectionModel, NetBased, Trainable):
    def __init__(
        self,
        from_yaml: Optional[str] = None,
        configuration: Optional[ModelConfiguration] = None,
        string_replacement_map: Optional[Dict[str, str]] = None,
        init_for_inference: bool = True,
    ):
        pass

    def get_checkpoint_filename_suffix(self) -> str:
        pass

    def get_training_output_dir(self) -> str:
        pass

    def predict(self, data_item: DataType) -> Tuple[DataType, List[BoundingBox]]:
        return data_item, []

    @property
    def num_classes(self) -> int:
        return 1

    def get_classes_id_dict(self) -> Dict[int, str]:
        return {}

    @staticmethod
    def create_configuration(
        from_yaml: Optional[str] = None,
        configuration: Optional[TestConfig] = None,
        string_replacement_map: Optional[Dict[str, str]] = None,
    ) -> TestConfig:
        return TestConfig(unique_name="test-model")

    def restore(self, checkpoint_path: str) -> None:
        pass

    def train(self) -> None:
        pass

    def store(self, checkpoint_path: str) -> None:
        pass


@fixture(scope="function")
def determine_config_class_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_base.models.model_registry.ModelRegistry.determine_config_class",
        return_value=TestConfig,
    )


@fixture(scope="function")
def training_job_load_training_data_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_service.training_job.TrainingJob._TrainingJob__load_training_data",
        return_value=None,
    )


@fixture(scope="function")
def training_job_train_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_service.training_job.TrainingJob._TrainingJob__train",
        return_value=None,
    )


class TestTrainService(TestCase):
    def setUp(self):
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_service"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

        app.config["TESTING"] = True
        return app

    def __generate_default_params(self) -> Dict[str, Any]:
        minio_target_dir = os.path.join(
            self.project_root,
            "test_output/test-bucket",
        )

        return {
            RequestKeys.IMAGE_LOCATIONS.value: [
                {
                    "adapter_type": "S3",
                    "data": {
                        "uri": "https://127.0.0.1:9000/test-bucket/images",
                        "username": "testuser",
                        "password": "12345678",
                        "location_id": "TEST_OUTPUT",
                    },
                }
            ],
            RequestKeys.ANNOTATION_LOCATIONS.value: [
                {
                    "adapter_type": "S3",
                    "data": {
                        "uri": "https://127.0.0.1:9000/test-bucket/annotations",
                        "username": "testuser",
                        "password": "12345678",
                        "location_id": "TEST_OUTPUT",
                    },
                }
            ],
            RequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION.value: {
                "adapter_type": "S3",
                "data": {
                    "uri": "https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/model.pth",
                    "username": "testuser",
                    "password": "12345678",
                    "location_id": "TEST_OUTPUT",
                },
            },
            RequestKeys.RESULT_MODEL_CHECKPOINT_LOCATION.value: {
                "adapter_type": "S3",
                "data": {
                    "uri": "https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/result_model.pth",
                    "username": "testuser",
                    "password": "12345678",
                    "location_id": "TEST_OUTPUT",
                },
            },
            RequestKeys.MODEL_TYPE_NAME.value: "test_model",
            RequestKeys.MODEL_CONFIG_DATA.value: TestConfig(
                unique_name="test-model"
            ).to_dict(),
            "EVAL_CONFIG": {
                "iou_thresholds": [0.5],
                "annotation_handler_config": AnnotationHandlerConfig(
                    pascal_voc_input_data=[
                        AnnotationHandlerPASCALVOCInputDataConfig(
                            input_image_dir=os.path.join(minio_target_dir, "images"),
                            input_xml_dir=os.path.join(
                                minio_target_dir,
                                "annotations/pascal_voc/dummy_task",
                            ),
                            image_format=".jpg",
                            input_sub_dirs=[],
                        )
                    ],
                ).to_dict(),
            },
            "DATASET_NAME": "Test Dataset",
        }

    @mark.usefixtures(
        "training_job_train_mock",
        "determine_config_class_mock",
        "training_job_load_training_data_mock",
    )
    def test_training_job_manager_train_single_job(self):
        training_job_manager = TrainingJobManager()

        job_id = training_job_manager.add_training_job(
            image_locations=[],
            annotation_locations=[],
            initial_model_checkpoint_location=Location(
                username="test",
                password="test",
                uri="https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/result_model.pth",
                location_id="TEST_OUTPUT",
            ),
            model_type_name="test_model",
            model_configuration_dict={
                "test_config": "Test",
                "unique_name": "test-model",
            },
            evaluator_configuration_dict={
                "iou_thresholds": [0.5],
                "annotation_handler_config": AnnotationHandlerConfig().to_dict(),
            },
        )

        time.sleep(5)

        assert (
            training_job_manager.get_job_status(job_id=job_id)
            == TrainingStatus.FINISHED
        )

    @mark.usefixtures(
        "training_job_train_mock",
        "determine_config_class_mock",
        "training_job_load_training_data_mock",
    )
    def test_training_job_manager_train_single_job_parameter_error(self):
        training_job_manager = TrainingJobManager()

        job_id = training_job_manager.add_training_job(
            image_locations=[],
            annotation_locations=[],
            initial_model_checkpoint_location=Location(
                username="test",
                password="test",
                uri="https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/result_model.pth",
                location_id="TEST_OUTPUT",
            ),
            model_type_name="test_model",
            model_configuration_dict={
                "WRONG_ITEM": "Test",
                "unique_name": "test-model",
            },
            evaluator_configuration_dict={
                "WRONG_ITEM": "Test",
            },
        )

        assert job_id is None

    @mark.usefixtures(
        "training_job_train_mock",
        "determine_config_class_mock",
        "training_job_load_training_data_mock",
    )
    def test_training_job_manager_job_status(self):
        training_job_manager = TrainingJobManager()

        training_job = TrainingJob(
            image_locations=[],
            annotation_locations=[],
            initial_model_checkpoint_location=Location(
                username="test",
                password="test",
                uri="https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/result_model.pth",
                location_id="TEST_OUTPUT",
            ),
            model_type_name="test_model",
            model_configuration_dict={
                "test_config": "Test",
                "unique_name": "test-model",
            },
            evaluator_configuration_dict={
                "iou_thresholds": [0.5],
                "annotation_handler_config": AnnotationHandlerConfig().to_dict(),
            },
            execute_as_thread=False,
            job_finished_callback=training_job_manager.job_finished,
        )

        job_id = uuid.UUID("{850a2039-9e98-418b-b59c-a07ac7960d5c}")

        training_job._TrainingJob__id = job_id

        training_job_manager._job_queue.append(training_job)

        assert training_job_manager.get_job_status(job_id=job_id) == TrainingStatus.NEW

        assert training_job_manager.get_job_status(job_id=uuid.uuid4()) is None


if __name__ == "__main__":
    unittest.main()

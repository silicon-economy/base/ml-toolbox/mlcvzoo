# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import os
import unittest
from typing import Dict, List, Optional, Tuple
from unittest import TestCase
from unittest.mock import MagicMock

import related
from attrs import define
from mlcvzoo_base.api.configuration import ModelConfiguration
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.interfaces import NetBased, Trainable
from mlcvzoo_base.api.model import DataType, ObjectDetectionModel
from mlcvzoo_base.configuration.annotation_handler_config import (
    AnnotationHandlerConfig,
    AnnotationHandlerPASCALVOCInputDataConfig,
)
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils import get_project_path_information
from mlcvzoo_util.model_evaluator.structs import CheckpointInfo
from pytest import fixture, mark
from pytest_mock import MockerFixture
from related import to_dict

from mlcvzoo_service.adapter.data_location import Location
from mlcvzoo_service.constants import TrainingStatus
from mlcvzoo_service.training_job import TrainingJob

logger = logging.getLogger(__name__)


@define
class TestConfig(ModelConfiguration):
    test_config: str = related.StringField(required=False, default="Hello World")


class TestModel(ObjectDetectionModel, NetBased, Trainable):
    def get_checkpoint_filename_suffix(self) -> str:
        return ""

    def get_training_output_dir(self) -> Optional[str]:
        return ""

    def __init__(
        self,
        from_yaml: Optional[str] = None,
        configuration: Optional[ModelConfiguration] = None,
        string_replacement_map: Optional[Dict[str, str]] = None,
        init_for_inference: bool = True,
    ):
        pass

    def predict(self, data_item: DataType) -> Tuple[DataType, List[BoundingBox]]:
        return data_item, []

    @property
    def num_classes(self) -> int:
        return 1

    def get_classes_id_dict(self) -> Dict[int, str]:
        return {}

    @staticmethod
    def create_configuration(
        from_yaml: Optional[str] = None,
        configuration: Optional[TestConfig] = None,
        string_replacement_map: Optional[Dict[str, str]] = None,
    ) -> TestConfig:
        return TestConfig(unique_name="test-model")

    def restore(self, checkpoint_path: str) -> None:
        pass

    def train(self) -> None:
        pass

    def store(self, checkpoint_path: str) -> None:
        pass


@fixture(scope="function")
def create_model_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_util.model_trainer.model_trainer.ModelTrainer.create_model",
        return_value=TestModel(),
    )


@fixture(scope="function")
def run_evaluation_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_service.training_job.run_evaluation",
        return_value=({}, CheckpointInfo(path="test_model.pth", score=1.0)),
    )


@fixture(scope="function")
def determine_config_class_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_base.models.model_registry.ModelRegistry.determine_config_class",
        return_value=TestConfig,
    )


@fixture(scope="function")
def download_objects_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_service.adapter.s3.adapter.S3StorageAdapter.download_objects",
        return_value=({}, []),
    )


@fixture(scope="function")
def upload_objects_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_service.adapter.s3.adapter.S3StorageAdapter.upload_object",
        return_value=None,
    )


class TestTrainingJob(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=3, code_base="mlcvzoo_service"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

    @mark.usefixtures(
        "create_model_mock",
        "determine_config_class_mock",
        "run_evaluation_mock",
        "download_objects_mock",
        "upload_objects_mock",
    )
    def test_add_job(self):
        minio_target_dir = os.path.join(
            self.project_root,
            "test_output/test-bucket",
        )

        def _job_finished(param) -> None:
            logger.info("Job finished")

        training_job = TrainingJob(
            image_locations=[
                Location(
                    uri="https://127.0.0.1:9000/test-bucket/images",
                    username="testuser",
                    password="12345678",
                    location_id="TEST_OUTPUT",
                )
            ],
            annotation_locations=[
                Location(
                    uri="https://127.0.0.1:9000/test-bucket/annotations",
                    username="testuser",
                    password="12345678",
                    location_id="TEST_OUTPUT",
                )
            ],
            initial_model_checkpoint_location=Location(
                uri="https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/model.pth",
                username="testuser",
                password="12345678",
                location_id="TEST_OUTPUT",
            ),
            result_model_checkpoint_location=Location(
                uri="https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/result_model.pth",
                username="testuser",
                password="12345678",
                location_id="TEST_OUTPUT",
            ),
            model_type_name="test_model",
            model_configuration_dict=TestConfig(unique_name="test-model").to_dict(),
            evaluator_configuration_dict={
                "iou_thresholds": [0.5],
                "annotation_handler_config": AnnotationHandlerConfig(
                    pascal_voc_input_data=[
                        AnnotationHandlerPASCALVOCInputDataConfig(
                            input_image_dir=os.path.join(minio_target_dir, "images"),
                            input_xml_dir=os.path.join(
                                minio_target_dir,
                                "annotations/pascal_voc/dummy_task",
                            ),
                            image_format=".jpg",
                            input_sub_dirs=[],
                        )
                    ],
                ).to_dict(),
            },
            execute_as_thread=False,
            mlflow_credentials=None,
            job_finished_callback=_job_finished,
        )

        training_job.run()

        assert training_job.status == TrainingStatus.FINISHED


if __name__ == "__main__":
    unittest.main()

# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import os
import unittest
from typing import Any, Dict, List, Optional, Tuple
from unittest.mock import MagicMock

import related
from config_builder import BaseConfigClass
from flask_testing import TestCase
from mlcvzoo_base.api.configuration import ModelConfiguration
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.interfaces import NetBased, Trainable
from mlcvzoo_base.api.model import DataType, ObjectDetectionModel
from mlcvzoo_base.configuration.annotation_handler_config import (
    AnnotationHandlerConfig,
    AnnotationHandlerPASCALVOCInputDataConfig,
)
from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils import get_project_path_information
from mlcvzoo_util.model_evaluator.structs import CheckpointInfo
from pytest import fixture, mark
from pytest_mock import MockerFixture
from related import to_dict
from werkzeug.test import TestResponse

from mlcvzoo_service.adapter.data_location import Location
from mlcvzoo_service.adapter.mlflow_credentials import MLFlowCredentials
from mlcvzoo_service.constants import (
    ImplementedMetricAdapters,
    RequestKeys,
    TrainingStatus,
)
from mlcvzoo_service.index import app
from mlcvzoo_service.training_job import TrainingJob

logger = logging.getLogger(__name__)


class TestConfig(ModelConfiguration):
    test_config: str = related.StringField(required=False, default="Hello World")


class TestModel(ObjectDetectionModel, NetBased, Trainable):
    def __init__(
        self,
        from_yaml: Optional[str] = None,
        configuration: Optional[ModelConfiguration] = None,
        string_replacement_map: Optional[Dict[str, str]] = None,
        init_for_inference: bool = True,
    ):
        pass

    def get_checkpoint_filename_suffix(self) -> str:
        pass

    def get_training_output_dir(self) -> str:
        pass

    def predict(self, data_item: DataType) -> Tuple[DataType, List[BoundingBox]]:
        return data_item, []

    @property
    def num_classes(self) -> int:
        return 1

    def get_classes_id_dict(self) -> Dict[int, str]:
        return {}

    @staticmethod
    def create_configuration(
        from_yaml: Optional[str] = None,
        configuration: Optional[TestConfig] = None,
        string_replacement_map: Optional[Dict[str, str]] = None,
    ) -> TestConfig:
        return TestConfig(unique_name="test-model")

    def restore(self, checkpoint_path: str) -> None:
        pass

    def train(self) -> None:
        pass

    def store(self, checkpoint_path: str) -> None:
        pass


@fixture(scope="function")
def create_model_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_util.model_trainer.model_trainer.ModelTrainer.create_model",
        return_value=TestModel(),
    )


@fixture(scope="function")
def run_evaluation_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_service.training_job.run_evaluation",
        return_value=({}, CheckpointInfo(path="test_model.pth", score=1.0)),
    )


@fixture(scope="function")
def determine_config_class_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_base.models.model_registry.ModelRegistry.determine_config_class",
        return_value=TestConfig,
    )


@fixture(scope="function")
def add_training_job_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_service.training_job_manager.TrainingJobManager.add_training_job",
        return_value=False,
    )


@fixture(scope="function")
def get_job_status_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_service.training_job_manager.TrainingJobManager.get_job_status",
        return_value=TrainingStatus.FINISHED,
    )


class TestTrainService(TestCase):
    def create_app(self):
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_service"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

        app.config["TESTING"] = True
        return app

    def __generate_default_params(self) -> Dict[str, Any]:
        minio_target_dir = os.path.join(
            self.project_root,
            "test_output/test-bucket",
        )

        return {
            RequestKeys.IMAGE_LOCATIONS.value: [
                {
                    "adapter_type": "S3",
                    "data": {
                        "uri": "https://127.0.0.1:9000/test-bucket/images",
                        "username": "testuser",
                        "password": "12345678",
                        "location_id": "TEST_OUTPUT",
                    },
                }
            ],
            RequestKeys.ANNOTATION_LOCATIONS.value: [
                {
                    "adapter_type": "S3",
                    "data": {
                        "uri": "https://127.0.0.1:9000/test-bucket/annotations",
                        "username": "testuser",
                        "password": "12345678",
                        "location_id": "TEST_OUTPUT",
                    },
                }
            ],
            RequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION.value: {
                "adapter_type": "S3",
                "data": {
                    "uri": "https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/model.pth",
                    "username": "testuser",
                    "password": "12345678",
                    "location_id": "TEST_OUTPUT",
                },
            },
            RequestKeys.RESULT_MODEL_CHECKPOINT_LOCATION.value: {
                "adapter_type": "S3",
                "data": {
                    "uri": "https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/result_model.pth",
                    "username": "testuser",
                    "password": "12345678",
                    "location_id": "TEST_OUTPUT",
                },
            },
            RequestKeys.MODEL_TYPE_NAME.value: "test_model",
            RequestKeys.MODEL_CONFIG_DATA.value: TestConfig(
                unique_name="test-model"
            ).to_dict(),
            RequestKeys.EVAL_CONFIG.value: {
                "iou_thresholds": [0.5],
                "annotation_handler_config": AnnotationHandlerConfig(
                    pascal_voc_input_data=[
                        AnnotationHandlerPASCALVOCInputDataConfig(
                            input_image_dir=os.path.join(minio_target_dir, "images"),
                            input_xml_dir=os.path.join(
                                minio_target_dir,
                                "annotations/pascal_voc/dummy_task",
                            ),
                            image_format=".jpg",
                            input_sub_dirs=[],
                        )
                    ],
                ).to_dict(),
            },
            RequestKeys.MLFLOW_CREDENTIALS.value: {
                "adapter_type": ImplementedMetricAdapters.MLFLOW.value,
                "data": {
                    "s3_artifact_access_key": "test-key",
                    "s3_artifact_access_id": "test-id",
                    "s3_artifact_region": "eu-region-1",
                    "s3_artifact_uri": "https://127.0.0.1:9000/test-bucket/",
                    "server_uri": "https://127.0.0.1:5050",
                    "run_id": "12345678",
                },
            },
        }

    @mark.usefixtures(
        "determine_config_class_mock", "create_model_mock", "run_evaluation_mock"
    )
    def test_mlcvzoo_service_train(self):
        with app.app_context():
            train_response: TestResponse = self.client.post(
                "/train", json=self.__generate_default_params()
            )
            logger.info("Training response: %r" % train_response)

            assert train_response.status == "200 OK"

    def test_mlcvzoo_service_train_missing_param(self):
        with app.app_context():
            params = self.__generate_default_params()
            params.pop(RequestKeys.IMAGE_LOCATIONS.value)
            train_response: TestResponse = self.client.post("/train", json=params)
            logger.info("Training response: %r" % train_response)

            assert train_response.status == "400 BAD REQUEST"

    @mark.usefixtures(
        "add_training_job_mock",
    )
    def test_mlcvzoo_service_train_not_accepted(self):
        with app.app_context():
            train_response: TestResponse = self.client.post(
                "/train", json=self.__generate_default_params()
            )
            logger.info("Training response: %r" % train_response)

            assert train_response.status == "500 INTERNAL SERVER ERROR"

    @mark.usefixtures(
        "get_job_status_mock",
    )
    def test_mlcvzoo_service_job_status(self):
        with app.app_context():
            job_status_response: TestResponse = self.client.get(
                "/job_status", json={"job_id": "12345678123456781234567812345678"}
            )
            logger.info("Job status response: %r" % job_status_response)

            assert job_status_response.status == "200 OK"
            assert job_status_response.data == b"{'job_status': 'FINISHED'}"

    def test_mlcvzoo_service_job_status_bad(self):
        with app.app_context():
            job_status_response: TestResponse = self.client.get(
                "/job_status", json={"job_id": "12345678123456781234567812345678"}
            )
            logger.info("Job status response: %r" % job_status_response)

            assert job_status_response.status == "400 BAD REQUEST"

    def test_mlcvzoo_service_default(self):
        with app.app_context():
            default_response: TestResponse = self.client.post("/", json={})
            logger.info("Default response: %r" % default_response)

            assert default_response.status == "200 OK"


if __name__ == "__main__":
    unittest.main()

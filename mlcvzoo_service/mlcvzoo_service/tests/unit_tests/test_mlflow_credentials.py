# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import os
import unittest
from typing import Dict
from unittest import TestCase

from mlcvzoo_base.configuration.utils import get_replacement_map_from_replacement_config
from mlcvzoo_base.utils import get_project_path_information

from mlcvzoo_service.adapter.mlflow_credentials import MLFlowCredentials

logger = logging.getLogger(__name__)


class TestMlflowCredentials(TestCase):
    def setUp(self) -> None:
        (
            self.this_dir,
            self.project_root,
            self.code_root,
        ) = get_project_path_information(
            file_path=__file__, dir_depth=4, code_base="mlcvzoo_service"
        )

        self.string_replacement_map: Dict[str, str]

        (
            self.string_replacement_map,
            _,
        ) = get_replacement_map_from_replacement_config(
            yaml_config_path=os.path.join(
                self.project_root, "config", "replacement_config.yaml"
            ),
        )

        # Set dummy variables for test cases
        self.credentials = (
            "AWS_DEFAULT_REGION",
            "AWS_SECRET_ACCESS_KEY",
            "AWS_ACCESS_KEY_ID",
            "MLFLOW_S3_ENDPOINT_URL",
        )

        self.values = []
        for credential in self.credentials:
            value = os.getenv(credential)
            if value is not None:
                self.values.append(value)
            else:
                self.values.append(None)

            # Temporarily set dummy value for test case
            os.environ[credential] = credential.lower()

    def tearDown(self) -> None:
        # Reset variables to initial values
        for pair in zip(self.credentials, self.values):
            if pair[1] is not None:
                os.environ[pair[0]] = pair[1]
            else:
                del os.environ[pair[0]]

    def test_update_os_environment(self) -> None:
        credentials = MLFlowCredentials(
            s3_artifact_access_key="s3_artifact_access_key",
            s3_artifact_access_id="s3_artifact_access_id",
            s3_artifact_region="s3_artifact_region",
            s3_artifact_uri="s3_artifact_uri",
            server_uri="server_uri",
            run_id="run_id",
        )

        credentials.update_os_environment()

        self.assertEqual(
            os.getenv("AWS_DEFAULT_REGION"), credentials.s3_artifact_region
        )
        self.assertEqual(
            os.getenv("AWS_SECRET_ACCESS_KEY"), credentials.s3_artifact_access_key
        )
        self.assertEqual(
            os.getenv("AWS_ACCESS_KEY_ID"), credentials.s3_artifact_access_id
        )
        self.assertEqual(
            os.getenv("MLFLOW_S3_ENDPOINT_URL"), credentials.s3_artifact_uri
        )


if __name__ == "__main__":
    unittest.main()

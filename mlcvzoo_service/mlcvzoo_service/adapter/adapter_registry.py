# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

from typing import Any, Dict

from mlcvzoo_service.adapter.data_location import Location
from mlcvzoo_service.adapter.mlflow_credentials import MLFlowCredentials
from mlcvzoo_service.constants import (
    ImplementedMetricAdapters,
    ImplementedStorageAdapters,
)

adapter_registry: Dict[str, Any] = {
    ImplementedStorageAdapters.S3.value: Location,
    ImplementedMetricAdapters.MLFLOW.value: MLFlowCredentials,
}

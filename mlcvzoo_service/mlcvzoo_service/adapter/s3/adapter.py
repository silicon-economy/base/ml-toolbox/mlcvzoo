# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
TODO
"""

import logging
import os
from dataclasses import dataclass
from typing import Any, Dict, List, Optional, Tuple, cast
from urllib.parse import urlparse

import boto3
from mypy_boto3_s3 import S3Client

from mlcvzoo_service.adapter.data_location import Location
from mlcvzoo_service.utils import update_string_replacement_map

logger = logging.getLogger(__name__)


@dataclass(init=False)
class S3Data:
    """
    Data in the S3 storage to be accessed.
    """

    endpoint: str
    bucket_name: str
    prefix: str

    def __init__(self, uri: str):
        url = urlparse(uri)
        self.endpoint = f"{url.scheme}://{url.netloc}"
        try:
            # Split by '/' starts with the empty string so first token is in [1]
            self.bucket_name = url.path.split("/")[1]
            self.prefix = "/".join(url.path.split("/")[2:])
        except IndexError:
            raise ValueError(
                f"Could not build a S3Data object from the given uri='{uri}' "
            )


# TODO: Potentially define abstract base class
class S3StorageAdapter:
    """
    Adapter class for handling data in a S3 storage
    """

    def __init__(self, location: Location, s3_config: str = "s3") -> None:
        self.location = location
        self.s3_data = S3Data(location.uri)
        self.s3_config = s3_config

        self.client = self.__init_client()

    def __init_client(self) -> S3Client:
        """
        Inits an S3 client
        """

        return cast(
            S3Client,
            boto3.client(  # type: ignore[call-overload]
                "s3",
                region_name="eu-central-1",  # TODO: Make configurable
                endpoint_url=self.s3_data.endpoint,
                aws_access_key_id=self.location.username,
                aws_secret_access_key=self.location.password,
                aws_session_token=None,
                config=boto3.session.Config(  # type: ignore[attr-defined]
                    signature_version=self.s3_config
                ),
                verify=False,
            ),
        )

    def _list_objects(self, bucket_name: str, prefix: str) -> Any:
        # TODO: If the objects are not stored in the S3, the returned dict does not have
        #       a Contents entry. TODO Handle error?!
        return self.client.list_objects_v2(Bucket=bucket_name, Prefix=prefix)

    def _download_file(self, s3_key: str, local_path: str) -> Optional[str]:
        self.client.download_file(
            Bucket=self.s3_data.bucket_name, Key=s3_key, Filename=local_path
        )

        # TODO: needed?
        if os.path.isfile(local_path):
            return local_path

        return None

    def download_objects(
        self, string_replacement_map: Dict[str, str]
    ) -> Tuple[Dict[str, str], List[str]]:
        """
        Downloads all objects from a s3 location utilizing the
        s3_data object of this instance. The download directory
        is determined by finding a replacement value for the
        location-id of the s3 object.
        target_dir + bucket_name

        An object can be any file e.g. an image or an annotation file.

        Args:
            string_replacement_map:

        Returns:
            The updated string_replacement_map
        """

        objects = self._list_objects(
            bucket_name=self.s3_data.bucket_name, prefix=self.s3_data.prefix
        )

        object_contents = objects["Contents"]

        location_dir = os.getenv(self.location.location_id, os.getcwd())

        download_dir = os.path.join(
            location_dir,
            self.s3_data.bucket_name,
        )
        # TODO: Re-use concept / code of ConfigBuilder?
        update_string_replacement_map(
            string_replacement_map=string_replacement_map,
            replacement_key=self.location.location_id,
            replacement_value=location_dir,
        )
        logger.info("Download and store data from S3 to directory='%s'" % download_dir)

        downloaded_files: List[str] = []
        if len(objects["Contents"]) > 0:
            for obj in object_contents:
                file_path = os.path.join(download_dir, obj["Key"])
                logger.debug("Download s3 file %s to %s", obj["Key"], file_path)
                os.makedirs(name=os.path.dirname(file_path), exist_ok=True)

                downloaded_file_path = self._download_file(
                    s3_key=obj["Key"], local_path=file_path
                )

                if downloaded_file_path is not None:
                    downloaded_files.append(downloaded_file_path)
        else:
            logger.warning("Did not find any files in bucket='' with prefix=''")

        return string_replacement_map, downloaded_files

    def upload_object(self, src_file: str) -> None:
        """
        Uploads a file as object to a bucket.

        Args:
            src_file: Path of the file to upload

        Returns:
            None
        """

        # TODO: Check for overwrite of checkpoint?
        # objects = self.client.list_objects(Bucket=self.s3_data.bucket_name)["Contents"]
        #
        # for obj in objects:
        #     if obj["Key"] in self.s3_data.prefix:
        #         print("Duplicate")

        self.client.upload_file(
            Bucket=self.s3_data.bucket_name,
            Key=self.s3_data.prefix,
            Filename=src_file,
        )

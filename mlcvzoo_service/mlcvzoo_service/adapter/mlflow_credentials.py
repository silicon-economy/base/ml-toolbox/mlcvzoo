# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Providing classes that allow to store information that is needed
in order to enable the service to connect to a running mlflow server
and its artifact store
"""

import os
from dataclasses import dataclass, field
from enum import Enum


class MLFlowCredentialConstants(Enum):
    """
    Provide constants for environment variable names
    which provide mlflow s3 credential information
    """

    AWS_DEFAULT_REGION = "AWS_DEFAULT_REGION"
    AWS_SECRET_ACCESS_KEY = "AWS_SECRET_ACCESS_KEY"
    AWS_ACCESS_KEY_ID = "AWS_ACCESS_KEY_ID"
    MLFLOW_S3_ENDPOINT_URL = "MLFLOW_S3_ENDPOINT_URL"
    MLFLOW_SERVER_URL = "MLFLOW_SERVER_URL"


@dataclass
class MLFlowCredentials:
    """
    Provide relevant information to be able to register a
    trained model on a remote MLflow Model Registry instance
    """

    # Hide those fields from repr to not leak them during logging
    s3_artifact_access_key: str = field(repr=False)
    s3_artifact_access_id: str = field(repr=False)
    s3_artifact_region: str
    s3_artifact_uri: str
    server_uri: str
    run_id: str

    def update_os_environment(self) -> None:
        """
        Updates the values of all class variables to the
        equivalent os environment variable content

        Returns:
            None
        """
        os.environ[
            MLFlowCredentialConstants.AWS_DEFAULT_REGION.value
        ] = self.s3_artifact_region
        os.environ[
            MLFlowCredentialConstants.AWS_SECRET_ACCESS_KEY.value
        ] = self.s3_artifact_access_key
        os.environ[
            MLFlowCredentialConstants.AWS_ACCESS_KEY_ID.value
        ] = self.s3_artifact_access_id
        os.environ[
            MLFlowCredentialConstants.MLFLOW_S3_ENDPOINT_URL.value
        ] = self.s3_artifact_uri

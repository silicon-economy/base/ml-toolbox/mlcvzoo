# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Providing a class for accessing files via a given uri and credentials
"""

from dataclasses import dataclass, field


@dataclass
class Location:
    """
    Class to store information about a data location that is accessible via
    credentials and the given URI
    """

    # Hide those fields from repr to not leak them during logging
    username: str = field(repr=False)
    password: str = field(repr=False)
    uri: str
    location_id: str

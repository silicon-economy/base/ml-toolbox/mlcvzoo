# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

from enum import Enum


class RequestParams(Enum):
    ADAPTER_TYPE = "adapter_type"
    DATA = "data"


class RequestKeys(Enum):
    IMAGE_LOCATIONS = "IMAGE_LOCATIONS"
    ANNOTATION_LOCATIONS = "ANNOTATION_LOCATIONS"
    INITIAL_MODEL_CHECKPOINT_LOCATION = "INITIAL_MODEL_CHECKPOINT_LOCATION"
    RESULT_MODEL_CHECKPOINT_LOCATION = "RESULT_MODEL_CHECKPOINT_LOCATION"
    MODEL_TYPE_NAME = "MODEL_TYPE_NAME"
    MODEL_CONFIG_CLASS = "MODEL_CONFIG_CLASS"
    MODEL_CONFIG_DATA = "MODEL_CONFIG_DATA"
    MODEL_EXTRA_CONFIG_LOCATION = "MODEL_EXTRA_CONFIG_LOCATION"
    EVAL_CONFIG = "EVAL_CONFIG"
    CREDENTIALS = "CREDENTIALS"
    MLFLOW_CREDENTIALS = "MLFLOW_CREDENTIALS"


class ImplementedStorageAdapters(Enum):
    S3 = "S3"


class ImplementedMetricAdapters(Enum):
    MLFLOW = "MLFLOW"


class TrainingStatus(Enum):
    NEW = "NEW"
    RUNNING = "RUNNING"
    FINISHED = "FINISHED"
    CRASHED = "CRASHED"

# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
TODO
"""

import logging
import uuid
from typing import Any, Dict, List, Optional

from mlcvzoo_service.adapter.data_location import Location
from mlcvzoo_service.adapter.mlflow_credentials import MLFlowCredentials
from mlcvzoo_service.constants import TrainingStatus
from mlcvzoo_service.training_job import TrainingJob

logger = logging.getLogger(__name__)


class TrainingJobManager:
    def __init__(self) -> None:
        self._job_queue: List[TrainingJob] = []
        self.__index: int = 0
        self.execute_jobs_as_threads = True

    @property
    def job_queue(self) -> List[TrainingJob]:
        return self._job_queue

    def get_job_status(self, job_id: uuid.UUID) -> Optional[TrainingStatus]:
        for job in self._job_queue:
            if job.id == job_id:
                return job.status
        return None

    def job_finished(self, training_job: TrainingJob) -> None:
        logger.info("TrainingJob with uuid=%r finished" % training_job.id)

        if (
            self.__index < len(self._job_queue)
            and self._job_queue[self.__index].status != TrainingStatus.RUNNING
        ):
            if self.execute_jobs_as_threads:
                self._job_queue[self.__index].start()
            else:
                self._job_queue[self.__index].run()

    def add_training_job(
        self,
        image_locations: List[Location],
        annotation_locations: List[Location],
        initial_model_checkpoint_location: Location,
        model_type_name: str,
        model_configuration_dict: Dict[str, Any],
        evaluator_configuration_dict: Dict[str, Any],
        result_model_checkpoint_location: Optional[Location] = None,
        mlflow_credentials: Optional[MLFlowCredentials] = None,
    ) -> Optional[uuid.UUID]:
        try:
            new_job = TrainingJob(
                image_locations=image_locations,
                annotation_locations=annotation_locations,
                initial_model_checkpoint_location=initial_model_checkpoint_location,
                model_type_name=model_type_name,
                model_configuration_dict=model_configuration_dict,
                evaluator_configuration_dict=evaluator_configuration_dict,
                execute_as_thread=self.execute_jobs_as_threads,
                job_finished_callback=self.job_finished,
                result_model_checkpoint_location=result_model_checkpoint_location,
                mlflow_credentials=mlflow_credentials,
            )

            self._job_queue.append(new_job)

            if self._job_queue[self.__index].status != TrainingStatus.RUNNING:
                self.__index += 1

                if self.execute_jobs_as_threads:
                    new_job.start()
                else:
                    new_job.run()

            return new_job.id
        except Exception as e:
            logger.exception(e)
            return None

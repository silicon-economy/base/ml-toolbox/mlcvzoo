# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import logging
import uuid
from typing import Any, Dict, List, Optional, Tuple, Union

from flask import Flask, jsonify, request
from flask.wrappers import Response

from mlcvzoo_service.adapter.adapter_registry import adapter_registry
from mlcvzoo_service.adapter.data_location import Location
from mlcvzoo_service.adapter.mlflow_credentials import MLFlowCredentials
from mlcvzoo_service.constants import RequestKeys, RequestParams
from mlcvzoo_service.training_job_manager import TrainingJobManager

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)

app = Flask(__name__)

training_job_manager: TrainingJobManager = TrainingJobManager()


@app.route("/", methods=["POST"])
def default() -> Tuple[Union[str, Dict[str, Any], Response], int]:
    return jsonify(training_job_manager.job_queue), 200


@app.route("/train", methods=["POST"])
def train() -> Tuple[str, int]:
    """
    Takes a training post, parses the relevant parameters and
    start a new TrainingJob if they fulfill the requirements

    Returns:
        REST Response stating the status of the performed training
    """

    params: Optional[Dict[str, Any]] = request.get_json()

    if params is None:
        return "No params given", 500

    # TODO: pop every parameter to check if there are fed in to many?
    # TODO: Only allow paths that are within the DOWNLOAD_LOCATION
    try:
        image_locations: List[Union[Location]] = []
        for location in params[RequestKeys.IMAGE_LOCATIONS.value]:
            image_locations.append(
                adapter_registry[location[RequestParams.ADAPTER_TYPE.value]](
                    **location[RequestParams.DATA.value]
                )
            )

        annotation_locations: List[Union[Location]] = []
        for location in params[RequestKeys.ANNOTATION_LOCATIONS.value]:
            annotation_locations.append(
                adapter_registry[location[RequestParams.ADAPTER_TYPE.value]](
                    **location[RequestParams.DATA.value]
                )
            )
        initial_model_checkpoint_location: Union[Location] = adapter_registry[
            params[RequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION.value][
                RequestParams.ADAPTER_TYPE.value
            ]
        ](
            **params[RequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION.value][
                RequestParams.DATA.value
            ]
        )
        result_model_checkpoint_location: Optional[Union[Location]] = None
        try:
            result_model_checkpoint_location = adapter_registry[
                params[RequestKeys.RESULT_MODEL_CHECKPOINT_LOCATION.value][
                    RequestParams.ADAPTER_TYPE.value
                ]
            ](
                **params[RequestKeys.RESULT_MODEL_CHECKPOINT_LOCATION.value][
                    RequestParams.DATA.value
                ]
            )
        except KeyError:
            pass

        model_type_name: str = params[RequestKeys.MODEL_TYPE_NAME.value]
        model_configuration_dict: Dict[str, Any] = params[
            RequestKeys.MODEL_CONFIG_DATA.value
        ]

        evaluator_configuration_dict: Dict[str, Any] = params[
            RequestKeys.EVAL_CONFIG.value
        ]

        mlflow_credentials: Optional[MLFlowCredentials] = None
        if RequestKeys.MLFLOW_CREDENTIALS.value in params:
            mlflow_credentials = adapter_registry[
                params[RequestKeys.MLFLOW_CREDENTIALS.value][
                    RequestParams.ADAPTER_TYPE.value
                ]
            ](**params[RequestKeys.MLFLOW_CREDENTIALS.value][RequestParams.DATA.value])
    except KeyError as key_error:
        logger.exception(key_error)
        return (
            f"Missing parameter: {str(key_error)}, "
            f"please provide {[r.value for r in RequestKeys]}",
            400,
        )

    job_id = training_job_manager.add_training_job(
        image_locations=image_locations,
        annotation_locations=annotation_locations,
        initial_model_checkpoint_location=initial_model_checkpoint_location,
        model_type_name=model_type_name,
        model_configuration_dict=model_configuration_dict,
        evaluator_configuration_dict=evaluator_configuration_dict,
        result_model_checkpoint_location=result_model_checkpoint_location,
        mlflow_credentials=mlflow_credentials,
    )

    if job_id:
        return str({"job_id": str(job_id)}), 200
    else:
        return "training job not accepted", 500


@app.route("/job_status", methods=["GET"])
def job_status() -> Tuple[str, int]:
    """
    Returns:
        REST Response stating the status of the current running job
    """
    params: Optional[Dict[str, Any]] = request.get_json()

    if params is None:
        return "No params given", 500

    _job_status = training_job_manager.get_job_status(
        job_id=uuid.UUID("{%s}" % params["job_id"])
    )

    if _job_status is not None:
        return str({"job_status": str(_job_status.value)}), 200
    else:
        return f"TrainingJob not found", 400

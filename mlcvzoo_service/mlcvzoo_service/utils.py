# Copyright 2023 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Module for gathering utility functions that are needed across the mlcvzoo-service
"""

import logging
from typing import Dict

logger = logging.getLogger(__name__)


def update_string_replacement_map(
    string_replacement_map: Dict[str, str], replacement_key: str, replacement_value: str
) -> None:
    if replacement_key not in string_replacement_map:
        logger.info(
            "Update string-replacement-map: key='%s', value='%s'",
            replacement_key,
            replacement_value,
        )
        string_replacement_map[replacement_key] = replacement_value
    else:
        # TODO: Overwrite existing value?
        pass

#!/bin/sh

if [ $# -lt 1 ]; then
  echo "Usage: $0 POETRY_ARGS"
  return 1
fi

# Install yolox afterwards as it fails in poetry due to pep517, and others fail without it:
if ! poetry "$@"; then
  # All deps are already present, install yolox without build-isolation and pep517
  poetry run python -m pip install \
  --no-deps \
  --no-build-isolation \
  --no-use-pep517 \
  yolox==$(poetry show yolox | grep version | awk '{print $3}')
  # Repeat to ensure that all dependencies are installed even if yolox was not the last one
  poetry "$@"
fi

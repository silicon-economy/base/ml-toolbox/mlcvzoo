ARG BASE_IMG=nvidia/cuda:11.1.1-cudnn8-devel-ubuntu20.04
FROM $BASE_IMG

# Fetch updated NVIDIA keys
RUN apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/3bf863cc.pub && \
    apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/7fa2af80.pub

# INFORMATION about packages:
# - gettext-base is needed for envsubst which is used in CI
# - libgl1-mesa-glx is needed by opencv-python, otherwise it
#   results in an 'ImportError: libGL.so.1'
# - libprotobuf-dev and protobuf-compiler are needed by onnx
# - libpq-dev is needed by postgresql
# - blas, lapack, fftw3 and hdf5 are needed by pytorch et. al.
# - Keep default GCC (see compatibility matrix here: https://docs.nvidia.com/deeplearning/cudnn/pdf/cuDNN-Support-Matrix.pdf)
#   * GCC-7 for Jetson builds (automatically via ubuntu 18.04)
#   * GCC-9 for AMD64 and CUDA 11 (automatically via ubuntu 20.04)
RUN echo ">------ Update system, install packages" && \
    apt update && \
    DEBIAN_FRONTEND="noninteractive" apt install --no-install-recommends -y \
    nginx uwsgi uwsgi-plugin-python3 \
    build-essential gettext-base cmake ninja-build libprotobuf-dev protobuf-compiler \
    python3.8 python3.8-venv python3.8-dev python3-pip python3-setuptools \
		git tree curl unzip \
		pkg-config libgl1-mesa-glx libcap-dev libpq-dev libopencv-dev \
    libblas3 liblapack3 liblapack-dev libblas-dev gfortran libatlas-base-dev \
    libfftw3-bin libfftw3-dev libhdf5-dev libhdf5-mpi-dev libhdf5-openmpi-dev \
    && apt clean && rm -rf /var/lib/apt/lists/*

ENV CUDA_HOME="/usr/local/cuda/"

# ====================================================================
# Install project sources and dependencies

ARG MAKEFLAGS="-j8"

# For NVIDIA Jetson devices, select one or more appropriate CUDA arch values below
# Look up your CUDA compute capability at: https://developer.nvidia.com/cuda-gpus
# Some common CUDA compute Capabilites for x86:
#  - Quadro P5000/Geforce RTX10*0: 6.1
#  - Tesla V100: 7.0
#  - Quadro T1000/Geforce RTX20*0: 7.5
#  - NVIDIA A100: 8.0
#  - RTX A*000/Geforce RTX3090: 8.6
# Some common CUDA compute capabilities for Jetson devices (arm64):
#  - Jetson Nano/TX1/Tegra X1: 5.3
#  - Jetson TX2: 6.2
#  - Jetson Xavier AGX: 7.2
ARG CUDA_ARCH_LIST="6.1;7.0;7.5;8.0;8.6"
ENV TORCH_CUDA_ARCH_LIST $CUDA_ARCH_LIST

ENV CUDA_HOME="/usr/local/cuda/"

ENV PIP_NO_CACHE_DIR=1
ARG POETRY_VERSION="1.3.0"
RUN curl -sSL https://install.python-poetry.org | python3 - --version "$POETRY_VERSION"
ENV PATH "/root/.local/bin:$PATH"
RUN poetry --version

COPY . /srv/mlcvzoo-service
WORKDIR /srv/mlcvzoo-service

ENV VIRTUAL_ENV=/usr/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN poetry config experimental.new-installer true && python3 -m venv $VIRTUAL_ENV \
  && poetry run pip install --upgrade pip \
  && ./build.sh install --no-interaction --no-ansi --without dev --extras=yolox

COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/uwsgi.ini /etc/uwsgi/uwsgi.ini
RUN chmod ugo+rx /etc/uwsgi/ /etc/nginx/; chmod ugo+r /etc/nginx/nginx.conf /etc/uwsgi/uwsgi.ini
RUN mkdir -p /var/log/nginx/ && chmod --recursive ugo+rwx /var/log/nginx/
RUN mkdir -p /var/lib/nginx/body && chmod --recursive ugo+rwx /var/lib/nginx/
RUN chmod ugo+rwx /run
RUN chmod --recursive ugo+rw /srv/mlcvzoo-service

# Cleanup poetry cache
RUN rm -rf ~/.cache/pypoetry

EXPOSE 8080/tcp
# socket permissions are not read from uwsgi config file so set them here
CMD nginx & exec uwsgi --ini /etc/uwsgi/uwsgi.ini --chmod-socket=666

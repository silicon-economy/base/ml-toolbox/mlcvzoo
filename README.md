# MLCVZoo

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven) computer vision algorithms.
Details can be found in the [documentation](documentation/index.adoc).

## Install

The installation and setup of the MLCVZoo is described in [chapter 11](documentation/index.adoc)
of the documentation.

# Technology stack

- Python

## Contributing

We welcome and appreciate all contributions and feedback to the MLCVZoo. Guidelines for contributing code can be found in the
[contribution guidelines](CONTRIBUTING.md).

## License
See the license file in the top directory.

## Contact information

Maintainer:
- Maximilian Otten <a href="mailto:maximilian.otten@iml.fraunhofer.de?">maximilian.otten@iml.fraunhofer.de</a>

Development Team:
- Christian Hoppe <a href="mailto:christian.hoppe@iml.fraunhofer.de?">christian.hoppe@iml.fraunhofer.de</a>
- Oliver Bredtmann <a href="mailto:oliver.bredtmann@dbschenker.com?">oliver.bredtmann@dbschenker.com</a>
- Thilo Bauer <a href="mailto:thilo.bauer@dbschenker.com?">thilo.bauer@dbschenker.com</a>
- Oliver Urbann <a href="mailto:oliver.urbann@iml.fraunhofer.de?">oliver.urbann@iml.fraunhofer.de</a>
- Jan Basrawi <a href="mailto:jan.basrawi@dbschenker.com?">jan.basrawi@dbschenker.com</a>
- Luise Weickhmann <a href="mailto:luise.weickhmann@iml.fraunhofer.de?">luise.weickhmann@iml.fraunhofer.de</a>
- Luca Kotulla <a href="mailto:luca.kotulla@iml.fraunhofer.de?">luca.kotulla@iml.fraunhofer.de</a>
